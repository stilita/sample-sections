#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.10.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/01_MeshGeneration/T2_Bauchau')

####################################################
##       Begin of NoteBook variables section      ##
####################################################

b = 0.2
h = 0.2
length = 0.5

tp = 0.000625
# tp = 0.005

# mesh
n_thick = 2   # number of segments in thickness
n_width = 20  # number of segmenst in width (has to be even)
n_width_2 = n_width // 2
n_height = 16 # number of segments in height
n_length = 5  # number of blocks

is_quadratic = True
is_biquadratic = False

if is_quadratic:
    mesh_name = "T2_{0:02d}w_{1:02d}h_{2:02d}t_hex20".format(n_width, n_height, n_thick)
else:
    mesh_name = "T2_{0:02d}w_{1:02d}h_{2:02d}t_hex08".format(n_width, n_height, n_thick)


####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

vertices = []
lines = []
faces = []


# vertices first lower 4 plies
for i in range(5):
    vertices.append(geompy.MakeVertex(0, -b/2, -h/2+tp*i))
    if i == 4:
        vertices.append(geompy.MakeVertex(0,  0, -h/2+tp*i))
    vertices.append(geompy.MakeVertex(0,  b/2, -h/2+tp*i))

# vertices first upper 4 plies
for i in range(5):
    vertices.append(geompy.MakeVertex(0, -b/2,  h/2-tp*i))
    if i == 4:
        vertices.append(geompy.MakeVertex(0,  0,  h/2-tp*i))
    vertices.append(geompy.MakeVertex(0,  b/2,  h/2-tp*i))


#vertices second lower 4 plies
for i in range(4):
    vertices.append(geompy.MakeVertex(0, -b/2, -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0, -tp*(i+1), -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  tp*(i+1), -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  b/2, -h/2+tp*(5+i)))

# vertices for second upper 4 plies
for i in range(4):
    vertices.append(geompy.MakeVertex(0, -b/2, h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0, -tp*(i+1), h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  tp*(i+1), h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  b/2, h/2-tp*(5+i)))


#lines for first lower 4 plies
lines.append(geompy.MakeLineTwoPnt(vertices[0], vertices[1]))

for i in range(0,5,2):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+3]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+2]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+2], vertices[i]))

# last face with 5 lines
lines.append(geompy.MakeLineTwoPnt(vertices[7], vertices[10]))
lines.append(geompy.MakeLineTwoPnt(vertices[10], vertices[9]))
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[8]))
lines.append(geompy.MakeLineTwoPnt(vertices[8], vertices[6]))


# lines for first upper 4 plies
lines.append(geompy.MakeLineTwoPnt(vertices[11], vertices[12]))

for i in range(11,16,2):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+2]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+2], vertices[i+3]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+1]))


# last face with 5 lines
lines.append(geompy.MakeLineTwoPnt(vertices[17], vertices[19]))
lines.append(geompy.MakeLineTwoPnt(vertices[19], vertices[20]))
lines.append(geompy.MakeLineTwoPnt(vertices[20], vertices[21]))
lines.append(geompy.MakeLineTwoPnt(vertices[21], vertices[18]))


#lines for second lower 4 plies

#first ply
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[23]))
lines.append(geompy.MakeLineTwoPnt(vertices[23], vertices[22]))
lines.append(geompy.MakeLineTwoPnt(vertices[22], vertices[8]))

lines.append(geompy.MakeLineTwoPnt(vertices[10], vertices[25]))
lines.append(geompy.MakeLineTwoPnt(vertices[25], vertices[24]))
lines.append(geompy.MakeLineTwoPnt(vertices[24], vertices[9]))

# other 3 plies
for i in range(22,31,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+5]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+5], vertices[i+4]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[i]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+7]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+7], vertices[i+6]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+6], vertices[i+2]))


#lines for second upper 4 plies

#first ply
lines.append(geompy.MakeLineTwoPnt(vertices[20], vertices[39]))
lines.append(geompy.MakeLineTwoPnt(vertices[39], vertices[38]))
lines.append(geompy.MakeLineTwoPnt(vertices[38], vertices[19]))

lines.append(geompy.MakeLineTwoPnt(vertices[21], vertices[41]))
lines.append(geompy.MakeLineTwoPnt(vertices[41], vertices[40]))
lines.append(geompy.MakeLineTwoPnt(vertices[40], vertices[20]))


# other 3 plies
for i in range(38,47,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+5]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+5], vertices[i+4]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[i]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+7]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+7], vertices[i+6]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+6], vertices[i+2]))



# vertical plies
for i in range(23,36,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+16]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+17]))

#central line
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[20]))


# first bottom face
faces.append(geompy.MakeFaceWires([lines[0], lines[1], lines[2], lines[3]], 1))

for i in range(2,7,3):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+2], lines[i+3], lines[i+4]], 1))

# last face 5 lines
faces.append(geompy.MakeFaceWires([lines[8], lines[10], lines[11], lines[12], lines[13]], 1))

#first upper face
faces.append(geompy.MakeFaceWires([lines[14], lines[15], lines[16], lines[17]], 1))

for i in range(16,21,3):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+2], lines[i+3], lines[i+4]], 1))

# last face 5 lines
faces.append(geompy.MakeFaceWires([lines[22], lines[24], lines[25], lines[26], lines[27]], 1))



# first faces second lower ply
faces.append(geompy.MakeFaceWires([lines[12], lines[28], lines[29], lines[30]], 1))
faces.append(geompy.MakeFaceWires([lines[11], lines[31], lines[32], lines[33]], 1))

# other 3 lower plies
for i in range(29,42,6):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+5], lines[i+6], lines[i+7]], 1))
    faces.append(geompy.MakeFaceWires([lines[i+3], lines[i+8], lines[i+9], lines[i+10]], 1))


# first faces second upper ply
faces.append(geompy.MakeFaceWires([lines[25], lines[52], lines[53], lines[54]], 1))
faces.append(geompy.MakeFaceWires([lines[26], lines[55], lines[56], lines[57]], 1))

# other 3 upper plies
for i in range(53,66,6):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+5], lines[i+6], lines[i+7]], 1))
    faces.append(geompy.MakeFaceWires([lines[i+3], lines[i+8], lines[i+9], lines[i+10]], 1))

faces.append(geompy.MakeFaceWires([lines[82], lines[46], lines[80], lines[70]], 1))
faces.append(geompy.MakeFaceWires([lines[80], lines[40], lines[78], lines[64]], 1))
faces.append(geompy.MakeFaceWires([lines[78], lines[34], lines[76], lines[58]], 1))
faces.append(geompy.MakeFaceWires([lines[76], lines[28], lines[84], lines[52]], 1))

faces.append(geompy.MakeFaceWires([lines[83], lines[51], lines[81], lines[75]], 1))
faces.append(geompy.MakeFaceWires([lines[81], lines[45], lines[79], lines[69]], 1))
faces.append(geompy.MakeFaceWires([lines[79], lines[39], lines[77], lines[63]], 1))
faces.append(geompy.MakeFaceWires([lines[77], lines[33], lines[84], lines[57]], 1))


extrude = []

for face in faces:
    extrude.append(geompy.MakePrismVecH(face, OX, length))

T_comp = geompy.MakeCompound(extrude)

# ok
half_width_ind = [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 58, 59 ,60, 61, 62, 63, 64, 65, 66, 67]


thick_ind = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 26, 28, 30, 32, 38, 39, 41, 42, 44, 45, 50, 52, 53, 54, 56, 57, 68, 69]


width_ind = [33, 34, 35, 36, 46, 47, 48, 49]

height_ind = [27, 29, 31, 37, 40, 43, 51, 55]

all_ind = range(102)

length_ind = []

for i in all_ind:
    if i not in thick_ind and i not in half_width_ind and i not in width_ind and i not in height_ind:
        length_ind.append(i)

#print(length_ind)

Compound = geompy.Propagate(T_comp)
thickness = geompy.UnionListOfGroups([Compound[i] for i in thick_ind])
half_width = geompy.UnionListOfGroups([Compound[i] for i in half_width_ind])
width = geompy.UnionListOfGroups([Compound[i] for i in width_ind])
height = geompy.UnionListOfGroups([Compound[i] for i in height_ind])
length = geompy.UnionListOfGroups([Compound[i] for i in length_ind])



deg_p0_f0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p0_f0, [145, 179, 492, 526])

deg_p0_f45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p0_f45, [247, 288, 322])

deg_p0_fm45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p0_fm45, [213, 356, 390])

deg_p0_f90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p0_f90, [424, 458])



deg_p180_f0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p180_f0, [2, 36, 764, 798])

deg_p180_f45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p180_f45, [104, 560, 594])

deg_p180_fm45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p180_fm45, [70, 628, 662])

deg_p180_f90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p180_f90, [696, 730])


deg_p90_f0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p90_f0, [832])

deg_p90_f45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p90_f45, [934])

deg_p90_fm45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p90_fm45, [900])

deg_p90_f90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_p90_f90, [866])


deg_pm90_f0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_pm90_f0, [968])

deg_pm90_f45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_pm90_f45, [1070])

deg_pm90_fm45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_pm90_fm45, [1036])

deg_pm90_f90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_pm90_f90, [1002])




R1 = geompy.CreateGroup(T_comp, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [175, 209, 243, 284, 590, 624, 692, 658, 726, 794, 760, 828, 862, 896, 930, 964, 1100, 1066, 1032, 998, 522, 556, 488, 454, 386, 420, 352, 318, 141, 66, 100, 32])
R2 = geompy.CreateGroup(T_comp, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [177, 211, 245, 286, 592, 626, 660, 694, 728, 762, 796, 830, 864, 898, 932, 966, 1102, 1068, 1034, 1000, 524, 558, 490, 456, 388, 422, 320, 354, 143, 102, 68, 34])



deg_p0_f0.SetColor(SALOMEDS.Color(0.25,0,0))
deg_p0_f90.SetColor(SALOMEDS.Color(0.5,0,0))
deg_p0_f45.SetColor(SALOMEDS.Color(0.75,0,0))
deg_p0_fm45.SetColor(SALOMEDS.Color(1,0,0))

deg_p90_f0.SetColor(SALOMEDS.Color(0,0.25,1))
deg_p90_f90.SetColor(SALOMEDS.Color(0,0.5,1))
deg_p90_f45.SetColor(SALOMEDS.Color(0,0.75,1))
deg_p90_fm45.SetColor(SALOMEDS.Color(0,1,1))

deg_pm90_f0.SetColor(SALOMEDS.Color(0,0.25,0))
deg_pm90_f90.SetColor(SALOMEDS.Color(0,0.5,0))
deg_pm90_f45.SetColor(SALOMEDS.Color(0,0.75,0))
deg_pm90_fm45.SetColor(SALOMEDS.Color(0,1,0))

deg_p180_f0.SetColor(SALOMEDS.Color(0,0,0.25))
deg_p180_f90.SetColor(SALOMEDS.Color(0,0,0.5))
deg_p180_f45.SetColor(SALOMEDS.Color(0,0,0.75))
deg_p180_fm45.SetColor(SALOMEDS.Color(0,0,1))


#Tshell = geompy.MakeShell(faces)
#T_Solid = geompy.MakeThickSolid(Tshell, length, [], True)
#edge = geompy.Propagate(T_Solid)

#thickness = geompy.UnionListOfGroups([edge[0], edge[1], edge[2], edge[3], edge[4], edge[5], edge[6], edge[7], edge[8], edge[9], edge[10], edge[11], edge[17], edge[18], edge[19], edge[20], edge[23], edge[24]])
#half_width = geompy.UnionListOfGroups([edge[12], edge[13], edge[21], edge[22]])
#width = geompy.UnionListOfGroups([edge[15], edge[16]])


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for i, vertex in enumerate(vertices):
#    geompy.addToStudy( vertex, f'Vertex_{i}' )


#for i, line in enumerate(lines):
#    geompy.addToStudy( line, f'Line_{i}' )

#for i, solid in enumerate(extrude):
#    geompy.addToStudy( solid, f'solid_{i}' )
    
#geompy.addToStudy( Tshell, 'T_shell' )
#geompy.addToStudy( T_Solid, 'T_Solid' )


#for i, edge in enumerate(Edgepart):
#    geompy.addToStudyInFather( Tsolid, edge, f'edge_{i}' )

#geompy.addToStudyInFather( T_Solid, edge[14], 'height' )
#geompy.addToStudyInFather( T_Solid, edge[25], 'length' )
#geompy.addToStudyInFather( T_Solid, thickness, 'thickness' )
#geompy.addToStudyInFather( T_Solid, half_width, 'half_width' )
#geompy.addToStudyInFather( T_Solid, width, 'width' )

geompy.addToStudy( T_comp, 'T_comp' )
geompy.addToStudyInFather( T_comp, thickness, 'thickness' )
geompy.addToStudyInFather( T_comp, half_width, 'half_width' )
geompy.addToStudyInFather( T_comp, width, 'width' )
geompy.addToStudyInFather( T_comp, height, 'height' )
geompy.addToStudyInFather( T_comp, length, 'length' )

geompy.addToStudyInFather( T_comp, deg_p0_f0, 'deg_p0_f0' )
geompy.addToStudyInFather( T_comp, deg_p0_f90, 'deg_p0_f90' )
geompy.addToStudyInFather( T_comp, deg_p0_f45, 'deg_p0_f45' )
geompy.addToStudyInFather( T_comp, deg_p0_fm45, 'deg_p0_fm45' )

geompy.addToStudyInFather( T_comp, deg_p90_f0, 'deg_p90_f0' )
geompy.addToStudyInFather( T_comp, deg_p90_f90, 'deg_p90_f90' )
geompy.addToStudyInFather( T_comp, deg_p90_f45, 'deg_p90_f45' )
geompy.addToStudyInFather( T_comp, deg_p90_fm45, 'deg_p90_fm45' )

geompy.addToStudyInFather( T_comp, deg_p180_f0, 'deg_p180_f0' )
geompy.addToStudyInFather( T_comp, deg_p180_f90, 'deg_p180_f90' )
geompy.addToStudyInFather( T_comp, deg_p180_f45, 'deg_p180_f45' )
geompy.addToStudyInFather( T_comp, deg_p180_fm45, 'deg_p180_fm45' )

geompy.addToStudyInFather( T_comp, deg_pm90_f0, 'deg_pm90_f0' )
geompy.addToStudyInFather( T_comp, deg_pm90_f90, 'deg_pm90_f90' )
geompy.addToStudyInFather( T_comp, deg_pm90_f45, 'deg_pm90_f45' )
geompy.addToStudyInFather( T_comp, deg_pm90_fm45, 'deg_pm90_fm45' )

geompy.addToStudyInFather( T_comp, R1, 'R1' )
geompy.addToStudyInFather( T_comp, R2, 'R2' )


###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

T2Mesh = smesh.Mesh(T_comp)

Hexa_3D = T2Mesh.Hexahedron(algo=smeshBuilder.Hexa)

Quadrangle_2D = T2Mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])

thickness_1 = T2Mesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
half_width_1 = T2Mesh.GroupOnGeom(half_width,'half_width',SMESH.EDGE)
width_1 = T2Mesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = T2Mesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = T2Mesh.GroupOnGeom(length,'length',SMESH.EDGE)

deg_p0_f0_1 = T2Mesh.GroupOnGeom(deg_p0_f0,'deg_p0_f0',SMESH.VOLUME)
deg_p0_f90_1 = T2Mesh.GroupOnGeom(deg_p0_f90,'deg_p0_f90',SMESH.VOLUME)
deg_p0_f45_1 = T2Mesh.GroupOnGeom(deg_p0_f45,'deg_p0_f45',SMESH.VOLUME)
deg_p0_fm45_1 = T2Mesh.GroupOnGeom(deg_p0_fm45,'deg_p0_fm45',SMESH.VOLUME)

deg_p90_f0_1 = T2Mesh.GroupOnGeom(deg_p90_f0,'deg_p90_f0',SMESH.VOLUME)
deg_p90_f90_1 = T2Mesh.GroupOnGeom(deg_p90_f90,'deg_p90_f90',SMESH.VOLUME)
deg_p90_f45_1 = T2Mesh.GroupOnGeom(deg_p90_f45,'deg_p90_f45',SMESH.VOLUME)
deg_p90_fm45_1 = T2Mesh.GroupOnGeom(deg_p90_fm45,'deg_p90_fm45',SMESH.VOLUME)

deg_pm90_f0_1 = T2Mesh.GroupOnGeom(deg_pm90_f0,'deg_pm90_f0',SMESH.VOLUME)
deg_pm90_f90_1 = T2Mesh.GroupOnGeom(deg_pm90_f90,'deg_pm90_f90',SMESH.VOLUME)
deg_pm90_f45_1 = T2Mesh.GroupOnGeom(deg_pm90_f45,'deg_pm90_f45',SMESH.VOLUME)
deg_pm90_fm45_1 = T2Mesh.GroupOnGeom(deg_pm90_fm45,'deg_pm90_fm45',SMESH.VOLUME)

deg_p180_f0_1 = T2Mesh.GroupOnGeom(deg_p180_f0,'deg_p180_f0',SMESH.VOLUME)
deg_p180_f90_1 = T2Mesh.GroupOnGeom(deg_p180_f90,'deg_p180_f90',SMESH.VOLUME)
deg_p180_f45_1 = T2Mesh.GroupOnGeom(deg_p180_f45,'deg_p180_f45',SMESH.VOLUME)
deg_p180_fm45_1 = T2Mesh.GroupOnGeom(deg_p180_fm45,'deg_p180_fm45',SMESH.VOLUME)


R1_1 = T2Mesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = T2Mesh.GroupOnGeom(R2,'R2',SMESH.FACE)

Regular_1D_1 = T2Mesh.Segment(geom=thickness)
n_thick = Regular_1D_1.NumberOfSegments(n_thick)

Regular_1D_2 = T2Mesh.Segment(geom=width)
nw = Regular_1D_2.NumberOfSegments(n_width)

Regular_1D_3 = T2Mesh.Segment(geom=half_width)
nw2 = Regular_1D_3.NumberOfSegments(n_width_2)

Regular_1D_4 = T2Mesh.Segment(geom=height)
nh = Regular_1D_4.NumberOfSegments(n_height)

Regular_1D_5 = T2Mesh.Segment(geom=length)
nl = Regular_1D_5.NumberOfSegments(n_length)

isDone = T2Mesh.Compute()

coincident_nodes_on_part = T2Mesh.FindCoincidentNodesOnPart( [ T2Mesh ], 1e-05, [], 0 )
#print(coincident_nodes_on_part)
T2Mesh.MergeNodes(coincident_nodes_on_part, [], 0)

equal_elements = T2Mesh.FindEqualElements( [ T2Mesh ], [] )
T2Mesh.MergeElements(equal_elements, [])


if is_quadratic:
    T2Mesh.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)

R1_2 = T2Mesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = T2Mesh.GroupOnGeom(R2,'R2',SMESH.NODE)

smesh.SetName(T2Mesh.GetMesh(), 'T2Mesh')



try:
    T2Mesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/T2_Bauchau/'+mesh_name+'.med', auto_groups=0, version=40, overwrite=1, meshPart=None, autoDimension=1)
    print('MED file saved')
except:
    print('ExportMED() failed. Invalid file name?')

Sub_mesh_1 = Regular_1D_1.GetSubMesh()
Sub_mesh_2 = Regular_1D_2.GetSubMesh()
Sub_mesh_3 = Regular_1D_3.GetSubMesh()
Sub_mesh_4 = Regular_1D_4.GetSubMesh()
Sub_mesh_5 = Regular_1D_5.GetSubMesh()

## some objects were removed
aStudyBuilder = salome.myStudy.NewBuilder()

## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(nw, 'nw')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nl, 'nl')
smesh.SetName(nw2, 'nw2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(nh, 'nh')
smesh.SetName(R2_1, 'R2')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_5, 'Sub-mesh_5')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')

smesh.SetName(thickness_1, 'thickness')
smesh.SetName(half_width_1, 'half_width')
smesh.SetName(width_1, 'width')
smesh.SetName(height_1, 'height')
smesh.SetName(length_1, 'length')

smesh.SetName(deg_p0_f0_1, 'deg_p0_f0')
smesh.SetName(deg_p0_f45_1, 'deg_p0_f45')
smesh.SetName(deg_p0_fm45_1, 'deg_p0_fm45')
smesh.SetName(deg_p0_f90_1, 'deg_p0_f90')

smesh.SetName(deg_p90_f0_1, 'deg_p90_f0')
smesh.SetName(deg_p90_f45_1, 'deg_p90_f45')
smesh.SetName(deg_p90_fm45_1, 'deg_p90_fm45')
smesh.SetName(deg_p90_f90_1, 'deg_p90_f90')

smesh.SetName(deg_pm90_f0_1, 'deg_pm90_f0')
smesh.SetName(deg_pm90_f45_1, 'deg_pm90_f45')
smesh.SetName(deg_pm90_fm45_1, 'deg_pm90_fm45')
smesh.SetName(deg_pm90_f90_1, 'deg_pm90_f90')

smesh.SetName(deg_p180_f0_1, 'deg_p180_f0')
smesh.SetName(deg_p180_f45_1, 'deg_p180_f45')
smesh.SetName(deg_p180_fm45_1, 'deg_p180_fm45')
smesh.SetName(deg_p180_f90_1, 'deg_p180_f90')

smesh.SetName(R2_2, 'R2')
smesh.SetName(R1_2, 'R1')



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
