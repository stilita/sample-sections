#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.7.0 with dump python functionality
###

import sys
import salome
import os

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/01_MeshGeneration')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()


# inner radius of ring
minor_axis = 0.1

# inner radius of ring
major_axis = 0.2

scale = major_axis / minor_axis

print('Ellipse major axis: {0}'.format(major_axis))
print('Ellipse minor axis: {0}'.format(minor_axis))


# length of ring 
length = 0.5

# number of elements in x-direction (axis of beam)
z_elems = 5

# number of elements for each block
block_elems = 4


# linear or quadratic mesh
quad = True 

# folder to save
folder = 'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/01_MeshGeneration'


O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

Divided_Disk_1 = geompy.MakeDividedDisk(minor_axis, 2, GEOM.HEXAGON)

[Face_1,Face_2,Face_3,Face_4,Face_5,Face_6,Face_7,Face_8,Face_9,Face_10,Face_11,Face_12,Face_13,Face_14,Face_15,Face_16,Face_17,Face_18] = geompy.ExtractShapes(Divided_Disk_1, geompy.ShapeType["FACE"], True)
Shell_1 = geompy.MakeShell([Face_1, Face_2, Face_3, Face_4, Face_5, Face_6, Face_7, Face_8, Face_9, Face_10, Face_11, Face_12, Face_13, Face_14, Face_15, Face_16, Face_17, Face_18])

Scale_1 = geompy.MakeScaleAlongAxes(Shell_1, O, 1, scale, 1)

Extrusion_1 = geompy.MakePrismVecH(Scale_1, OX, 0.5)

all = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["EDGE"])

geompy.UnionIDs(all, [6, 9, 12, 13, 16, 19, 20, 23, 26, 27, 30, 31, 40, 43, 46, 47, 50, 51, 54, 55, 64, 67, 68, 71, 74, 75, 78, 79, 88, 91, 94, 95, 98, 99, 102, 103, 112, 115, 116, 119, 120, 129, 130, 139, 142, 143, 146, 149, 150, 153, 154, 163, 166, 169, 170, 173, 174, 177, 178, 187, 190, 191, 194, 195, 204, 207, 208, 211, 212, 221, 224, 225, 228, 229, 238, 241, 242, 245, 246, 255, 258, 259, 262, 265, 266, 269, 270, 279, 282, 283, 286, 287, 296, 299, 300, 303, 304, 313, 316, 317, 320, 321, 330, 333, 334, 337, 338, 347, 348])

lx_1 = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["EDGE"])

geompy.UnionIDs(lx_1, [313, 330, 238, 279, 262, 204, 166, 221, 187, 112, 91, 88, 146, 139, 43, 71, 9, 23, 64, 6, 16])
geompy.DifferenceIDs(lx_1, [313, 330, 238, 279, 262, 204, 166, 221, 187, 112, 91, 88, 146, 139, 43, 71, 9, 23, 64, 6, 16])
geompy.UnionIDs(lx_1, [313, 330, 238, 279, 262, 204, 166, 221, 187, 112, 91, 88, 146, 139, 43, 71, 9, 23, 64, 6, 16, 163, 40, 255, 296])

sup = geompy.CutListOfGroups([all], [lx_1])

R2 = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["FACE"])

geompy.UnionIDs(R2, [106, 181, 249, 324, 351, 341, 307, 232, 157, 82, 34, 58, 133, 215, 290, 273, 198, 123])

R1 = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["FACE"])

geompy.UnionIDs(R1, [349, 322, 247, 179, 104, 56, 32, 80, 155, 230, 305, 339, 288, 213, 131, 121, 196, 271])

[all, lx_1, sup, R2, R1] = geompy.GetExistingSubObjects(Extrusion_1, False)

allsup = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["FACE"])

geompy.UnionIDs(allsup, [4, 14, 21, 28, 32, 34, 38, 48, 52, 56, 58, 62, 69, 76, 80, 82, 86, 96, 100, 104, 106, 110, 117, 121, 123, 127, 131, 133, 137, 144, 151, 155, 157, 161, 171, 175, 179, 181, 185, 192, 196, 198, 202, 209, 213, 215, 219, 226, 230, 232, 236, 243, 247, 249, 253, 260, 267, 271, 273, 277, 284, 288, 290, 294, 301, 305, 307, 311, 318, 322, 324, 328, 335, 339, 341, 345, 349, 351])

[all, lx_1, sup, R2, R1, allsup] = geompy.GetExistingSubObjects(Extrusion_1, False)

geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Divided_Disk_1, 'Divided Disk_1' )
geompy.addToStudyInFather( Divided_Disk_1, Face_1, 'Face_1' )
geompy.addToStudyInFather( Divided_Disk_1, Face_2, 'Face_2' )
geompy.addToStudyInFather( Divided_Disk_1, Face_3, 'Face_3' )
geompy.addToStudyInFather( Divided_Disk_1, Face_4, 'Face_4' )
geompy.addToStudyInFather( Divided_Disk_1, Face_5, 'Face_5' )
geompy.addToStudyInFather( Divided_Disk_1, Face_6, 'Face_6' )
geompy.addToStudyInFather( Divided_Disk_1, Face_7, 'Face_7' )
geompy.addToStudyInFather( Divided_Disk_1, Face_8, 'Face_8' )
geompy.addToStudyInFather( Divided_Disk_1, Face_9, 'Face_9' )
geompy.addToStudyInFather( Divided_Disk_1, Face_10, 'Face_10' )
geompy.addToStudyInFather( Divided_Disk_1, Face_11, 'Face_11' )
geompy.addToStudyInFather( Divided_Disk_1, Face_12, 'Face_12' )
geompy.addToStudyInFather( Divided_Disk_1, Face_13, 'Face_13' )
geompy.addToStudyInFather( Divided_Disk_1, Face_14, 'Face_14' )
geompy.addToStudyInFather( Divided_Disk_1, Face_15, 'Face_15' )
geompy.addToStudyInFather( Divided_Disk_1, Face_16, 'Face_16' )
geompy.addToStudyInFather( Divided_Disk_1, Face_17, 'Face_17' )
geompy.addToStudyInFather( Divided_Disk_1, Face_18, 'Face_18' )
geompy.addToStudy( Shell_1, 'Shell_1' )
geompy.addToStudy( Scale_1, 'Scale_1' )
geompy.addToStudy( Extrusion_1, 'Extrusion_1' )
geompy.addToStudyInFather( Extrusion_1, all, 'all' )
geompy.addToStudyInFather( Extrusion_1, lx_1, 'lx' )
geompy.addToStudyInFather( Extrusion_1, sup, 'sup' )
geompy.addToStudyInFather( Extrusion_1, R2, 'R2' )
geompy.addToStudyInFather( Extrusion_1, R1, 'R1' )
geompy.addToStudyInFather( Extrusion_1, allsup, 'allsup' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Quadrangle_Parameters_1 = smesh.CreateHypothesis('QuadrangleParams')
Quadrangle_Parameters_1.SetQuadType( smeshBuilder.QUAD_REDUCED )
Quadrangle_Parameters_1.SetTriaVertex( -1 )
Quadrangle_Parameters_1.SetEnforcedNodes( [], [] )

Quadrangle_2D = smesh.CreateHypothesis('Quadrangle_2D')

l1 = smesh.CreateHypothesis('NumberOfSegments')
l1.SetNumberOfSegments( block_elems )

Regular_1D = smesh.CreateHypothesis('Regular_1D')
MG_Hexa = smesh.CreateHypothesis('MG-Hexa', 'HexoticEngine')

lx_2 = smesh.CreateHypothesis('NumberOfSegments')
lx_2.SetNumberOfSegments( z_elems )
Hexa_3D = smesh.CreateHypothesis('Hexa_3D')

Mesh_1 = smesh.Mesh(Extrusion_1)
status = Mesh_1.AddHypothesis(Hexa_3D)
all_1 = Mesh_1.GroupOnGeom(all,'all',SMESH.EDGE)
lx_3 = Mesh_1.GroupOnGeom(lx_1,'lx',SMESH.EDGE)
sup_1 = Mesh_1.GroupOnGeom(sup,'sup',SMESH.EDGE)
R2_1 = Mesh_1.GroupOnGeom(R2,'R2',SMESH.FACE)
R1_1 = Mesh_1.GroupOnGeom(R1,'R1',SMESH.FACE)
allsup_1 = Mesh_1.GroupOnGeom(allsup,'allsup',SMESH.FACE)

status = Mesh_1.AddHypothesis(Quadrangle_2D,allsup)
status = Mesh_1.AddHypothesis(Quadrangle_Parameters_1,allsup)
status = Mesh_1.AddHypothesis(Regular_1D,lx_1)
status = Mesh_1.AddHypothesis(lx_2,lx_1)
status = Mesh_1.AddHypothesis(Regular_1D,sup)
status = Mesh_1.AddHypothesis(l1,sup)

[ all_1, lx_3, sup_1, R2_1, R1_1, allsup_1 ] = Mesh_1.GetGroups()

isDone = Mesh_1.Compute()

[ all_1, lx_3, sup_1, R2_1, R1_1, allsup_1 ] = Mesh_1.GetGroups()

if quad:
    Mesh_1.ConvertToQuadratic(0)
    meshtype = 'C3D20'
else:
    meshtype = 'C3D08'


R2_2 = Mesh_1.GroupOnGeom(R2,'R2',SMESH.NODE)
R1_2 = Mesh_1.GroupOnGeom(R1,'R1',SMESH.NODE)

Sub_mesh_1 = Mesh_1.GetSubMesh( allsup, 'Sub-mesh_1' )
Sub_mesh_2 = Mesh_1.GetSubMesh( lx_1, 'Sub-mesh_2' )
Sub_mesh_3 = Mesh_1.GetSubMesh( sup, 'Sub-mesh_3' )

name = 'Ellipse_{0}_z{1:02d}_block{2:02d}.med'.format(meshtype, z_elems, block_elems) 


if os.path.isdir(folder):
    print('Writing file '+name+' in folder ' + folder)
    act_folder = folder
else:
    print('Folder '+folder+' not found. Using default location: '+os.getcwd())
    act_folder = os.getcwd()
    

print('Full Name = ' + act_folder + '/' + name)

smesh.SetName(Mesh_1, 'Ellipsis')

try:
  Mesh_1.ExportMED(act_folder+'/'+name,auto_groups=0,version=41,overwrite=1,meshPart=None,autoDimension=1)
  pass
except:
  print('ExportMED() failed. Invalid file name?')



## Set names of Mesh objects
smesh.SetName(Quadrangle_2D, 'Quadrangle_2D')
smesh.SetName(MG_Hexa, 'MG-Hexa')
smesh.SetName(Regular_1D, 'Regular_1D')
smesh.SetName(l1, 'l1')
smesh.SetName(Hexa_3D, 'Hexa_3D')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(lx_2, 'lx')
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(R1_2, 'R1')
smesh.SetName(R2_2, 'R2')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(R2_1, 'R2')
smesh.SetName(allsup_1, 'allsup')
smesh.SetName(R1_1, 'R1')
smesh.SetName(all_1, 'all')
smesh.SetName(lx_3, 'lx')
smesh.SetName(sup_1, 'sup')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
