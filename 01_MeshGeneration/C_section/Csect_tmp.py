#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.10.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/C_section')

####################################################
##       Begin of NoteBook variables section      ##
####################################################
notebook.set("max_x", 0.2)
notebook.set("max_y", 0.3)
notebook.set("min_y", -0.3)
notebook.set("dz", 0.5)
####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Vertex_1 = geompy.MakeVertex("max_x", "max_y", 0)
Vertex_2 = geompy.MakeVertex(0, "max_y", 0)
Vertex_3 = geompy.MakeVertex(0, "min_y", 0)
Vertex_4 = geompy.MakeVertex("max_x", "min_y", 0)
Line_1 = geompy.MakeLineTwoPnt(Vertex_1, Vertex_2)
Line_2 = geompy.MakeLineTwoPnt(Vertex_2, Vertex_3)
Line_3 = geompy.MakeLineTwoPnt(Vertex_3, Vertex_4)
Wire_1 = geompy.MakeWire([Line_1, Line_2, Line_3], 1e-07)
C_section = geompy.MakePrismVecH(Wire_1, OZ, "dz")
[z, y, x1, x2] = geompy.Propagate(C_section)
R1 = geompy.CreateGroup(C_section, geompy.ShapeType["EDGE"])
geompy.UnionIDs(R1, [24, 17, 10])
R2 = geompy.CreateGroup(C_section, geompy.ShapeType["EDGE"])
geompy.UnionIDs(R2, [25, 18, 11])
[z, y, x1, x2, R1, R2] = geompy.GetExistingSubObjects(C_section, False)
shell = geompy.CreateGroup(C_section, geompy.ShapeType["FACE"])
geompy.UnionIDs(shell, [2, 12, 19])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Vertex_3, 'Vertex_3' )
geompy.addToStudy( Vertex_4, 'Vertex_4' )
geompy.addToStudy( Line_1, 'Line_1' )
geompy.addToStudy( Line_2, 'Line_2' )
geompy.addToStudy( Line_3, 'Line_3' )
geompy.addToStudy( Wire_1, 'Wire_1' )
geompy.addToStudy( C_section, 'C_section' )
geompy.addToStudyInFather( C_section, z, 'z' )
geompy.addToStudyInFather( C_section, y, 'y' )
geompy.addToStudyInFather( C_section, x1, 'x1' )
geompy.addToStudyInFather( C_section, x2, 'x2' )
geompy.addToStudyInFather( C_section, R1, 'R1' )
geompy.addToStudyInFather( C_section, R2, 'R2' )
geompy.addToStudyInFather( C_section, shell, 'shell' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

C_section_1 = smesh.Mesh(C_section,'C_section')
Quadrangle_2D = C_section_1.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_QUADRANGLE_PREF,None,[],[])
z_1 = C_section_1.GroupOnGeom(z,'z',SMESH.EDGE)
y_1 = C_section_1.GroupOnGeom(y,'y',SMESH.EDGE)
x1_1 = C_section_1.GroupOnGeom(x1,'x1',SMESH.EDGE)
x2_1 = C_section_1.GroupOnGeom(x2,'x2',SMESH.EDGE)
R1_1 = C_section_1.GroupOnGeom(R1,'R1',SMESH.EDGE)
R2_1 = C_section_1.GroupOnGeom(R2,'R2',SMESH.EDGE)
Regular_1D = C_section_1.Segment(geom=z)
nz = Regular_1D.NumberOfSegments(5,None,[])
Regular_1D_1 = C_section_1.Segment(geom=y)
ny = Regular_1D_1.NumberOfSegments(12,None,[])
Regular_1D_2 = C_section_1.Segment(geom=x1)
nx = Regular_1D_2.NumberOfSegments(6,None,[])
Regular_1D_3 = C_section_1.Segment(geom=x2)
status = C_section_1.AddHypothesis(nx,x2)
isDone = C_section_1.Compute()
[ z_1, y_1, x1_1, x2_1, R1_1, R2_1 ] = C_section_1.GetGroups()
R1_2 = C_section_1.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = C_section_1.GroupOnGeom(R2,'R2',SMESH.NODE)
shell_1 = C_section_1.GroupOnGeom(shell,'shell',SMESH.FACE)
C_section_1.ConvertToQuadratic(1)
smesh.SetName(C_section_1, 'C_section')
try:
  C_section_1.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/C_section/Csect_S9R5_x06_y12_z05.med',auto_groups=0,version=41,overwrite=1,meshPart=None,autoDimension=1)
  pass
except:
  print('ExportMED() failed. Invalid file name?')
shell_2 = C_section_1.GroupOnGeom(shell,'shell',SMESH.NODE)
C_section_1.ConvertToQuadratic(0, C_section_1,True)
Sub_mesh_1 = Regular_1D.GetSubMesh()
Sub_mesh_2 = Regular_1D_1.GetSubMesh()
Sub_mesh_3 = Regular_1D_2.GetSubMesh()
Sub_mesh_4 = Regular_1D_3.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(nz, 'nz')
smesh.SetName(ny, 'ny')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(shell_1, 'shell')
smesh.SetName(nx, 'nx')
smesh.SetName(C_section_1.GetMesh(), 'C_section')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')
smesh.SetName(z_1, 'z')
smesh.SetName(x1_1, 'x1')
smesh.SetName(y_1, 'y')
smesh.SetName(R2_2, 'R2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(shell_2, 'shell')
smesh.SetName(x2_1, 'x2')
smesh.SetName(R1_2, 'R1')
smesh.SetName(R2_1, 'R2')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
