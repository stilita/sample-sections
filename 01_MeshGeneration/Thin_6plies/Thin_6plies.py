#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.11.0 with dump python functionality
###

import sys
import salome

import numpy as np

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


width_dir  = 1 # width along y
height_dir = 2 # height along z
axis_dir   = 0 # axis along x, for automatic identification of R1 and R2

axis_name = ['X', 'Y', 'Z']


width = 0.953
height= 0.537
thickness = 0.005
beam_length = 0.5

scale = 1.0

w_scaled = width * scale
h_scaled = height * scale
t_scaled = thickness * scale



# mesh
n_thick = 2  # number of segments in thickness
n_width = 20  # number of segmenst in width 
n_height = 10 # number of segments in height
n_length = 5  # number of blocks

is_quadratic = True
is_biquadratic = False

if is_quadratic:
    mesh_name = "Thin6_{0:02d}w_{1:02d}h_{2:02d}t_hex20".format(n_width, n_height, n_thick)
else:
    mesh_name = "Thin_{0:02d}w_{1:02d}h_{2:02d}t_hex08".format(n_width, n_height, n_thick)



geompy = geomBuilder.New()


O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

vertices = []

n_plies = 6


for i in range(n_plies +1):
    vertices.append(geompy.MakeVertex(0, -w_scaled/2+i*t_scaled, -h_scaled/2+i*t_scaled))
    vertices.append(geompy.MakeVertex(0,  w_scaled/2-i*t_scaled, -h_scaled/2+i*t_scaled))
    vertices.append(geompy.MakeVertex(0,  w_scaled/2-i*t_scaled,  h_scaled/2-i*t_scaled))
    vertices.append(geompy.MakeVertex(0, -w_scaled/2+i*t_scaled,  h_scaled/2-i*t_scaled))


lines = []

for i in range(n_plies+1):
    for j in range(4):
        lines.append(geompy.MakeLineTwoPnt(vertices[i*4+j], vertices[i*4+(j+1)%4]))

lines_skew = []

for i in range(n_plies):
    for j in range(4):
        lines_skew.append(geompy.MakeLineTwoPnt(vertices[i*4+j], vertices[(i+1)*4+j]))

    
faces=[]

for i in range(n_plies):
    for j in range(4):
        #print("i: {0} - j: {1}".format(i,j))
        faces.append(geompy.MakeFaceWires([lines[4*i+j], lines_skew[4*i+(j+1)%4], lines[4*(i+1)+j], lines_skew[4*i+j]], 1))
 
 
solids = []
 
for f in faces:
    solids.append(geompy.MakePrismVecH(f, OX, beam_length))
    
domain = geompy.MakeCompound(solids)
#propagate = geompy.Propagate(domain) 

# create groups of faces
R1 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
R2 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])

SubFacesList = geompy.SubShapeAllSortedCentres(domain, geompy.ShapeType["FACE"])

for act_f in SubFacesList:
    cm = geompy.MakeCDG(act_f)
    if cm is None:
        raise RuntimeError("MakeCDG(face) failed")
    else:
        #print("\nCentre of gravity of face has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        if(np.abs(coords[axis_dir]) < 1e-6):
            print("belongs to R1")
            FaceID = geompy.GetSubShapeID(domain, act_f)
            geompy.UnionIDs(R1, [FaceID])
        elif(np.abs(coords[axis_dir]-beam_length) < 1e-6):
            print("belongs to R2")
            FaceID = geompy.GetSubShapeID(domain, act_f)
            geompy.UnionIDs(R2, [FaceID])

# create groups of lines
thickness = geompy.CreateGroup(domain, geompy.ShapeType["EDGE"])
width = geompy.CreateGroup(domain, geompy.ShapeType["EDGE"])
height = geompy.CreateGroup(domain, geompy.ShapeType["EDGE"])
length = geompy.CreateGroup(domain, geompy.ShapeType["EDGE"])
    

EdgeList = geompy.SubShapeAllSortedCentres(domain, geompy.ShapeType["EDGE"])


for act_e in EdgeList:
    cm = geompy.MakeCDG(act_e)
    if cm is None:
        raise RuntimeError("MakeCDG(edge) failed")
    else:
        #print("\nCentre of gravity of face has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        EdgeID = geompy.GetSubShapeID(domain, act_e)
        if((np.abs(coords[axis_dir]) < 1e-6) or (np.abs(coords[axis_dir]-beam_length) < 1e-6)):
            if(np.abs(coords[height_dir]) < 1e-6):
                print("belongs to height")
                geompy.UnionIDs(height, [EdgeID])
            elif(np.abs(coords[width_dir]) < 1e-6):
                print("belongs to width")
                geompy.UnionIDs(width, [EdgeID])
            else:
                print("belongs to thickness")
                geompy.UnionIDs(thickness, [EdgeID])
        else:
            print("belongs to length")
            geompy.UnionIDs(length, [EdgeID])
        

def insertInArray(pliesArray, coordsArray, currCoords, coordIndex, plyID):
    
    mustAppend = True
    
    if len(pliesArray) == 0:
        pliesArray.append(plyID)
        coordsArray.append(currCoords[coordIndex])
    else:
        for i in range(len(pliesArray)):
            if np.abs(currCoords[coordIndex]) < np.abs(coordsArray[i]):
                pliesArray.insert(i, plyID)
                coordsArray.insert(i, currCoords[coordIndex])
                mustAppend = False
                break
        
        if mustAppend:
            pliesArray.append(plyID)
            coordsArray.append(currCoords[coordIndex])

    print(pliesArray)
    print(coordsArray)
    print(currCoords)
    print(coordIndex)
    print(plyID)


plies0array = []
plies90array = []
plies180array = []
plies270array = []

coords0array = []
coords90array = []
coords180array = []
coords270array = []


SolidList = geompy.SubShapeAllSortedCentres(domain, geompy.ShapeType["SOLID"])

for act_s in SolidList:
    cm = geompy.MakeCDG(act_s)
    if cm is None:
        raise RuntimeError("MakeCDG(edge) failed")
    else:
        #print("\nCentre of gravity of face has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        print("cm: x {0} y {1} z {2}".format(*coords))
        SolidID = geompy.GetSubShapeID(domain, act_s)
        if(np.abs(coords[height_dir]) < 1e-6):
            # print("to 0 180")
            if(coords[width_dir] > w_scaled/4):
                print("belongs to 270")
                insertInArray(plies270array, coords270array, coords, width_dir, SolidID)
            elif(coords[width_dir] < -w_scaled/4):
                print("belongs to 90")
                insertInArray(plies90array, coords90array, coords, width_dir, SolidID)
            else:
                print("height 0 but not found")
        elif(np.abs(coords[width_dir]) < 1e-6):
            print("to +/- 90")
            # print("current considered coord: {0} - Current threshold: {1}".format(coords[height_dir], w_scaled/2))
            if(coords[height_dir] > h_scaled/4):
                print("belongs to 0")
                insertInArray(plies0array, coords0array, coords, height_dir, SolidID)
            elif(coords[height_dir] < -h_scaled/4):
                print("belongs to 180")
                insertInArray(plies180array, coords180array, coords, height_dir, SolidID)
            else:
                print("height 0 but not found")
        else:
            print("cm has coordinates with nonzeros")

Plies0 = []

for cp in plies0array:
    cg = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(cg, [cp])
    Plies0.append(cg)

Plies90 = []

for cp in plies90array:
    cg = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(cg, [cp])
    Plies90.append(cg)

Plies180 = []

for cp in plies180array:
    cg = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(cg, [cp])
    Plies180.append(cg)

Plies270 = []

for cp in plies270array:
    cg = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(cg, [cp])
    Plies270.append(cg)


# print("plies 0")
# print(plies0array)
# print(coords0array)

# print("plies 90")
# print(plies90array)
# print(coords90array)

# print("plies 180")
# print(plies180array)
# print(coords180array)

# print("plies 270")
# print(plies270array)
# print(coords270array)



#Box1.SetColor(SALOMEDS.Color(0.333333,1,0))
#Box2.SetColor(SALOMEDS.Color(0.666667,1,1))
#Box3.SetColor(SALOMEDS.Color(1,0.666667,0))
#Box4.SetColor(SALOMEDS.Color(0.666667,0,1))


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for i, v in enumerate(vertices):
#    geompy.addToStudy( v, f'Vertex_{i}' )

#for i, l in enumerate(lines):
#    geompy.addToStudy( l, f'Line_{i}' )

#for i, l in enumerate(lines_skew):
#    geompy.addToStudy( l, f'Line_skew_{i}' )


#for i, f in enumerate(faces):
#    geompy.addToStudy( f, f'Face_{i}' )
    
#for i, s in enumerate(solids):
#    geompy.addToStudy( s, f'Solid_{i}' )

 
geompy.addToStudy( domain, 'domain' )
geompy.addToStudyInFather( domain, thickness, 'thickness' )
#geompy.addToStudyInFather( domain, half_width, 'half_width' )
geompy.addToStudyInFather( domain, width, 'width' )
geompy.addToStudyInFather( domain, height, 'height' )
geompy.addToStudyInFather( domain, length, 'length' )

for i, cp in enumerate(Plies0):
    geompy.addToStudyInFather( domain, cp, f'Ply_0d_{i}' )

for i, cp in enumerate(Plies90):
    geompy.addToStudyInFather( domain, cp, f'Ply_90d_{i}' )

for i, cp in enumerate(Plies180):
    geompy.addToStudyInFather( domain, cp, f'Ply_180d_{i}' )

for i, cp in enumerate(Plies270):
    geompy.addToStudyInFather( domain, cp, f'Ply_270d_{i}' )

#geompy.addToStudyInFather( domain, Box2, 'Box2' )
#geompy.addToStudyInFather( domain, Box3, 'Box3' )
#geompy.addToStudyInFather( domain, Box4, 'Box4' )
geompy.addToStudyInFather( domain, R1, 'R1' )
geompy.addToStudyInFather( domain, R2, 'R2' )


#for i, p in enumerate(propagate):
#    geompy.addToStudyInFather( domain, p, f'p_{i}' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

HRectMesh = smesh.Mesh(domain)

Hexa_3D = HRectMesh.Hexahedron(algo=smeshBuilder.Hexa)

Regular_1D = HRectMesh.Segment()
generic = Regular_1D.NumberOfSegments(15)


Quadrangle_2D = HRectMesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])

thickness_1 = HRectMesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
#half_width_1 = HRectMesh.GroupOnGeom(half_width,'half_width',SMESH.EDGE)
width_1 = HRectMesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = HRectMesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = HRectMesh.GroupOnGeom(length,'length',SMESH.EDGE)

R1_1 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.FACE)


R1_2 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.NODE)

Plies0_mesh = []
Plies90_mesh = []
Plies180_mesh = []
Plies270_mesh = []

for i, cp in enumerate(Plies0):
    Plies0_mesh.append(HRectMesh.GroupOnGeom(cp,f'Ply_m_0d_{i}',SMESH.VOLUME))

for i, cp in enumerate(Plies90):
    Plies90_mesh.append(HRectMesh.GroupOnGeom(cp,f'Ply_m_90d_{i}',SMESH.VOLUME))

for i, cp in enumerate(Plies180):
    Plies180_mesh.append(HRectMesh.GroupOnGeom(cp,f'Ply_m_180d_{i}',SMESH.VOLUME))

for i, cp in enumerate(Plies270):
    Plies270_mesh.append(HRectMesh.GroupOnGeom(cp,f'Ply_m_270d_{i}',SMESH.VOLUME))


Regular_1D_1 = HRectMesh.Segment(geom=thickness)
n_thick = Regular_1D_1.NumberOfSegments(n_thick)

Regular_1D_2 = HRectMesh.Segment(geom=width)
nw = Regular_1D_2.NumberOfSegments(n_width)

#Regular_1D_3 = HRectMesh.Segment(geom=half_width)
#nw2 = Regular_1D_3.NumberOfSegments(n_width_2)

Regular_1D_4 = HRectMesh.Segment(geom=height)
nh = Regular_1D_4.NumberOfSegments(n_height)

Regular_1D_5 = HRectMesh.Segment(geom=length)
nl = Regular_1D_5.NumberOfSegments(n_length)

isDone = HRectMesh.Compute()

coincident_nodes_on_part = HRectMesh.FindCoincidentNodesOnPart( [ HRectMesh ], 1e-05, [], 0 )
#print(coincident_nodes_on_part)
HRectMesh.MergeNodes(coincident_nodes_on_part, [], 0)

equal_elements = HRectMesh.FindEqualElements( [ HRectMesh ], [] )
HRectMesh.MergeElements(equal_elements, [])

if is_quadratic:
    HRectMesh.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)


smesh.SetName(HRectMesh.GetMesh(), 'Thin_6plies')
try:
    HRectMesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_6plies/'+mesh_name+'.med', auto_groups=0, version=40, overwrite=1, meshPart=None, autoDimension=1)
    print('MED file saved')
except:
    print('ExportMED() failed. Invalid file name?')


## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(nw, 'nw')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nl, 'nl')
#smesh.SetName(nw2, 'nw2')
smesh.SetName(nh, 'nh')
smesh.SetName(R1_1, 'R1')
smesh.SetName(R2_1, 'R2')

smesh.SetName(R1_2, 'R1')
smesh.SetName(R2_2, 'R2')

smesh.SetName(thickness_1, 'thickness')
#smesh.SetName(half_width_1, 'half_width')
smesh.SetName(width_1, 'width')
smesh.SetName(height_1, 'height')
smesh.SetName(length_1, 'length')

for i, cp in enumerate(Plies0_mesh):
    smesh.SetName(cp, f'Ply_m_0d_{i}')

for i, cp in enumerate(Plies90_mesh):
    smesh.SetName(cp, f'Ply_m_90d_{i}')

for i, cp in enumerate(Plies180_mesh):
    smesh.SetName(cp, f'Ply_m_180d_{i}')

for i, cp in enumerate(Plies270_mesh):
    smesh.SetName(cp, f'Ply_m_270d_{i}')

    

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
