#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.11.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

width = 0.48
height= 0.24
thickness = 0.05
length = 0.2

# mesh
n_thick = 5   # number of segments in thickness
n_width = 16  # number of segmenst in width (has to be even)
n_height = 8 # number of segments in height
n_length = 5  # number of blocks

is_quadratic = True
is_biquadratic = False

if is_quadratic:
    mesh_name = "HRec_{0:02d}w_{1:02d}h_{2:02d}t_hex20".format(n_width, n_height, n_thick)
else:
    mesh_name = "HRec_{0:02d}w_{1:02d}h_{2:02d}t_hex08".format(n_width, n_height, n_thick)



geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

vertices = []

vertices.append(geompy.MakeVertex(-width/2, -height/2, 0))
vertices.append(geompy.MakeVertex( width/2, -height/2, 0))
vertices.append(geompy.MakeVertex( width/2,  height/2, 0))
vertices.append(geompy.MakeVertex(-width/2,  height/2, 0))

vertices.append(geompy.MakeVertex(-width/2+thickness, -height/2+thickness, 0))
vertices.append(geompy.MakeVertex( width/2-thickness, -height/2+thickness, 0))
vertices.append(geompy.MakeVertex( width/2-thickness,  height/2-thickness, 0))
vertices.append(geompy.MakeVertex(-width/2+thickness,  height/2-thickness, 0))


lines = []

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[(i+1)%4]))

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[(i+1)%4+4]))

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+4]))
    
faces=[]

for i in range(4):
    if i == 3:
        last = 8
    else:
        last = i+9
    faces.append(geompy.MakeFaceWires([lines[i], lines[last], lines[i+4], lines[i+8]], 1))
 
 
solids = []
 
for f in faces:
    solids.append(geompy.MakePrismVecH(f, OZ, length))
    
domain = geompy.MakeCompound(solids)
propagate = geompy.Propagate(domain) 

thick_ind = [4, 5, 6, 11]

#half_width_ind = []

width_ind = [7, 8]

height_ind = [2, 10]

all_ind = list(range(len(propagate)))

print(all_ind)

length_ind = []

for i in all_ind:
    if i not in thick_ind and i not in width_ind and i not in height_ind:
        length_ind.append(i)



thickness = geompy.UnionListOfGroups([propagate[i] for i in thick_ind])
width = geompy.UnionListOfGroups([propagate[i] for i in width_ind])
height = geompy.UnionListOfGroups([propagate[i] for i in height_ind])
length = geompy.UnionListOfGroups([propagate[i] for i in length_ind])

Box1 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box1, [2])
Box2 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box2, [36])
Box3 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box3, [70])
Box4 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box4, [104])
R1 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [66, 100, 134, 32])
R2 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [68, 102, 136, 34])


Box1.SetColor(SALOMEDS.Color(0.333333,1,0))
Box2.SetColor(SALOMEDS.Color(0.666667,1,1))
Box3.SetColor(SALOMEDS.Color(1,0.666667,0))
Box4.SetColor(SALOMEDS.Color(0.666667,0,1))


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for i, v in enumerate(vertices):
#    geompy.addToStudy( v, f'Vertex_{i}' )

#for i, l in enumerate(lines):
#    geompy.addToStudy( l, f'Line_{i}' )

#for i, f in enumerate(faces):
#    geompy.addToStudy( f, f'Face_{i}' )
    
#for i, s in enumerate(solids):
#    geompy.addToStudy( s, f'Solid_{i}' )

 
geompy.addToStudy( domain, 'domain' )
geompy.addToStudyInFather( domain, thickness, 'thickness' )
#geompy.addToStudyInFather( domain, half_width, 'half_width' )
geompy.addToStudyInFather( domain, width, 'width' )
geompy.addToStudyInFather( domain, height, 'height' )
geompy.addToStudyInFather( domain, length, 'length' )
geompy.addToStudyInFather( domain, Box1, 'Box1' )
geompy.addToStudyInFather( domain, Box2, 'Box2' )
geompy.addToStudyInFather( domain, Box3, 'Box3' )
geompy.addToStudyInFather( domain, Box4, 'Box4' )
geompy.addToStudyInFather( domain, R1, 'R1' )
geompy.addToStudyInFather( domain, R2, 'R2' )


#for i, p in enumerate(propagate):
#    geompy.addToStudyInFather( domain, p, f'p_{i}' )

###
### SMESH component
###



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
