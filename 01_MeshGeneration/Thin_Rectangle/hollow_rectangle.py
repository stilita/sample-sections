#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.11.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

width = 0.48
height= 0.24
thickness = 0.05
length = 0.5

# mesh
n_thick = 10  # number of segments in thickness
n_width = 24  # number of segmenst in width 
n_height = 12 # number of segments in height
n_length = 5  # number of blocks

is_quadratic = True
is_biquadratic = False

if is_quadratic:
    mesh_name = "HRec_{0:02d}w_{1:02d}h_{2:02d}t_hex20".format(n_width, n_height, n_thick)
else:
    mesh_name = "HRec_{0:02d}w_{1:02d}h_{2:02d}t_hex08".format(n_width, n_height, n_thick)



geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

vertices = []

vertices.append(geompy.MakeVertex(0, -width/2, -height/2))
vertices.append(geompy.MakeVertex(0,  width/2, -height/2))
vertices.append(geompy.MakeVertex(0,  width/2,  height/2))
vertices.append(geompy.MakeVertex(0, -width/2,  height/2))

vertices.append(geompy.MakeVertex(0, -width/2+thickness, -height/2+thickness))
vertices.append(geompy.MakeVertex(0,  width/2-thickness, -height/2+thickness))
vertices.append(geompy.MakeVertex(0,  width/2-thickness,  height/2-thickness))
vertices.append(geompy.MakeVertex(0, -width/2+thickness,  height/2-thickness))


lines = []

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[(i+1)%4]))

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[(i+1)%4+4]))

for i in range(4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+4]))
    
faces=[]

for i in range(4):
    if i == 3:
        last = 8
    else:
        last = i+9
    faces.append(geompy.MakeFaceWires([lines[i], lines[last], lines[i+4], lines[i+8]], 1))
 
 
solids = []
 
for f in faces:
    solids.append(geompy.MakePrismVecH(f, OX, length))
    
domain = geompy.MakeCompound(solids)
propagate = geompy.Propagate(domain) 

thick_ind = [1, 2, 3, 7]

#half_width_ind = [1]

width_ind = [4, 5]

height_ind = [0, 6]

all_ind = list(range(len(propagate)))

length_ind = [8, 9, 10, 11]

#for i in all_ind:
#    if i not in thick_ind and i not in width_ind and i not in height_ind:
#        length_ind.append(i)



thickness = geompy.UnionListOfGroups([propagate[i] for i in thick_ind])
width = geompy.UnionListOfGroups([propagate[i] for i in width_ind])
height = geompy.UnionListOfGroups([propagate[i] for i in height_ind])
length = geompy.UnionListOfGroups([propagate[i] for i in length_ind])

Box1 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box1, [2])
Box2 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box2, [36])
Box3 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box3, [70])
Box4 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box4, [104])
R1 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [66, 100, 134, 32])
R2 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [68, 102, 136, 34])


Box1.SetColor(SALOMEDS.Color(0.333333,1,0))
Box2.SetColor(SALOMEDS.Color(0.666667,1,1))
Box3.SetColor(SALOMEDS.Color(1,0.666667,0))
Box4.SetColor(SALOMEDS.Color(0.666667,0,1))


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for i, v in enumerate(vertices):
#    geompy.addToStudy( v, f'Vertex_{i}' )

#for i, l in enumerate(lines):
#    geompy.addToStudy( l, f'Line_{i}' )

#for i, f in enumerate(faces):
#    geompy.addToStudy( f, f'Face_{i}' )
    
#for i, s in enumerate(solids):
#    geompy.addToStudy( s, f'Solid_{i}' )

 
geompy.addToStudy( domain, 'domain' )
geompy.addToStudyInFather( domain, thickness, 'thickness' )
#geompy.addToStudyInFather( domain, half_width, 'half_width' )
geompy.addToStudyInFather( domain, width, 'width' )
geompy.addToStudyInFather( domain, height, 'height' )
geompy.addToStudyInFather( domain, length, 'length' )
geompy.addToStudyInFather( domain, Box1, 'Box1' )
geompy.addToStudyInFather( domain, Box2, 'Box2' )
geompy.addToStudyInFather( domain, Box3, 'Box3' )
geompy.addToStudyInFather( domain, Box4, 'Box4' )
geompy.addToStudyInFather( domain, R1, 'R1' )
geompy.addToStudyInFather( domain, R2, 'R2' )


#for i, p in enumerate(propagate):
#    geompy.addToStudyInFather( domain, p, f'p_{i}' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

HRectMesh = smesh.Mesh(domain)

Hexa_3D = HRectMesh.Hexahedron(algo=smeshBuilder.Hexa)

Regular_1D = HRectMesh.Segment()
generic = Regular_1D.NumberOfSegments(15)


Quadrangle_2D = HRectMesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])

thickness_1 = HRectMesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
#half_width_1 = HRectMesh.GroupOnGeom(half_width,'half_width',SMESH.EDGE)
width_1 = HRectMesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = HRectMesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = HRectMesh.GroupOnGeom(length,'length',SMESH.EDGE)

Box1_1 = HRectMesh.GroupOnGeom(Box1,'Box1',SMESH.VOLUME)
Box2_1 = HRectMesh.GroupOnGeom(Box2,'Box2',SMESH.VOLUME)
Box3_1 = HRectMesh.GroupOnGeom(Box3,'Box3',SMESH.VOLUME)
Box4_1 = HRectMesh.GroupOnGeom(Box4,'Box4',SMESH.VOLUME)



R1_1 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.FACE)

Regular_1D_1 = HRectMesh.Segment(geom=thickness)
n_thick = Regular_1D_1.NumberOfSegments(n_thick)

Regular_1D_2 = HRectMesh.Segment(geom=width)
nw = Regular_1D_2.NumberOfSegments(n_width)

#Regular_1D_3 = HRectMesh.Segment(geom=half_width)
#nw2 = Regular_1D_3.NumberOfSegments(n_width_2)

Regular_1D_4 = HRectMesh.Segment(geom=height)
nh = Regular_1D_4.NumberOfSegments(n_height)

Regular_1D_5 = HRectMesh.Segment(geom=length)
nl = Regular_1D_5.NumberOfSegments(n_length)

isDone = HRectMesh.Compute()

coincident_nodes_on_part = HRectMesh.FindCoincidentNodesOnPart( [ HRectMesh ], 1e-05, [], 0 )
#print(coincident_nodes_on_part)
HRectMesh.MergeNodes(coincident_nodes_on_part, [], 0)

equal_elements = HRectMesh.FindEqualElements( [ HRectMesh ], [] )
HRectMesh.MergeElements(equal_elements, [])

if is_quadratic:
    HRectMesh.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)

R1_2 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.NODE)

smesh.SetName(HRectMesh.GetMesh(), 'HRectMesh')
try:
    HRectMesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle/'+mesh_name+'.med', auto_groups=0, version=40, overwrite=1, meshPart=None, autoDimension=1)
    print('MED file saved')
except:
    print('ExportMED() failed. Invalid file name?')

Sub_mesh_1 = Regular_1D_1.GetSubMesh()
Sub_mesh_2 = Regular_1D_2.GetSubMesh()
#Sub_mesh_3 = Regular_1D_3.GetSubMesh()
Sub_mesh_4 = Regular_1D_4.GetSubMesh()
Sub_mesh_5 = Regular_1D_5.GetSubMesh()

## some objects were removed
aStudyBuilder = salome.myStudy.NewBuilder()

## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(nw, 'nw')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nl, 'nl')
#smesh.SetName(nw2, 'nw2')
smesh.SetName(nh, 'nh')
smesh.SetName(R1_1, 'R1')
smesh.SetName(R2_1, 'R2')
#smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_5, 'Sub-mesh_5')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')

smesh.SetName(thickness_1, 'thickness')
#smesh.SetName(half_width_1, 'half_width')
smesh.SetName(width_1, 'width')
smesh.SetName(height_1, 'height')
smesh.SetName(length_1, 'length')

smesh.SetName(Box1_1, 'Box1')
smesh.SetName(Box2_1, 'Box2')
smesh.SetName(Box3_1, 'Box3')
smesh.SetName(Box4_1, 'Box4')

smesh.SetName(R1_2, 'R1')
smesh.SetName(R2_2, 'R2')
    

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
