#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.11.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeVertex(-0.24, -0.12, 0)
geomObj_2 = geompy.MakeVertex(0.24, -0.12, 0)
geomObj_3 = geompy.MakeVertex(0.24, 0.12, 0)
geomObj_4 = geompy.MakeVertex(-0.24, 0.12, 0)
geomObj_5 = geompy.MakeVertex(-0.19, -0.06999999999999999, 0)
geomObj_6 = geompy.MakeVertex(0.19, -0.06999999999999999, 0)
geomObj_7 = geompy.MakeVertex(0.19, 0.06999999999999999, 0)
geomObj_8 = geompy.MakeVertex(-0.19, 0.06999999999999999, 0)
geomObj_9 = geompy.MakeLineTwoPnt(geomObj_1, geomObj_2)
geomObj_10 = geompy.MakeLineTwoPnt(geomObj_2, geomObj_3)
geomObj_11 = geompy.MakeLineTwoPnt(geomObj_3, geomObj_4)
geomObj_12 = geompy.MakeLineTwoPnt(geomObj_4, geomObj_1)
geomObj_13 = geompy.MakeLineTwoPnt(geomObj_5, geomObj_6)
geomObj_14 = geompy.MakeLineTwoPnt(geomObj_6, geomObj_7)
geomObj_15 = geompy.MakeLineTwoPnt(geomObj_7, geomObj_8)
geomObj_16 = geompy.MakeLineTwoPnt(geomObj_8, geomObj_5)
geomObj_17 = geompy.MakeLineTwoPnt(geomObj_1, geomObj_5)
geomObj_18 = geompy.MakeLineTwoPnt(geomObj_2, geomObj_6)
geomObj_19 = geompy.MakeLineTwoPnt(geomObj_3, geomObj_7)
geomObj_20 = geompy.MakeLineTwoPnt(geomObj_4, geomObj_8)
geomObj_21 = geompy.MakeFaceWires([geomObj_9, geomObj_18, geomObj_13, geomObj_17], 1)
geomObj_22 = geompy.MakeFaceWires([geomObj_10, geomObj_19, geomObj_14, geomObj_18], 1)
geomObj_23 = geompy.MakeFaceWires([geomObj_11, geomObj_20, geomObj_15, geomObj_19], 1)
geomObj_24 = geompy.MakeFaceWires([geomObj_12, geomObj_17, geomObj_16, geomObj_20], 1)
geomObj_25 = geompy.MakePrismVecH(geomObj_21, OZ, 0.2)
geomObj_26 = geompy.MakePrismVecH(geomObj_22, OZ, 0.2)
geomObj_27 = geompy.MakePrismVecH(geomObj_23, OZ, 0.2)
geomObj_28 = geompy.MakePrismVecH(geomObj_24, OZ, 0.2)
domain = geompy.MakeCompound([geomObj_25, geomObj_26, geomObj_27, geomObj_28])
[geomObj_29, geomObj_30, geomObj_31, geomObj_32, geomObj_33, geomObj_34, geomObj_35, geomObj_36, geomObj_37, geomObj_38, geomObj_39, geomObj_40] = geompy.Propagate(domain)
thickness = geompy.UnionListOfGroups([geomObj_33, geomObj_34, geomObj_35, geomObj_40])
width = geompy.UnionListOfGroups([geomObj_36, geomObj_37])
height = geompy.UnionListOfGroups([geomObj_31, geomObj_39])
length = geompy.UnionListOfGroups([geomObj_29, geomObj_32, geomObj_38])
Box1 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box1, [2])
Box2 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box2, [36])
Box3 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box3, [70])
Box4 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box4, [104])
R1 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [66, 100, 134, 32])
R2 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [68, 102, 136, 34])
Box1.SetColor(SALOMEDS.Color(0.333333,1,0))
Box2.SetColor(SALOMEDS.Color(0.666667,1,1))
Box3.SetColor(SALOMEDS.Color(1,0.666667,0))
Box4.SetColor(SALOMEDS.Color(0.666667,0,1))
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( domain, 'domain' )
geompy.addToStudyInFather( domain, thickness, 'thickness' )
geompy.addToStudyInFather( domain, width, 'width' )
geompy.addToStudyInFather( domain, height, 'height' )
geompy.addToStudyInFather( domain, length, 'length' )
geompy.addToStudyInFather( domain, Box1, 'Box1' )
geompy.addToStudyInFather( domain, Box2, 'Box2' )
geompy.addToStudyInFather( domain, Box3, 'Box3' )
geompy.addToStudyInFather( domain, Box4, 'Box4' )
geompy.addToStudyInFather( domain, R1, 'R1' )
geompy.addToStudyInFather( domain, R2, 'R2' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

HRectMesh = smesh.Mesh(domain,'HRectMesh')
Hexa_3D = HRectMesh.Hexahedron(algo=smeshBuilder.Hexa)
Quadrangle_2D = HRectMesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,None,[],[])
thickness_1 = HRectMesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
width_1 = HRectMesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = HRectMesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = HRectMesh.GroupOnGeom(length,'length',SMESH.EDGE)
Box1_1 = HRectMesh.GroupOnGeom(Box1,'Box1',SMESH.VOLUME)
Box2_1 = HRectMesh.GroupOnGeom(Box2,'Box2',SMESH.VOLUME)
Box3_1 = HRectMesh.GroupOnGeom(Box3,'Box3',SMESH.VOLUME)
Box4_1 = HRectMesh.GroupOnGeom(Box4,'Box4',SMESH.VOLUME)
R1_1 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.FACE)
#status = HRectMesh.AddHypothesis(n_thick,thickness) ### n_thick has not been yet created
#n_thick.SetNumberOfSegments( 5 ) ### not created Object
#n_thick.SetReversedEdges( [] ) ### not created Object
#n_thick.SetObjectEntry( "domain" ) ### not created Object
nw = smesh.CreateHypothesis('NumberOfSegments')
status = HRectMesh.AddHypothesis(nw,width)
nw.SetNumberOfSegments( 16 )
nw.SetReversedEdges( [] )
nw.SetObjectEntry( "domain" )
nh = smesh.CreateHypothesis('NumberOfSegments')
status = HRectMesh.AddHypothesis(nh,height)
nh.SetNumberOfSegments( 8 )
nh.SetReversedEdges( [] )
nh.SetObjectEntry( "domain" )
nl = smesh.CreateHypothesis('NumberOfSegments')
status = HRectMesh.AddHypothesis(nl,length)
nl.SetNumberOfSegments( 5 )
nl.SetReversedEdges( [] )
nl.SetObjectEntry( "domain" )
R1_2 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.NODE)
smesh.SetName(HRectMesh, 'HRectMesh')
try:
  HRectMesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle/HRec_16w_08h_05t_hex20.med',auto_groups=0,version=40,overwrite=1,meshPart=None,autoDimension=1)
  pass
except:
  print('ExportMED() failed. Invalid file name?')
R1_2.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
R2_2.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
thickness_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
width_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
height_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
length_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
[ thickness_1, width_1, height_1, length_1, Box1_1, Box2_1, Box3_1, Box4_1, R1_1, R2_1, R1_2, R2_2 ] = HRectMesh.GetGroups()
Regular_1D = HRectMesh.Segment()
n_thick = Regular_1D.NumberOfSegments(5,None,[])
isDone = HRectMesh.Compute()
[ thickness_1, width_1, height_1, length_1, Box1_1, Box2_1, Box3_1, Box4_1, R1_1, R2_1, R1_2, R2_2 ] = HRectMesh.GetGroups()
Sub_mesh_1 = HRectMesh.GetSubMesh( thickness, 'Regular_1D' )
Sub_mesh_2 = HRectMesh.GetSubMesh( width, 'Regular_1D' )
Sub_mesh_4 = HRectMesh.GetSubMesh( height, 'Regular_1D' )
Sub_mesh_5 = HRectMesh.GetSubMesh( length, 'Regular_1D' )


## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(nw, 'nw')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nh, 'nh')
smesh.SetName(R1_1, 'R1')
smesh.SetName(nl, 'nl')
smesh.SetName(R2_1, 'R2')
smesh.SetName(HRectMesh.GetMesh(), 'HRectMesh')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_5, 'Sub-mesh_5')
smesh.SetName(Box4_1, 'Box4')
smesh.SetName(Box3_1, 'Box3')
smesh.SetName(Box2_1, 'Box2')
smesh.SetName(Box1_1, 'Box1')
smesh.SetName(thickness_1, 'thickness')
smesh.SetName(height_1, 'height')
smesh.SetName(width_1, 'width')
smesh.SetName(R2_2, 'R2')
smesh.SetName(length_1, 'length')
smesh.SetName(R1_2, 'R1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
