#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.12.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_1 = geompy.MakeVertex(0, -0.24, -0.12)
geomObj_2 = geompy.MakeVertex(0, 0.24, -0.12)
geomObj_3 = geompy.MakeVertex(0, 0.24, 0.12)
geomObj_4 = geompy.MakeVertex(0, -0.24, 0.12)
geomObj_5 = geompy.MakeVertex(0, -0.19, -0.06999999999999999)
geomObj_6 = geompy.MakeVertex(0, 0.19, -0.06999999999999999)
geomObj_7 = geompy.MakeVertex(0, 0.19, 0.06999999999999999)
geomObj_8 = geompy.MakeVertex(0, -0.19, 0.06999999999999999)
geomObj_9 = geompy.MakeLineTwoPnt(geomObj_1, geomObj_2)
geomObj_10 = geompy.MakeLineTwoPnt(geomObj_2, geomObj_3)
geomObj_11 = geompy.MakeLineTwoPnt(geomObj_3, geomObj_4)
geomObj_12 = geompy.MakeLineTwoPnt(geomObj_4, geomObj_1)
geomObj_13 = geompy.MakeLineTwoPnt(geomObj_5, geomObj_6)
geomObj_14 = geompy.MakeLineTwoPnt(geomObj_6, geomObj_7)
geomObj_15 = geompy.MakeLineTwoPnt(geomObj_7, geomObj_8)
geomObj_16 = geompy.MakeLineTwoPnt(geomObj_8, geomObj_5)
geomObj_17 = geompy.MakeLineTwoPnt(geomObj_1, geomObj_5)
geomObj_18 = geompy.MakeLineTwoPnt(geomObj_2, geomObj_6)
geomObj_19 = geompy.MakeLineTwoPnt(geomObj_3, geomObj_7)
geomObj_20 = geompy.MakeLineTwoPnt(geomObj_4, geomObj_8)
geomObj_21 = geompy.MakeFaceWires([geomObj_9, geomObj_18, geomObj_13, geomObj_17], 1)
geomObj_22 = geompy.MakeFaceWires([geomObj_10, geomObj_19, geomObj_14, geomObj_18], 1)
geomObj_23 = geompy.MakeFaceWires([geomObj_11, geomObj_20, geomObj_15, geomObj_19], 1)
geomObj_24 = geompy.MakeFaceWires([geomObj_12, geomObj_17, geomObj_16, geomObj_20], 1)
geomObj_25 = geompy.MakePrismVecH(geomObj_21, OX, 0.5)
geomObj_26 = geompy.MakePrismVecH(geomObj_22, OX, 0.5)
geomObj_27 = geompy.MakePrismVecH(geomObj_23, OX, 0.5)
geomObj_28 = geompy.MakePrismVecH(geomObj_24, OX, 0.5)
domain = geompy.MakeCompound([geomObj_25, geomObj_26, geomObj_27, geomObj_28])
[geomObj_29, geomObj_30, geomObj_31, geomObj_32, geomObj_33, geomObj_34, geomObj_35, geomObj_36, geomObj_37, geomObj_38, geomObj_39, geomObj_40] = geompy.Propagate(domain)
thickness = geompy.UnionListOfGroups([geomObj_30, geomObj_31, geomObj_32, geomObj_36])
width = geompy.UnionListOfGroups([geomObj_33, geomObj_34])
height = geompy.UnionListOfGroups([geomObj_29, geomObj_35])
length = geompy.UnionListOfGroups([geomObj_37, geomObj_38, geomObj_39, geomObj_40])
Box1 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box1, [2])
Box2 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box2, [36])
Box3 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box3, [70])
Box4 = geompy.CreateGroup(domain, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box4, [104])
R1 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [66, 100, 134, 32])
R2 = geompy.CreateGroup(domain, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [68, 102, 136, 34])
Box1.SetColor(SALOMEDS.Color(0.333333,1,0))
Box2.SetColor(SALOMEDS.Color(0.666667,1,1))
Box3.SetColor(SALOMEDS.Color(1,0.666667,0))
Box4.SetColor(SALOMEDS.Color(0.666667,0,1))
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( domain, 'domain' )
geompy.addToStudyInFather( domain, thickness, 'thickness' )
geompy.addToStudyInFather( domain, width, 'width' )
geompy.addToStudyInFather( domain, height, 'height' )
geompy.addToStudyInFather( domain, length, 'length' )
geompy.addToStudyInFather( domain, Box1, 'Box1' )
geompy.addToStudyInFather( domain, Box2, 'Box2' )
geompy.addToStudyInFather( domain, Box3, 'Box3' )
geompy.addToStudyInFather( domain, Box4, 'Box4' )
geompy.addToStudyInFather( domain, R1, 'R1' )
geompy.addToStudyInFather( domain, R2, 'R2' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

HRectMesh = smesh.Mesh(domain,'HRectMesh')
Hexa_3D = HRectMesh.Hexahedron(algo=smeshBuilder.Hexa)
Regular_1D_2 = HRectMesh.Segment()
NumberOfSegments_15 = Regular_1D_2.NumberOfSegments(15,None,[])
Quadrangle_2D = HRectMesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,None,[],[])
thickness_1 = HRectMesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
width_1 = HRectMesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = HRectMesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = HRectMesh.GroupOnGeom(length,'length',SMESH.EDGE)
Box1_1 = HRectMesh.GroupOnGeom(Box1,'Box1',SMESH.VOLUME)
Box2_1 = HRectMesh.GroupOnGeom(Box2,'Box2',SMESH.VOLUME)
Box3_1 = HRectMesh.GroupOnGeom(Box3,'Box3',SMESH.VOLUME)
Box4_1 = HRectMesh.GroupOnGeom(Box4,'Box4',SMESH.VOLUME)
R1_1 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.FACE)
Regular_1D_2_1 = HRectMesh.Segment(geom=thickness)
n_thick = Regular_1D_2_1.NumberOfSegments(15,None,[])
Regular_1D_2_2 = HRectMesh.Segment(geom=width)
nw = Regular_1D_2_2.NumberOfSegments(36,None,[])
Regular_1D_2_3 = HRectMesh.Segment(geom=height)
nh = Regular_1D_2_3.NumberOfSegments(18,None,[])
Regular_1D_2_4 = HRectMesh.Segment(geom=length)
nl = Regular_1D_2_4.NumberOfSegments(5,None,[])
isDone = HRectMesh.Compute()
coincident_nodes_on_part = HRectMesh.FindCoincidentNodesOnPart( [ HRectMesh ], 1e-05, [], 0 )
HRectMesh.MergeNodes([[ 1, 27 ], [ 2, 28 ], [ 3, 9 ], [ 4, 10 ], [ 5, 15 ], [ 6, 16 ], [ 7, 29 ], [ 8, 30 ], [ 11, 17 ], [ 12, 18 ], [ 13, 23 ], [ 14, 24 ], [ 19, 25 ], [ 20, 26 ], [ 21, 31 ], [ 22, 32 ], [ 33, 601 ], [ 34, 602 ], [ 35, 603 ], [ 36, 604 ], [ 37, 245 ], [ 38, 246 ], [ 39, 247 ], [ 40, 248 ], [ 111, 319 ], [ 112, 320 ], [ 113, 321 ], [ 114, 322 ], [ 115, 357 ], [ 116, 358 ], [ 117, 359 ], [ 118, 360 ], [ 119, 361 ], [ 120, 362 ], [ 121, 363 ], [ 122, 364 ], [ 123, 365 ], [ 124, 366 ], [ 125, 367 ], [ 126, 368 ], [ 127, 369 ], [ 128, 370 ], [ 129, 371 ], [ 130, 372 ], [ 131, 373 ], [ 132, 374 ], [ 133, 375 ], [ 134, 376 ], [ 135, 377 ], [ 136, 378 ], [ 137, 379 ], [ 138, 380 ], [ 139, 381 ], [ 140, 382 ], [ 141, 383 ], [ 142, 384 ], [ 143, 639 ], [ 144, 640 ], [ 145, 641 ], [ 146, 642 ], [ 217, 643 ], [ 218, 644 ], [ 219, 645 ], [ 220, 646 ], [ 221, 647 ], [ 222, 648 ], [ 223, 649 ], [ 224, 650 ], [ 225, 651 ], [ 226, 652 ], [ 227, 653 ], [ 228, 654 ], [ 229, 655 ], [ 230, 656 ], [ 231, 657 ], [ 232, 658 ], [ 233, 659 ], [ 234, 660 ], [ 235, 661 ], [ 236, 662 ], [ 237, 663 ], [ 238, 664 ], [ 239, 665 ], [ 240, 666 ], [ 241, 667 ], [ 242, 668 ], [ 243, 669 ], [ 244, 670 ], [ 249, 385 ], [ 250, 386 ], [ 251, 387 ], [ 252, 388 ], [ 287, 495 ], [ 288, 496 ], [ 289, 497 ], [ 290, 498 ], [ 291, 569 ], [ 292, 570 ], [ 293, 571 ], [ 294, 572 ], [ 295, 573 ], [ 296, 574 ], [ 297, 575 ], [ 298, 576 ], [ 299, 577 ], [ 300, 578 ], [ 301, 579 ], [ 302, 580 ], [ 303, 581 ], [ 304, 582 ], [ 305, 583 ], [ 306, 584 ], [ 307, 585 ], [ 308, 586 ], [ 309, 587 ], [ 310, 588 ], [ 311, 589 ], [ 312, 590 ], [ 313, 591 ], [ 314, 592 ], [ 315, 593 ], [ 316, 594 ], [ 317, 595 ], [ 318, 596 ], [ 389, 597 ], [ 390, 598 ], [ 391, 599 ], [ 392, 600 ], [ 463, 671 ], [ 464, 672 ], [ 465, 673 ], [ 466, 674 ], [ 467, 709 ], [ 468, 710 ], [ 469, 711 ], [ 470, 712 ], [ 471, 713 ], [ 472, 714 ], [ 473, 715 ], [ 474, 716 ], [ 475, 717 ], [ 476, 718 ], [ 477, 719 ], [ 478, 720 ], [ 479, 721 ], [ 480, 722 ], [ 481, 723 ], [ 482, 724 ], [ 483, 725 ], [ 484, 726 ], [ 485, 727 ], [ 486, 728 ], [ 487, 729 ], [ 488, 730 ], [ 489, 731 ], [ 490, 732 ], [ 491, 733 ], [ 492, 734 ], [ 493, 735 ], [ 494, 736 ], [ 877, 2343 ], [ 878, 2344 ], [ 879, 2345 ], [ 880, 2346 ], [ 881, 2347 ], [ 882, 2348 ], [ 883, 2349 ], [ 884, 2350 ], [ 885, 2351 ], [ 886, 2352 ], [ 887, 2353 ], [ 888, 2354 ], [ 889, 2355 ], [ 890, 2356 ], [ 891, 2329 ], [ 892, 2330 ], [ 893, 2331 ], [ 894, 2332 ], [ 895, 2333 ], [ 896, 2334 ], [ 897, 2335 ], [ 898, 2336 ], [ 899, 2337 ], [ 900, 2338 ], [ 901, 2339 ], [ 902, 2340 ], [ 903, 2341 ], [ 904, 2342 ], [ 905, 2315 ], [ 906, 2316 ], [ 907, 2317 ], [ 908, 2318 ], [ 909, 2319 ], [ 910, 2320 ], [ 911, 2321 ], [ 912, 2322 ], [ 913, 2323 ], [ 914, 2324 ], [ 915, 2325 ], [ 916, 2326 ], [ 917, 2327 ], [ 918, 2328 ], [ 919, 2301 ], [ 920, 2302 ], [ 921, 2303 ], [ 922, 2304 ], [ 923, 2305 ], [ 924, 2306 ], [ 925, 2307 ], [ 926, 2308 ], [ 927, 2309 ], [ 928, 2310 ], [ 929, 2311 ], [ 930, 2312 ], [ 931, 2313 ], [ 932, 2314 ], [ 1073, 4315 ], [ 1074, 4316 ], [ 1075, 4317 ], [ 1076, 4318 ], [ 1077, 4319 ], [ 1078, 4320 ], [ 1079, 4321 ], [ 1080, 4322 ], [ 1081, 4323 ], [ 1082, 4324 ], [ 1083, 4325 ], [ 1084, 4326 ], [ 1085, 4327 ], [ 1086, 4328 ], [ 1087, 4301 ], [ 1088, 4302 ], [ 1089, 4303 ], [ 1090, 4304 ], [ 1091, 4305 ], [ 1092, 4306 ], [ 1093, 4307 ], [ 1094, 4308 ], [ 1095, 4309 ], [ 1096, 4310 ], [ 1097, 4311 ], [ 1098, 4312 ], [ 1099, 4313 ], [ 1100, 4314 ], [ 1101, 4287 ], [ 1102, 4288 ], [ 1103, 4289 ], [ 1104, 4290 ], [ 1105, 4291 ], [ 1106, 4292 ], [ 1107, 4293 ], [ 1108, 4294 ], [ 1109, 4295 ], [ 1110, 4296 ], [ 1111, 4297 ], [ 1112, 4298 ], [ 1113, 4299 ], [ 1114, 4300 ], [ 1115, 4273 ], [ 1116, 4274 ], [ 1117, 4275 ], [ 1118, 4276 ], [ 1119, 4277 ], [ 1120, 4278 ], [ 1121, 4279 ], [ 1122, 4280 ], [ 1123, 4281 ], [ 1124, 4282 ], [ 1125, 4283 ], [ 1126, 4284 ], [ 1127, 4285 ], [ 1128, 4286 ], [ 2177, 3211 ], [ 2178, 3212 ], [ 2179, 3213 ], [ 2180, 3214 ], [ 2181, 3215 ], [ 2182, 3216 ], [ 2183, 3217 ], [ 2184, 3218 ], [ 2185, 3219 ], [ 2186, 3220 ], [ 2187, 3221 ], [ 2188, 3222 ], [ 2189, 3223 ], [ 2190, 3224 ], [ 2191, 3197 ], [ 2192, 3198 ], [ 2193, 3199 ], [ 2194, 3200 ], [ 2195, 3201 ], [ 2196, 3202 ], [ 2197, 3203 ], [ 2198, 3204 ], [ 2199, 3205 ], [ 2200, 3206 ], [ 2201, 3207 ], [ 2202, 3208 ], [ 2203, 3209 ], [ 2204, 3210 ], [ 2205, 3183 ], [ 2206, 3184 ], [ 2207, 3185 ], [ 2208, 3186 ], [ 2209, 3187 ], [ 2210, 3188 ], [ 2211, 3189 ], [ 2212, 3190 ], [ 2213, 3191 ], [ 2214, 3192 ], [ 2215, 3193 ], [ 2216, 3194 ], [ 2217, 3195 ], [ 2218, 3196 ], [ 2219, 3169 ], [ 2220, 3170 ], [ 2221, 3171 ], [ 2222, 3172 ], [ 2223, 3173 ], [ 2224, 3174 ], [ 2225, 3175 ], [ 2226, 3176 ], [ 2227, 3177 ], [ 2228, 3178 ], [ 2229, 3179 ], [ 2230, 3180 ], [ 2231, 3181 ], [ 2232, 3182 ], [ 2973, 4439 ], [ 2974, 4440 ], [ 2975, 4441 ], [ 2976, 4442 ], [ 2977, 4443 ], [ 2978, 4444 ], [ 2979, 4445 ], [ 2980, 4446 ], [ 2981, 4447 ], [ 2982, 4448 ], [ 2983, 4449 ], [ 2984, 4450 ], [ 2985, 4451 ], [ 2986, 4452 ], [ 2987, 4425 ], [ 2988, 4426 ], [ 2989, 4427 ], [ 2990, 4428 ], [ 2991, 4429 ], [ 2992, 4430 ], [ 2993, 4431 ], [ 2994, 4432 ], [ 2995, 4433 ], [ 2996, 4434 ], [ 2997, 4435 ], [ 2998, 4436 ], [ 2999, 4437 ], [ 3000, 4438 ], [ 3001, 4411 ], [ 3002, 4412 ], [ 3003, 4413 ], [ 3004, 4414 ], [ 3005, 4415 ], [ 3006, 4416 ], [ 3007, 4417 ], [ 3008, 4418 ], [ 3009, 4419 ], [ 3010, 4420 ], [ 3011, 4421 ], [ 3012, 4422 ], [ 3013, 4423 ], [ 3014, 4424 ], [ 3015, 4397 ], [ 3016, 4398 ], [ 3017, 4399 ], [ 3018, 4400 ], [ 3019, 4401 ], [ 3020, 4402 ], [ 3021, 4403 ], [ 3022, 4404 ], [ 3023, 4405 ], [ 3024, 4406 ], [ 3025, 4407 ], [ 3026, 4408 ], [ 3027, 4409 ], [ 3028, 4410 ]], [], 0)
HRectMesh.ConvertToQuadratic(1)
R1_2 = HRectMesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = HRectMesh.GroupOnGeom(R2,'R2',SMESH.NODE)
smesh.SetName(HRectMesh, 'HRectMesh')
try:
  HRectMesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Thin_Rectangle/HRec_36w_18h_15t_hex20.med',auto_groups=0,version=40,overwrite=1,meshPart=None,autoDimension=1)
  pass
except:
  #print('ExportMED() failed. Invalid file name?') ### not created Object
[ thickness_1, width_1, height_1, length_1, Box1_1, Box2_1, Box3_1, Box4_1, R1_1, R2_1, R1_2, R2_2 ] = HRectMesh.GetGroups()
equal_elements = HRectMesh.FindEqualElements( [ HRectMesh ], [] )
HRectMesh.MergeElements( [[ 6, 225 ], [ 7, 226 ], [ 8, 227 ], [ 9, 228 ], [ 10, 229 ], [ 83, 306 ], [ 84, 307 ], [ 85, 308 ], [ 86, 309 ], [ 87, 310 ], [ 88, 347 ], [ 89, 348 ], [ 90, 349 ], [ 91, 350 ], [ 92, 351 ], [ 93, 352 ], [ 94, 353 ], [ 95, 354 ], [ 96, 355 ], [ 97, 356 ], [ 98, 357 ], [ 99, 358 ], [ 100, 359 ], [ 101, 360 ], [ 102, 361 ], [ 103, 362 ], [ 104, 363 ], [ 105, 364 ], [ 106, 365 ], [ 107, 366 ], [ 108, 367 ], [ 109, 368 ], [ 110, 369 ], [ 111, 370 ], [ 112, 371 ], [ 113, 372 ], [ 114, 373 ], [ 115, 374 ], [ 116, 375 ], [ 117, 376 ], [ 230, 377 ], [ 231, 378 ], [ 232, 379 ], [ 233, 380 ], [ 234, 381 ], [ 271, 494 ], [ 272, 495 ], [ 273, 496 ], [ 274, 497 ], [ 275, 498 ], [ 276, 571 ], [ 277, 572 ], [ 278, 573 ], [ 279, 574 ], [ 280, 575 ], [ 281, 576 ], [ 282, 577 ], [ 283, 578 ], [ 284, 579 ], [ 285, 580 ], [ 286, 581 ], [ 287, 582 ], [ 288, 583 ], [ 289, 584 ], [ 290, 585 ], [ 291, 586 ], [ 292, 587 ], [ 293, 588 ], [ 294, 589 ], [ 295, 590 ], [ 296, 591 ], [ 297, 592 ], [ 298, 593 ], [ 299, 594 ], [ 300, 595 ], [ 301, 596 ], [ 302, 597 ], [ 303, 598 ], [ 304, 599 ], [ 305, 600 ], [ 382, 601 ], [ 383, 602 ], [ 384, 603 ], [ 385, 604 ], [ 386, 605 ], [ 1, 606 ], [ 2, 607 ], [ 3, 608 ], [ 4, 609 ], [ 5, 610 ], [ 118, 647 ], [ 119, 648 ], [ 120, 649 ], [ 121, 650 ], [ 122, 651 ], [ 195, 652 ], [ 196, 653 ], [ 197, 654 ], [ 198, 655 ], [ 199, 656 ], [ 200, 657 ], [ 201, 658 ], [ 202, 659 ], [ 203, 660 ], [ 204, 661 ], [ 205, 662 ], [ 206, 663 ], [ 207, 664 ], [ 208, 665 ], [ 209, 666 ], [ 210, 667 ], [ 211, 668 ], [ 212, 669 ], [ 213, 670 ], [ 214, 671 ], [ 215, 672 ], [ 216, 673 ], [ 217, 674 ], [ 218, 675 ], [ 219, 676 ], [ 220, 677 ], [ 221, 678 ], [ 222, 679 ], [ 223, 680 ], [ 224, 681 ], [ 459, 682 ], [ 460, 683 ], [ 461, 684 ], [ 462, 685 ], [ 463, 686 ], [ 464, 723 ], [ 465, 724 ], [ 466, 725 ], [ 467, 726 ], [ 468, 727 ], [ 469, 728 ], [ 470, 729 ], [ 471, 730 ], [ 472, 731 ], [ 473, 732 ], [ 474, 733 ], [ 475, 734 ], [ 476, 735 ], [ 477, 736 ], [ 478, 737 ], [ 479, 738 ], [ 480, 739 ], [ 481, 740 ], [ 482, 741 ], [ 483, 742 ], [ 484, 743 ], [ 485, 744 ], [ 486, 745 ], [ 487, 746 ], [ 488, 747 ], [ 489, 748 ], [ 490, 749 ], [ 491, 750 ], [ 492, 751 ], [ 493, 752 ], [ 993, 2598 ], [ 994, 2599 ], [ 995, 2600 ], [ 996, 2601 ], [ 997, 2602 ], [ 998, 2603 ], [ 999, 2604 ], [ 1000, 2605 ], [ 1001, 2606 ], [ 1002, 2607 ], [ 1003, 2608 ], [ 1004, 2609 ], [ 1005, 2610 ], [ 1006, 2611 ], [ 1007, 2612 ], [ 978, 2613 ], [ 979, 2614 ], [ 980, 2615 ], [ 981, 2616 ], [ 982, 2617 ], [ 983, 2618 ], [ 984, 2619 ], [ 985, 2620 ], [ 986, 2621 ], [ 987, 2622 ], [ 988, 2623 ], [ 989, 2624 ], [ 990, 2625 ], [ 991, 2626 ], [ 992, 2627 ], [ 963, 2628 ], [ 964, 2629 ], [ 965, 2630 ], [ 966, 2631 ], [ 967, 2632 ], [ 968, 2633 ], [ 969, 2634 ], [ 970, 2635 ], [ 971, 2636 ], [ 972, 2637 ], [ 973, 2638 ], [ 974, 2639 ], [ 975, 2640 ], [ 976, 2641 ], [ 977, 2642 ], [ 948, 2643 ], [ 949, 2644 ], [ 950, 2645 ], [ 951, 2646 ], [ 952, 2647 ], [ 953, 2648 ], [ 954, 2649 ], [ 955, 2650 ], [ 956, 2651 ], [ 957, 2652 ], [ 958, 2653 ], [ 959, 2654 ], [ 960, 2655 ], [ 961, 2656 ], [ 962, 2657 ], [ 933, 2658 ], [ 934, 2659 ], [ 935, 2660 ], [ 936, 2661 ], [ 937, 2662 ], [ 938, 2663 ], [ 939, 2664 ], [ 940, 2665 ], [ 941, 2666 ], [ 942, 2667 ], [ 943, 2668 ], [ 944, 2669 ], [ 945, 2670 ], [ 946, 2671 ], [ 947, 2672 ], [ 2493, 3648 ], [ 2494, 3649 ], [ 2495, 3650 ], [ 2496, 3651 ], [ 2497, 3652 ], [ 2498, 3653 ], [ 2499, 3654 ], [ 2500, 3655 ], [ 2501, 3656 ], [ 2502, 3657 ], [ 2503, 3658 ], [ 2504, 3659 ], [ 2505, 3660 ], [ 2506, 3661 ], [ 2507, 3662 ], [ 2478, 3663 ], [ 2479, 3664 ], [ 2480, 3665 ], [ 2481, 3666 ], [ 2482, 3667 ], [ 2483, 3668 ], [ 2484, 3669 ], [ 2485, 3670 ], [ 2486, 3671 ], [ 2487, 3672 ], [ 2488, 3673 ], [ 2489, 3674 ], [ 2490, 3675 ], [ 2491, 3676 ], [ 2492, 3677 ], [ 2463, 3678 ], [ 2464, 3679 ], [ 2465, 3680 ], [ 2466, 3681 ], [ 2467, 3682 ], [ 2468, 3683 ], [ 2469, 3684 ], [ 2470, 3685 ], [ 2471, 3686 ], [ 2472, 3687 ], [ 2473, 3688 ], [ 2474, 3689 ], [ 2475, 3690 ], [ 2476, 3691 ], [ 2477, 3692 ], [ 2448, 3693 ], [ 2449, 3694 ], [ 2450, 3695 ], [ 2451, 3696 ], [ 2452, 3697 ], [ 2453, 3698 ], [ 2454, 3699 ], [ 2455, 3700 ], [ 2456, 3701 ], [ 2457, 3702 ], [ 2458, 3703 ], [ 2459, 3704 ], [ 2460, 3705 ], [ 2461, 3706 ], [ 2462, 3707 ], [ 2433, 3708 ], [ 2434, 3709 ], [ 2435, 3710 ], [ 2436, 3711 ], [ 2437, 3712 ], [ 2438, 3713 ], [ 2439, 3714 ], [ 2440, 3715 ], [ 2441, 3716 ], [ 2442, 3717 ], [ 2443, 3718 ], [ 2444, 3719 ], [ 2445, 3720 ], [ 2446, 3721 ], [ 2447, 3722 ], [ 1248, 4893 ], [ 1249, 4894 ], [ 1250, 4895 ], [ 1251, 4896 ], [ 1252, 4897 ], [ 1253, 4898 ], [ 1254, 4899 ], [ 1255, 4900 ], [ 1256, 4901 ], [ 1257, 4902 ], [ 1258, 4903 ], [ 1259, 4904 ], [ 1260, 4905 ], [ 1261, 4906 ], [ 1262, 4907 ], [ 1233, 4908 ], [ 1234, 4909 ], [ 1235, 4910 ], [ 1236, 4911 ], [ 1237, 4912 ], [ 1238, 4913 ], [ 1239, 4914 ], [ 1240, 4915 ], [ 1241, 4916 ], [ 1242, 4917 ], [ 1243, 4918 ], [ 1244, 4919 ], [ 1245, 4920 ], [ 1246, 4921 ], [ 1247, 4922 ], [ 1218, 4923 ], [ 1219, 4924 ], [ 1220, 4925 ], [ 1221, 4926 ], [ 1222, 4927 ], [ 1223, 4928 ], [ 1224, 4929 ], [ 1225, 4930 ], [ 1226, 4931 ], [ 1227, 4932 ], [ 1228, 4933 ], [ 1229, 4934 ], [ 1230, 4935 ], [ 1231, 4936 ], [ 1232, 4937 ], [ 1203, 4938 ], [ 1204, 4939 ], [ 1205, 4940 ], [ 1206, 4941 ], [ 1207, 4942 ], [ 1208, 4943 ], [ 1209, 4944 ], [ 1210, 4945 ], [ 1211, 4946 ], [ 1212, 4947 ], [ 1213, 4948 ], [ 1214, 4949 ], [ 1215, 4950 ], [ 1216, 4951 ], [ 1217, 4952 ], [ 1188, 4953 ], [ 1189, 4954 ], [ 1190, 4955 ], [ 1191, 4956 ], [ 1192, 4957 ], [ 1193, 4958 ], [ 1194, 4959 ], [ 1195, 4960 ], [ 1196, 4961 ], [ 1197, 4962 ], [ 1198, 4963 ], [ 1199, 4964 ], [ 1200, 4965 ], [ 1201, 4966 ], [ 1202, 4967 ], [ 3453, 5058 ], [ 3454, 5059 ], [ 3455, 5060 ], [ 3456, 5061 ], [ 3457, 5062 ], [ 3458, 5063 ], [ 3459, 5064 ], [ 3460, 5065 ], [ 3461, 5066 ], [ 3462, 5067 ], [ 3463, 5068 ], [ 3464, 5069 ], [ 3465, 5070 ], [ 3466, 5071 ], [ 3467, 5072 ], [ 3438, 5073 ], [ 3439, 5074 ], [ 3440, 5075 ], [ 3441, 5076 ], [ 3442, 5077 ], [ 3443, 5078 ], [ 3444, 5079 ], [ 3445, 5080 ], [ 3446, 5081 ], [ 3447, 5082 ], [ 3448, 5083 ], [ 3449, 5084 ], [ 3450, 5085 ], [ 3451, 5086 ], [ 3452, 5087 ], [ 3423, 5088 ], [ 3424, 5089 ], [ 3425, 5090 ], [ 3426, 5091 ], [ 3427, 5092 ], [ 3428, 5093 ], [ 3429, 5094 ], [ 3430, 5095 ], [ 3431, 5096 ], [ 3432, 5097 ], [ 3433, 5098 ], [ 3434, 5099 ], [ 3435, 5100 ], [ 3436, 5101 ], [ 3437, 5102 ], [ 3408, 5103 ], [ 3409, 5104 ], [ 3410, 5105 ], [ 3411, 5106 ], [ 3412, 5107 ], [ 3413, 5108 ], [ 3414, 5109 ], [ 3415, 5110 ], [ 3416, 5111 ], [ 3417, 5112 ], [ 3418, 5113 ], [ 3419, 5114 ], [ 3420, 5115 ], [ 3421, 5116 ], [ 3422, 5117 ], [ 3393, 5118 ], [ 3394, 5119 ], [ 3395, 5120 ], [ 3396, 5121 ], [ 3397, 5122 ], [ 3398, 5123 ], [ 3399, 5124 ], [ 3400, 5125 ], [ 3401, 5126 ], [ 3402, 5127 ], [ 3403, 5128 ], [ 3404, 5129 ], [ 3405, 5130 ], [ 3406, 5131 ], [ 3407, 5132 ]], [] )
Sub_mesh_1 = Regular_1D_2_1.GetSubMesh()
Sub_mesh_2 = Regular_1D_2_2.GetSubMesh()
Sub_mesh_4 = Regular_1D_2_3.GetSubMesh()
Sub_mesh_5 = Regular_1D_2_4.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D_2.GetAlgorithm(), 'Regular_1D_2')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(NumberOfSegments_15, 'NumberOfSegments=15,[],0:1:1:5')
smesh.SetName(nl, 'nl')
smesh.SetName(nw, 'nw')
smesh.SetName(R1_1, 'R1')
smesh.SetName(nh, 'nh')
smesh.SetName(R2_1, 'R2')
smesh.SetName(HRectMesh.GetMesh(), 'HRectMesh')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_5, 'Sub-mesh_5')
smesh.SetName(Box4_1, 'Box4')
smesh.SetName(Box3_1, 'Box3')
smesh.SetName(Box2_1, 'Box2')
smesh.SetName(Box1_1, 'Box1')
smesh.SetName(thickness_1, 'thickness')
smesh.SetName(height_1, 'height')
smesh.SetName(width_1, 'width')
smesh.SetName(R2_2, 'R2')
smesh.SetName(length_1, 'length')
smesh.SetName(R1_2, 'R1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
