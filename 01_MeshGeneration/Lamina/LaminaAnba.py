#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Anba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin #import *
# from dolfin import compile_extension_module
import time
import math
import numpy as np
from petsc4py import PETSc
import os
import matplotlib.pyplot as plt

import anba4 #import *
import mshr

# from voight_notation import stressVectorToStressTensor, stressTensorToStressVector, strainVectorToStrainTensor, strainTensorToStrainVector
# from material import material
# from anbax import anbax

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

e_xx = 9.8e9
e_yy = 9.8e9
e_zz = 1.42e11
g_xy = 4.8e9
g_xz = 6.0e9
g_yz = 6.0e9
nu_xy = 0.34
nu_zx = 0.3
nu_zy = 0.3
#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy


width = 0.1 
height = 0.01

h = 0.04
n_layer = 4
ylist = np.linspace(-h/2, h/2, n_layer+1)

Pi = []

for act_y in ylist:
    Pi.append(dolfin.Point(-width/2., act_y, 0.))
    Pi.append(dolfin.Point( width/2., act_y, 0.))

Box1 = mshr.Polygon((Pi[0], Pi[1], Pi[3], Pi[2]))
Box2 = mshr.Polygon((Pi[2], Pi[3], Pi[5], Pi[4]))
Box3 = mshr.Polygon((Pi[4], Pi[5], Pi[7], Pi[6]))
Box4 = mshr.Polygon((Pi[6], Pi[7], Pi[9], Pi[8]))

C_shape = Box1 + Box2 + Box3 + Box4
C_shape.set_subdomain(1, Box1)
C_shape.set_subdomain(2, Box2)
C_shape.set_subdomain(3, Box3)
C_shape.set_subdomain(4, Box4)

mesh = mshr.generate_mesh(C_shape, 128)
mf = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim(), mesh.domains())


mesh_points=mesh.coordinates()
dolfin.plot(mesh)
#dolfin.plot(mf)
plt.show()

# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
tol = 1e-14


# Rotate mesh.
theta = 0.0 #0.0
rotation_angle = 0.
materials.set_all(0)
#fiber_orientations.set_all(theta)
plane_orientations.set_all(rotation_angle)

[fiber_orientations.set_value(i, theta) for i in mf.where_equal(1)]
[fiber_orientations.set_value(i, -theta) for i in mf.where_equal(2)]
[fiber_orientations.set_value(i, -theta) for i in mf.where_equal(3)]
[fiber_orientations.set_value(i, theta) for i in mf.where_equal(4)]

pippo=dolfin.plot(plane_orientations)
plt.show()

pippo=dolfin.plot(fiber_orientations)
plt.legend()
plt.show()


# rotate mesh.
rotate = dolfin.Expression(("x[0] * (cos(rotation_angle)-1.0) - x[1] * sin(rotation_angle)",
    "x[0] * sin(rotation_angle) + x[1] * (cos(rotation_angle)-1.0)"), rotation_angle = rotation_angle * np.pi / 180.0,
    degree = 1)

dolfin.ALE.move(mesh, rotate)
dolfin.plot(mesh)
#dolfin.plot(mf)
plt.show()


# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)


anba = anba4.anbax_singular(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
stiff = anba.compute()

#anba2 = anbax2(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
#stiff2 = anba2.compute()
stiff.view()
#stiff2.view()
