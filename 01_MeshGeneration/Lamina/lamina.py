#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.10.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Lamina/')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import numpy as np
import SALOMEDS


geompy = geomBuilder.New()

w = 0.1
length = 0.05

same_height = True
is_quadratic = True

n_seg_w = 20
n_seg_l = 5
n_seg_h = 4

width_dir = 0 # width along x
height_dir = 1 # height along y
axis_dir = 2 # axis along z, for automatic identification of R1 and R2

if same_height:
    h = 0.02
    n_layer = 1
    ylist = np.linspace(-h/2, h/2, n_layer+1)
else:
    hlist = np.array([0.01, 0.02, 0.01])
    h = np.sum(hlist)
    n_layer = len(hlist)
    ylist = np.zeros_like(h_list)
    ylist[0] = -h/2
    for i in range(len(hlist)):
        ylist[i+1] = ylist[i] + hlist[i]


#filename
if is_quadratic:
    eltype = 'hex20'
else:
    eltype = 'hex08'
    




O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)


vertices = []


for i, act_y in enumerate(ylist):
    vertices.append(geompy.MakeVertex(-w/2, act_y, 0))
    vertices.append(geompy.MakeVertex( w/2, act_y, 0))

lines = []

lines.append(geompy.MakeLineTwoPnt(vertices[0], vertices[1]))

for act_layer in range(n_layer):
    lines.append(geompy.MakeLineTwoPnt(vertices[act_layer*2+1], vertices[act_layer*2+3]))
    lines.append(geompy.MakeLineTwoPnt(vertices[act_layer*2+3], vertices[act_layer*2+2]))
    lines.append(geompy.MakeLineTwoPnt(vertices[act_layer*2+2], vertices[act_layer*2]))


faces = []

faces.append(geompy.MakeFaceWires([lines[0], lines[1], lines[2], lines[3]], 1))

for nf in range(1, n_layer):
    faces.append(geompy.MakeFaceWires([lines[3*nf-1], lines[3*nf+1], lines[3*nf+2], lines[3*nf+3]], 1))

solids = []

for act_face in faces:
    solids.append(geompy.MakePrismVecH(act_face, OZ, length))

Composite = geompy.MakeCompound(solids)

# add objects to the group
LayerList = geompy.SubShapeAllSortedCentres(Composite, geompy.ShapeType["SOLID"])

layer_groups = []

for act_layer in LayerList:
    SolidID = geompy.GetSubShapeID(Composite, act_layer)
    layer = geompy.CreateGroup(Composite, geompy.ShapeType["SOLID"])
    geompy.UnionIDs(layer, [SolidID])
    layer_groups.append(layer)



FacesList = geompy.SubShapeAllSortedCentres(Composite, geompy.ShapeType["FACE"])

R1 = geompy.CreateGroup(Composite, geompy.ShapeType["FACE"])
R2 = geompy.CreateGroup(Composite, geompy.ShapeType["FACE"])

for act_f in FacesList:
    cm = geompy.MakeCDG(act_f)
    if cm is None:
        raise RuntimeError("MakeCDG(box) failed")
    else:
        print("\nCentre of gravity of face has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        if(np.abs(coords[axis_dir]) < 1e-6):
            #belongs to R1
            FaceID = geompy.GetSubShapeID(Composite, act_f)
            geompy.UnionIDs(R1, [FaceID])
        elif(np.abs(coords[axis_dir]-length) < 1e-6):
            #belongs to R2
            FaceID = geompy.GetSubShapeID(Composite, act_f)
            geompy.UnionIDs(R2, [FaceID])
            

EdgeList = geompy.SubShapeAllSortedCentres(Composite, geompy.ShapeType["EDGE"])

n_width = geompy.CreateGroup(Composite, geompy.ShapeType["EDGE"])
n_length = geompy.CreateGroup(Composite, geompy.ShapeType["EDGE"])
n_height = geompy.CreateGroup(Composite, geompy.ShapeType["EDGE"])

for act_e in EdgeList:
    cm = geompy.MakeCDG(act_e)
    if cm is None:
        raise RuntimeError("MakeCDG(EDGE) failed")
    else:
        # print("\nCentre of gravity of edge has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        print(coords)
        EdgeID = geompy.GetSubShapeID(Composite, act_e)
        if(np.abs(coords[axis_dir]) < 1e-6):
            #belongs to R1
            if(np.abs(coords[width_dir] - w/2) < 1e-6 or np.abs(coords[width_dir] + w/2) < 1e-6):
                # belongs to height
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " assigned to height")
                geompy.UnionIDs(n_height, [EdgeID])
            elif(np.abs(coords[width_dir]) < 1e-6):
                # belongs to width
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " assigned to width")
                geompy.UnionIDs(n_width, [EdgeID])
            else:
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " not assigned")
        elif(np.abs(coords[axis_dir]-length) < 1e-6):
            if(np.abs(coords[width_dir] - w/2) < 1e-6 or np.abs(coords[width_dir] + w/2) < 1e-6):
                # belongs to height
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " assigned to height")
                geompy.UnionIDs(n_height, [EdgeID])
            elif(np.abs(coords[width_dir]) < 1e-6):
                # belongs to width
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " assigned to width")
                geompy.UnionIDs(n_width, [EdgeID])
            else:
                print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " not assigned")
        else:
            print("Edge with coordinates: ", coords[0], ", ", coords[1], ", ", coords[2], " assigned to length")
            geompy.UnionIDs(n_length, [EdgeID])


#all_lines = geompy.Propagate(Compound_1)
#R1 = geompy.CreateGroup(Compound_1, geompy.ShapeType["FACE"])
#geompy.UnionIDs(R1, [32, 66, 100, 134])
#R2 = geompy.CreateGroup(Compound_1, geompy.ShapeType["FACE"])
#geompy.UnionIDs(R2, [34, 68, 102, 136])


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for nv, v in enumerate(vertices):
#    geompy.addToStudy( v, f'Vertex_{nv}' )

#for nl, l in enumerate(lines):
#    geompy.addToStudy( l, f'Line_{nl}' )

#for nf, f in enumerate(faces):
#    geompy.addToStudy( f, f'Face_{nf}' )

for ns, s in enumerate(solids):
    geompy.addToStudy( s, f'Solid_{ns}' )

geompy.addToStudy( Composite, 'Composite' )

for i, act_layer_g in enumerate(layer_groups):
    geompy.addToStudyInFather(Composite, act_layer_g, f'layer_{i}')

geompy.addToStudyInFather(Composite, R1, 'R1')
geompy.addToStudyInFather(Composite, R2, 'R2')

geompy.addToStudyInFather(Composite, n_width, 'n_width')
geompy.addToStudyInFather(Composite, n_length, 'n_length')
geompy.addToStudyInFather(Composite, n_height, 'n_height')


import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Composite_Mesh = smesh.Mesh(Composite,'Composite_Mesh')

Hexa_3D = Composite_Mesh.Hexahedron(algo=smeshBuilder.Hexa)

Regular_1D = Composite_Mesh.Segment()
generic = Regular_1D.NumberOfSegments(15)


Quadrangle_2D = Composite_Mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])

# Prism_3D = Composite_Mesh.Prism()

layer_i_1 = []

for i, act_layer_g in enumerate(layer_groups):
    layer_i_1.append(Composite_Mesh.GroupOnGeom(act_layer_g, f'layer_{i}', SMESH.VOLUME))

R1_1 = Composite_Mesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_1 = Composite_Mesh.GroupOnGeom(R2,'R2',SMESH.NODE)

n_width_1 = Composite_Mesh.GroupOnGeom(n_width,'n_width',SMESH.EDGE)
n_length_1 = Composite_Mesh.GroupOnGeom(n_length,'n_length',SMESH.EDGE)
n_height_1 = Composite_Mesh.GroupOnGeom(n_height,'n_height',SMESH.EDGE)

Regular_1D = Composite_Mesh.Segment(geom=n_height)
nh = Regular_1D.NumberOfSegments(n_seg_h)
Regular_1D_1 = Composite_Mesh.Segment(geom=n_width)
nw = Regular_1D_1.NumberOfSegments(n_seg_w)
Regular_1D_2 = Composite_Mesh.Segment(geom=n_length)
nl = Regular_1D_2.NumberOfSegments(n_seg_l)

isDone = Composite_Mesh.Compute()

#[ layer_0_1, layer_1_1, layer_2_1, layer_3_1, R1_1, R2_1, n_width_1, n_length_1, h_height_1 ] = Composite_Mesh.GetGroups()

if is_quadratic:
    Composite_Mesh.ConvertToQuadratic(0)



#[ layer_0_1, layer_1_1, layer_2_1, layer_3_1, R1_1, R2_1, n_width_1, n_length_1, h_height_1 ] = Composite_Mesh.GetGroups()

coincident_nodes_on_part = Composite_Mesh.FindCoincidentNodesOnPart( [ Composite_Mesh ], 1e-05, [], 0 )

Composite_Mesh.MergeNodes(coincident_nodes_on_part, [], 0)

smesh.SetName(Composite_Mesh, 'Composite_Mesh')


filename = '/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Lamina/Lam_nw{0:02d}_nl{1:02d}_{2}.med'.format(n_seg_w, n_layer, eltype)

try:
    Composite_Mesh.ExportMED( filename, 0, 41, 1, Composite_Mesh, 1, [], '',-1, 1 )
except:
    print('ExportPartToMED() failed. Invalid file name?')


Sub_mesh_1 = Regular_1D.GetSubMesh()
Sub_mesh_2 = Regular_1D_1.GetSubMesh()
Sub_mesh_3 = Regular_1D_2.GetSubMesh()


## Set names of Mesh objects
# smesh.SetName(Prism_3D.GetAlgorithm(), 'Prism_3D')

smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')

smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(nl, 'nl')
smesh.SetName(nh, 'nh')
smesh.SetName(nw, 'nw')
smesh.SetName(Composite_Mesh.GetMesh(), 'Composite_Mesh')
smesh.SetName(R2_1, 'R2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(n_width_1, 'n_width')
smesh.SetName(n_length_1, 'n_length')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(n_height_1, 'n_height')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')

for i, act_layer_m in enumerate(layer_i_1):
    smesh.SetName(act_layer, f'layer_{i}')




if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
