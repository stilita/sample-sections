#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.7.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/Ring')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()



# inner radius of ring
radius_int = 0.1

# inner radius of ring
radius_ext = 0.2


# length of ring 
length = 0.5

# number of elements in x-direction (axis of beam)
z_elems = 5

# number of elements of the radial part
r_elems = 8

# number of elements of a quarter of circle
c_elems = 15

# linear or quadratic mesh
quad = True 

# folder to save
folder = 'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/Ring'



O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Disk_1 = geompy.MakeDiskR(radius_ext, 2)
Disk_2 = geompy.MakeDiskR(radius_int, 2)
Cut_1 = geompy.MakeCutList(Disk_1, [Disk_2], True)
Face_1 = geompy.MakeFaceHW(0.2, 3*radius_ext, 1)
Face_2 = geompy.MakeFaceHW(3*radius_ext, 0.2, 3)
Partition_1 = geompy.MakePartition([Cut_1], [Face_1, Face_2], [], [], geompy.ShapeType["FACE"], 0, [], 0)
[Face_3,Face_4,Face_5,Face_6] = geompy.ExtractShapes(Partition_1, geompy.ShapeType["FACE"], True)
Shell_1 = geompy.MakeShell([Face_3, Face_4, Face_5, Face_6])
Ring = geompy.MakePrismVecH(Shell_1, OX, length)
[r, c1, c2, c3, c4, z] = geompy.Propagate(Ring)
R2 = geompy.CreateGroup(Ring, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [96, 82, 58, 34])
R1 = geompy.CreateGroup(Ring, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [56, 94, 32, 80])
[r, c1, c2, c3, c4, z, R2, R1] = geompy.GetExistingSubObjects(Ring, False)


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Disk_1, 'Disk_1' )
geompy.addToStudy( Disk_2, 'Disk_2' )
geompy.addToStudy( Cut_1, 'Cut_1' )
geompy.addToStudy( Face_1, 'Face_1' )
geompy.addToStudy( Face_2, 'Face_2' )
geompy.addToStudy( Partition_1, 'Partition_1' )
geompy.addToStudyInFather( Partition_1, Face_3, 'Face_3' )
geompy.addToStudyInFather( Partition_1, Face_4, 'Face_4' )
geompy.addToStudyInFather( Partition_1, Face_5, 'Face_5' )
geompy.addToStudyInFather( Partition_1, Face_6, 'Face_6' )
geompy.addToStudy( Shell_1, 'Shell_1' )
geompy.addToStudy( Ring, 'Ring' )
geompy.addToStudyInFather( Ring, r, 'r' )
geompy.addToStudyInFather( Ring, c1, 'c1' )
geompy.addToStudyInFather( Ring, c2, 'c2' )
geompy.addToStudyInFather( Ring, c3, 'c3' )
geompy.addToStudyInFather( Ring, c4, 'c4' )
geompy.addToStudyInFather( Ring, z, 'z' )
geompy.addToStudyInFather( Ring, R2, 'R2' )
geompy.addToStudyInFather( Ring, R1, 'R1' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Ring_1 = smesh.Mesh(Ring)
Quadrangle_2D = Ring_1.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_QUADRANGLE_PREF,-1,[],[])
Hexa_3D = Ring_1.Hexahedron(algo=smeshBuilder.Hexa)

r_1 = Ring_1.GroupOnGeom(r,'r',SMESH.EDGE)
c1_1 = Ring_1.GroupOnGeom(c1,'c1',SMESH.EDGE)
c2_1 = Ring_1.GroupOnGeom(c2,'c2',SMESH.EDGE)
c3_1 = Ring_1.GroupOnGeom(c3,'c3',SMESH.EDGE)
c4_1 = Ring_1.GroupOnGeom(c4,'c4',SMESH.EDGE)
z_1 = Ring_1.GroupOnGeom(z,'z',SMESH.EDGE)

Regular_1D = Ring_1.Segment(geom=r)
nr = Regular_1D.NumberOfSegments(r_elems)

Regular_1D_1 = Ring_1.Segment(geom=z)
nz = Regular_1D_1.NumberOfSegments(z_elems)

Regular_1D_2 = Ring_1.Segment(geom=c1)
nc = Regular_1D_2.NumberOfSegments(c_elems)


Regular_1D_3 = Ring_1.Segment(geom=c2)
Regular_1D_4 = Ring_1.Segment(geom=c3)
Regular_1D_5 = Ring_1.Segment(geom=c4)

status = Ring_1.AddHypothesis(nc,c2)
status = Ring_1.AddHypothesis(nc,c3)
status = Ring_1.AddHypothesis(nc,c4)


isDone = Ring_1.Compute()
[ r_1, c1_1, c2_1, c3_1, c4_1, z_1 ] = Ring_1.GetGroups()

if quad:
    Ring_1.ConvertToQuadratic(0)
    meshtype = 'C3D20'
else:
    meshtype = 'C3D08'


R2_1 = Ring_1.GroupOnGeom(R2,'R2',SMESH.NODE)
R1_1 = Ring_1.GroupOnGeom(R1,'R1',SMESH.NODE)

name = 'Ring_{0}_z{1:02d}_r{2:02d}_c{3:02d}.med'.format(meshtype, z_elems, r_elems, c_elems) 


if os.path.isdir(folder):
    print('Writing file '+name+' in folder ' + folder)
    act_folder = folder
else:
    print('Folder '+folder+' not found. Using default location: '+os.getcwd())
    act_folder = os.getcwd()
    

print('Full Name = ' + act_folder + '/' + name)

smesh.SetName(Ring_1, 'Ring')

try:
  Ring_1.ExportMED(act_folder+'/'+name,auto_groups=0,version=41,overwrite=1,meshPart=None,autoDimension=1)
  pass
except:
  print('ExportMED() failed. Invalid file name?')

radial = Regular_1D.GetSubMesh()
axial = Regular_1D_1.GetSubMesh()
circonf1 = Regular_1D_2.GetSubMesh()
circonf2 = Regular_1D_3.GetSubMesh()
circonf3 = Regular_1D_4.GetSubMesh()
circonf4 = Regular_1D_5.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(nr, 'nr')
smesh.SetName(nz, 'nz')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nc, 'nc')
smesh.SetName(Ring_1.GetMesh(), 'Ring')
smesh.SetName(circonf1, 'circonf1')
smesh.SetName(circonf3, 'circonf3')
smesh.SetName(circonf2, 'circonf2')
smesh.SetName(radial, 'radial')
smesh.SetName(axial, 'axial')
smesh.SetName(circonf4, 'circonf4')
smesh.SetName(r_1, 'r')
smesh.SetName(c2_1, 'c2')
smesh.SetName(c1_1, 'c1')
smesh.SetName(R1_1, 'R1')
smesh.SetName(c4_1, 'c4')
smesh.SetName(c3_1, 'c3')
smesh.SetName(R2_1, 'R2')
smesh.SetName(z_1, 'z')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
