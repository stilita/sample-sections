#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.9.0 with dump python functionality
###

import sys
import salome
import os

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/Cylinder')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

# radius of cylinder
radius = 0.1

# length of cylinder 
length = 0.5

# number of elements in z-direction (axis of beam)
z_elems = 5

# number of elements of the radial part
r_elems = 4

# number of elements of the square part and of a quarter of circle
c_elems = 6

# linear or quadratic mesh
quad = True 

# folder to save
folder = '/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Cylinder'


O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
cylinder = geompy.MakeDividedCylinder(radius, length, GEOM.SQUARE)
[z, r, c1, c2] = geompy.Propagate(cylinder)
None
[z, r, c1, c2] = geompy.GetExistingSubObjects(cylinder, False)
R1 = geompy.CreateGroup(cylinder, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [94, 32, 56, 80, 100])
R2 = geompy.CreateGroup(cylinder, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [34, 58, 82, 96, 102])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( cylinder, 'cylinder' )
geompy.addToStudyInFather( cylinder, z, 'z' )
geompy.addToStudyInFather( cylinder, r, 'r' )
geompy.addToStudyInFather( cylinder, c1, 'c1' )
geompy.addToStudyInFather( cylinder, c2, 'c2' )
geompy.addToStudyInFather( cylinder, R1, 'R1' )
geompy.addToStudyInFather( cylinder, R2, 'R2' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

cylinder_1 = smesh.Mesh(cylinder,'cylinder')
Quadrangle_2D = cylinder_1.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_QUADRANGLE_PREF,-1,[],[])
Hexa_3D = cylinder_1.Hexahedron(algo=smeshBuilder.Hexa)
z_1 = cylinder_1.GroupOnGeom(z,'z',SMESH.EDGE)
r_1 = cylinder_1.GroupOnGeom(r,'r',SMESH.EDGE)
c1_1 = cylinder_1.GroupOnGeom(c1,'c1',SMESH.EDGE)
c2_1 = cylinder_1.GroupOnGeom(c2,'c2',SMESH.EDGE)
Regular_1D = cylinder_1.Segment(geom=z)
nz = Regular_1D.NumberOfSegments(z_elems)
Regular_1D_1 = cylinder_1.Segment(geom=r)
nr = Regular_1D_1.NumberOfSegments(r_elems)
Regular_1D_2 = cylinder_1.Segment(geom=c1)
nc = Regular_1D_2.NumberOfSegments(c_elems)
Regular_1D_3 = cylinder_1.Segment(geom=c2)
status = cylinder_1.AddHypothesis(nc,c2)
isDone = cylinder_1.Compute()
[ z_1, r_1, c1_1, c2_1 ] = cylinder_1.GetGroups()

if quad:
    cylinder_1.ConvertToQuadratic(0)
    meshtype = 'C3D20'
else:
    meshtype = 'C3D08'



R1_1 = cylinder_1.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_1 = cylinder_1.GroupOnGeom(R2,'R2',SMESH.NODE)
smesh.SetName(cylinder_1, 'cylinder')

name = 'cyl_{0}_z{1:02d}_r{2:02d}_c{3:02d}.med'.format(meshtype, z_elems, r_elems, c_elems) 

if os.path.isdir(folder):
    print('Writing file '+name+' in folder ' + folder)
    act_folder = folder
else:
    print('Folder '+folder+' not found. Using default location: '+os.getcwd())
    act_folder = os.getcwd()
    

print('Full Name = ' + act_folder + '/' + name)

try:
  cylinder_1.ExportMED(act_folder+'/'+name,auto_groups=0,version=41,overwrite=1,meshPart=None,autoDimension=1)
  #cylinder_1.ExportMED( os.getcwd()+'/'+name, 0, 40, 1, cylinder_1, 1, [], '',-1, 1 )
  pass
except:
  print('ExportPartToMED() failed. Invalid file name?')
Sub_mesh_1 = Regular_1D.GetSubMesh()
Sub_mesh_2 = Regular_1D_1.GetSubMesh()
Sub_mesh_3 = Regular_1D_2.GetSubMesh()
Sub_mesh_4 = Regular_1D_3.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nz, 'nz')
smesh.SetName(nr, 'nr')
smesh.SetName(nc, 'nc')
smesh.SetName(cylinder_1.GetMesh(), 'cylinder')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')
smesh.SetName(z_1, 'z')
smesh.SetName(c1_1, 'c1')
smesh.SetName(r_1, 'r')
smesh.SetName(R2_1, 'R2')
smesh.SetName(c2_1, 'c2')
smesh.SetName(R1_1, 'R1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
