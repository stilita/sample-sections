#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.7.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/Ring_ellipse_shell')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

# ellipse shell along X axis

# major axis
Rmax = 0.25
# minor axis
Rmin = 0.2
#length
length = 0.5

# number of elements in x-direction (axis of beam)
x_elems = 5

# number of elements along circumference
c_elems = 30

# linear or quadratic mesh
quad = False 

# folder to save
folder = 'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/Ring_ellipse_shell'


O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Ellipse_1 = geompy.MakeEllipse(O, OX, Rmax, Rmin, OY)
Extrusion_1 = geompy.MakePrismVecH(Ellipse_1, OX, length)
R1 = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(R1, [6])
R2 = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(R2, [7])
z = geompy.CreateGroup(Extrusion_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(z, [3])
[R1, R2, z] = geompy.GetExistingSubObjects(Extrusion_1, False)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Ellipse_1, 'Ellipse_1' )
geompy.addToStudy( Extrusion_1, 'Extrusion_1' )
geompy.addToStudyInFather( Extrusion_1, R1, 'R1' )
geompy.addToStudyInFather( Extrusion_1, R2, 'R2' )
geompy.addToStudyInFather( Extrusion_1, z, 'z' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Ellipse_shell = smesh.Mesh(Extrusion_1)
Quadrangle_2D = Ellipse_shell.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_QUADRANGLE_PREF,-1,[],[])
R1_1 = Ellipse_shell.GroupOnGeom(R1,'R1',SMESH.EDGE)
R2_1 = Ellipse_shell.GroupOnGeom(R2,'R2',SMESH.EDGE)
z_1 = Ellipse_shell.GroupOnGeom(z,'z',SMESH.EDGE)
Regular_1D = Ellipse_shell.Segment(geom=R1)
nc = Regular_1D.NumberOfSegments(c_elems)
Regular_1D_1 = Ellipse_shell.Segment(geom=R2)
status = Ellipse_shell.AddHypothesis(nc,R2)
Regular_1D_2 = Ellipse_shell.Segment(geom=z)
nz = Regular_1D_2.NumberOfSegments(x_elems)
isDone = Ellipse_shell.Compute()
[ R1_1, R2_1, z_1 ] = Ellipse_shell.GetGroups()
smesh.SetName(Ellipse_shell, 'Ellipse_shell')


if quad:
    Ellipse_shell.ConvertToQuadratic(1)
    meshtype = 'S9R5'
else:
    meshtype = 'S4'

name = 'EllipseShell_{0}_x{1:02d}_c{2:02d}.med'.format(meshtype, x_elems, c_elems) 

if os.path.isdir(folder):
    print('Writing file '+name+' in folder ' + folder)
    act_folder = folder
else:
    print('Folder '+folder+' not found. Using default location: '+os.getcwd())
    act_folder = os.getcwd()
    

print('Full Name = ' + act_folder + '/' + name)


try:
    Ellipse_shell.ExportMED(act_folder+'/'+name, auto_groups=0, version=41, overwrite=1, meshPart=None, autoDimension=1)
    pass
except:
  print('ExportMED() failed. Invalid file name?')
Sub_mesh_1 = Regular_1D.GetSubMesh()
Sub_mesh_2 = Regular_1D_1.GetSubMesh()
Sub_mesh_3 = Regular_1D_2.GetSubMesh()


## Set names of Mesh objects
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(nc, 'nc')
smesh.SetName(nz, 'nz')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(Ellipse_shell.GetMesh(), 'Ellipse_shell')
smesh.SetName(R1_1, 'R1')
smesh.SetName(z_1, 'z')
smesh.SetName(R2_1, 'R2')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
