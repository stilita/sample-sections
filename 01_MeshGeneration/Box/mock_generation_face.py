#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 12:17:43 2023

@author: claudio
"""

import numpy as np
import subprocess


def replace_tokens(template_file_path, output_file_path, token_replacements):
    try:
        # Read the template file
        with open(template_file_path, 'r') as template_file:
            template_content = template_file.read()

        # Replace tokens with predefined strings
        for token, replacement in token_replacements.items():
            template_content = template_content.replace(token, replacement)

        # Save the modified content to a new file
        with open(output_file_path, 'w') as output_file:
            output_file.write(template_content)

        print(f"File '{output_file_path}' created successfully.")

    except Exception as e:
        print(f"An error occurred: {str(e)}")

def run_another_program(input_file_path):
    # Replace this line with the code to run another program using the input_file_path
    # For example, you can use subprocess to run a command:
    
    subprocess.run(['/opt/claudio/salome/SALOME-9.11.0/salome', '-t', input_file_path])

    print(f"Salome executed with '{input_file_path}' as input.")

if __name__ == "__main__":
    # Define the template file, output file, and token replacements
    
    lx = 0.5
    ly = 0.16
    lz = 0.32
    
    nx = 5
    ny = [1, 2, 3, 4, 5, 8, 12, 16, 20]
    #nz = 16
    
    isQuad = ['True']
    
    template_file_path = 'box_dump_template.py'
    output_file_path = 'box_dump_out.py'
    
    for curr_ny in ny:
        for isHex20 in isQuad:
            
            token_replacements = {
                '{{LX}}': '{0:.4f}'.format(lx),
                '{{LYH}}': '{0:.4f}'.format(ly/2),
                '{{LZH}}': '{0:.4f}'.format(lz/2),
                '{{NX}}': '{0:d}'.format(nx),
                '{{NY}}': '{0:d}'.format(curr_ny),
                '{{NZ}}': '{0:d}'.format(curr_ny*2),     
                '{{QUAD}}': isHex20
            }

            # Replace tokens in the template file and save to a new file
            replace_tokens(template_file_path, output_file_path, token_replacements)
        
            # Run another program using the generated file as input
            run_another_program(output_file_path)
