#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.7.0 with dump python functionality
###

import sys
import salome

import numpy as np

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Box/')



width  = 0.8
height = 0.2
length = 0.5

n_seg_w = 40
n_seg_h = 20
n_seg_l = 5   # possibly always 5


width_dir  = 1 # width along y
height_dir = 2 # height along z
axis_dir   = 0 # axis along x, for automatic identification of R1 and R2

axis_name = ['X', 'Y', 'Z']


is_quadratic = True
is_biquadratic = False

if width == height:
    sect = 'SQ'
else:
    sect = 'RECT'

if is_quadratic:
    mesh_name = 'Box_dir_{0}_w{1:02d}_h{2:02d}_l{3:02d}_{4}_hex20'.format(axis_name[axis_dir], n_seg_w, n_seg_h, n_seg_l, sect)
else:
    mesh_name = 'Box_dir_{0}_w{1:02d}_h{2:02d}_l{3:02d}_{4}_hex08'.format(axis_name[axis_dir], n_seg_w, n_seg_h, n_seg_l, sect)


###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

V1 = [None] * 3
V2 = [None] * 3

V1[axis_dir] = 0.0
V2[axis_dir] = length

V1[width_dir] = -width/2
V2[width_dir] =  width/2

V1[height_dir] = -height/2
V2[height_dir] =  height/2



Vertex_1 = geompy.MakeVertex(*V1)
Vertex_2 = geompy.MakeVertex(*V2)
Box_1 = geompy.MakeBoxTwoPnt(Vertex_1, Vertex_2)
[z, y, x] = geompy.Propagate(Box_1)


FacesList = geompy.SubShapeAllSortedCentres(Box_1, geompy.ShapeType["FACE"])


allfaces = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(allfaces, [geompy.GetSubShapeID(Box_1, f) for f in FacesList])


R1 = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])
R2 = geompy.CreateGroup(Box_1, geompy.ShapeType["FACE"])

for act_f in FacesList:
    cm = geompy.MakeCDG(act_f)
    if cm is None:
        raise RuntimeError("MakeCDG(box) failed")
    else:
        #print("\nCentre of gravity of face has been successfully obtained:")
        coords = geompy.PointCoordinates(cm)
        if(np.abs(coords[axis_dir]) < 1e-6):
            print("belongs to R1")
            FaceID = geompy.GetSubShapeID(Box_1, act_f)
            geompy.UnionIDs(R1, [FaceID])
        elif(np.abs(coords[axis_dir]-length) < 1e-6):
            print("belongs to R2")
            FaceID = geompy.GetSubShapeID(Box_1, act_f)
            geompy.UnionIDs(R2, [FaceID])

# add objects to the group
SolidList = geompy.SubShapeAllSortedCentres(Box_1, geompy.ShapeType["SOLID"])

SolidID = geompy.GetSubShapeID(Box_1, SolidList[0])
Box_volume = geompy.CreateGroup(Box_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Box_volume, [SolidID])



geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertex_1, 'Vertex_1' )
geompy.addToStudy( Vertex_2, 'Vertex_2' )
geompy.addToStudy( Box_1, 'Box_1' )
geompy.addToStudyInFather( Box_1, z, 'z' )
geompy.addToStudyInFather( Box_1, y, 'y' )
geompy.addToStudyInFather( Box_1, x, 'x' )
geompy.addToStudyInFather( Box_1, R1, 'R1' )
geompy.addToStudyInFather( Box_1, R2, 'R2' )
geompy.addToStudyInFather( Box_1, Box_volume, 'Box_volume' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

section = smesh.Mesh(Box_1)
Hexa_3D = section.Hexahedron(algo=smeshBuilder.Hexa)
Quadrangle_2D = section.Quadrangle(algo=smeshBuilder.QUADRANGLE,geom=allfaces)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])


if axis_dir == 0:
    Regular_1D_l = section.Segment(geom=x)
    if width_dir == 1:
        Regular_1D_w = section.Segment(geom=y)
        Regular_1D_h = section.Segment(geom=z)
    else:
        Regular_1D_w = section.Segment(geom=z)
        Regular_1D_h = section.Segment(geom=y)
elif axis_dir == 1:
    Regular_1D_l = section.Segment(geom=y)
    if width_dir == 0:
        Regular_1D_w = section.Segment(geom=x)
        Regular_1D_h = section.Segment(geom=z)
    else:
        Regular_1D_w = section.Segment(geom=z)
        Regular_1D_h = section.Segment(geom=x)
else:
    Regular_1D_l = section.Segment(geom=z)
    if width_dir == 0:
        Regular_1D_w = section.Segment(geom=x)
        Regular_1D_h = section.Segment(geom=y)
    else:
        Regular_1D_w = section.Segment(geom=y)
        Regular_1D_h = section.Segment(geom=x)


nl = Regular_1D_l.NumberOfSegments(n_seg_l, None,[])
nw = Regular_1D_w.NumberOfSegments(n_seg_w, None,[])
nh = Regular_1D_h.NumberOfSegments(n_seg_h, None,[])


Mesh_volume = section.GroupOnGeom(Box_volume, 'Box_volume', SMESH.VOLUME)

R1_1 = section.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_1 = section.GroupOnGeom(R2,'R2',SMESH.NODE)
isDone = section.Compute()

if is_quadratic:
    section.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)

smesh.SetName(section, 'section')
try:
  section.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/Box/'+mesh_name+'.med',auto_groups=0,version=40,overwrite=1,meshPart=None,autoDimension=1)
  print("MED file saved")
except:
  print('ExportMED() failed. Invalid file name?')



## Set names of Mesh objects
smesh.SetName(R2_1, 'R2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Regular_1D_l.GetAlgorithm(), 'Regular_1D_l')
smesh.SetName(Regular_1D_h.GetAlgorithm(), 'Regular_1D_h')
smesh.SetName(Regular_1D_w.GetAlgorithm(), 'Regular_1D_w')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(section.GetMesh(), 'section')
smesh.SetName(nl, 'nl')
smesh.SetName(nw, 'nw')
smesh.SetName(nh, 'nh')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
