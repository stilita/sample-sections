#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 17:50:48 2024

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox



def theta_term(a,b, thresh):
    acc = 0
    curr_val = 1e6
    n = 1
    
    while curr_val > thresh:
        curr_val = n**(-5) * np.tanh(n*np.pi*b/(2*a))
        acc += curr_val
        #print("current n: {0:d} - current value: {1:f}".format(n, curr_val))
        n+=2
        
    return 1-192/(np.pi**5)*a/b*acc
        
def tau_term(a,b, z, num):
    acc = np.zeros_like(z)
    for n in range(1, num, 2):
        acc += (n**(-2))*(1- np.cosh(n*np.pi*z/(2*a))/np.cosh(n*np.pi*b/(2*a)))
        
    return acc
        

#*((-1)**((n-1)/2))

def tau_max_term(a, b, num):
    acc = 0
    for n in range(1, num, 2):
        acc += (n**2 * np.cosh(n*np.pi*b/(2*a)))**(-1)
        
    return acc

a = 0.08
b = 0.16

l = 0.5

E = 1
nu = 0.3
G = E/(2*(1+nu))


Tz = 1e-4
My = Tz*l

Mt = 1

Jz = 1/12*(2*a)*(2*b)**3

sigma_max = My/Jz*b

stress_edge0 = np.genfromtxt('Box3_Tz_face0_vert_right.csv', skip_header=1, delimiter=',')


stress_mid = np.genfromtxt('Box3_Tz_face1_vert_mid.csv', skip_header=1, delimiter=',')
stress_edge = np.genfromtxt('Box3_Tz_face1_vert_right.csv', skip_header=1, delimiter=',')


# plt.figure()
# plt.plot(stress_edge[:,-1], stress_edge[:,3], label=r'\sigma_{\xi} edge')
# plt.plot(stress_mid[:,-1], stress_mid[:,3], label=r'\sigma_{\xi} mid')
# plt.legend()


print("max stress: computed {0} - analytical {1}".format(max(stress_edge0[:,3]), sigma_max))


# z = np.linspace(-b, b, num = 35)
# #z = 0.0

# tau_timo = 16*G*theta_timo*a/(np.pi**2)*tau_term(a, b, z, 100)

# tau_max_timo = (2*G*a*theta_timo - 16*G*theta_timo*a/(np.pi**2)*tau_max_term(a, b, 200))

# brick = plt.imread('./line_crop.png')

# img = OffsetImage(brick, zoom=0.19, resample=True)
# image_position = (-0.01, 2)  # Replace with the desired (x, y) coordinates

# ab = AnnotationBbox(img, (0.5, 0), xycoords='axes fraction', box_alignment=(0.5,0.0), frameon=False, zorder=3)


# #plt.imshow(brick)
# fs = 20
# #plt.figure()
# fig, ax = plt.subplots()
# #for i in range(3,9):
# plt.plot(dtau[:,-1], -dtau[:,7], label=r'computed')
# plt.plot(z, tau_timo, 'o', label=r'Timoshenko')
# plt.plot(0.0, tau_max_roark, 'o', label='Roark (max)')
# #♦plt.plot(0.0, tau_max_timo, 'x', label='Timoshenko')

# ax.set_xlim(-b,b)
# ax.set_ylim(0,0.014)
# ax.set_ylabel(r'$\tau_{\xi \zeta}$', fontsize=fs)
# ax.set_xticks(ticks=np.linspace(-b, b, num=9))
# #ax.set_xticklabels(np.linspace(-b, b, num=9), fontsize=fs-4)
# #ax.tick_params(axis='y', labelsize=fs-4)
# ax.ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
# #ax.yaxis.offsetText.set_fontsize(fs-6)
# ax.grid()
# ax.legend(fontsize=fs-2);
# ax.legend();


# ax.add_artist(ab)

# plt.savefig('tau_Mx.png', dpi=300, bbox_inches='tight')

# print("Max stress: {0} - Timoshenko: {1} - Roark: {2}".format(np.max(-dtau[:,7]), tau_max_timo, tau_max_roark))


# # plt.axis('equal')