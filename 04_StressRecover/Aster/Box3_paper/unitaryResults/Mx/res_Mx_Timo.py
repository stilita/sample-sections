#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 17:50:48 2024

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from matplotlib.transforms import Affine2D


def compute_angle(vector1_start, vector1_end, vector2_start, vector2_end):
    # Convert the input points to NumPy arrays
    v1_start = np.array(vector1_start)
    v1_end = np.array(vector1_end)
    v2_start = np.array(vector2_start)
    v2_end = np.array(vector2_end)

    # Calculate the vectors
    vector1 = v1_end - v1_start
    vector2 = v2_end - v2_start

    # Calculate the dot product
    dot_product = np.dot(vector1, vector2)

    # Calculate the magnitudes of the vectors
    magnitude1 = np.linalg.norm(vector1)
    magnitude2 = np.linalg.norm(vector2)

    # Calculate the cosine of the angle
    cosine_angle = dot_product / (magnitude1 * magnitude2)

    # Calculate the angle in radians
    angle_rad = np.arccos(cosine_angle)

    # Convert the angle to degrees
    angle_deg = np.degrees(angle_rad)

    return angle_rad



def theta_term(a,b, thresh):
    acc = 0
    curr_val = 1e6
    n = 1
    
    while curr_val > thresh:
        curr_val = n**(-5) * np.tanh(n*np.pi*b/(2*a))
        acc += curr_val
        #print("current n: {0:d} - current value: {1:f}".format(n, curr_val))
        n+=2
        
    return 1-192/(np.pi**5)*a/b*acc
        
def tau_term(a,b, z, num):
    acc = np.zeros_like(z)
    for n in range(1, num, 2):
        acc += (n**(-2))*(1- np.cosh(n*np.pi*z/(2*a))/np.cosh(n*np.pi*b/(2*a)))
        
    return acc
        

#*((-1)**((n-1)/2))

def tau_max_term(a, b, num):
    acc = 0
    for n in range(1, num, 2):
        acc += (n**2 * np.cosh(n*np.pi*b/(2*a)))**(-1)
        
    return acc

d1 = np.genfromtxt('Mx_data_hor_mid.txt', skip_header=1, delimiter=',')


coords = d1[:,-3:]
disp = d1[:,0:3]


curr_coords = coords + disp



a = 0.08
b = 0.16

l = 0.5

E = 1
nu = 0.3
G = E/(2*(1+nu))


Mt = 1



theta_timo = 3*Mt/((2*a)**3*(2*b))/theta_term(a, b, 1e-9)/G

theta_timo_l = theta_timo*l

K = b*a**3*(16/3-3.36*a/b*(1-a**4/(12*b**4))) 

theta_roark = Mt*l/(K*G)

vector1_start = coords[0,:]
vector1_end = coords[-1,:]
vector2_start = curr_coords[0,:]
vector2_end = curr_coords[-1,:]

angle_between_vectors = compute_angle(vector1_start, vector1_end, vector2_start, vector2_end)/(2e-5)
print(f"Angle between vectors: {angle_between_vectors} - Timoshenko: {theta_timo_l} - Roark: {theta_roark}")


dtau = np.genfromtxt('Mx_data_vert_right.txt', skip_header=1, delimiter=',')

tau_max_roark = 3*Mt/(8*b*a**2)*(1+0.6095*a/b+0.8865*(a/b)**2-1.8023*(a/b)**3+0.9100*(a/b)**4)


z = np.linspace(-b, b, num = 35)
#z = 0.0

tau_timo = 16*G*theta_timo*a/(np.pi**2)*tau_term(a, b, z, 100)

tau_max_timo = (2*G*a*theta_timo - 16*G*theta_timo*a/(np.pi**2)*tau_max_term(a, b, 200))

brick = plt.imread('./line_crop.png')

img = OffsetImage(brick, zoom=0.21, resample=True)
image_position = (0.6, 2)  # Replace with the desired (x, y) coordinates

ab = AnnotationBbox(img, (0.5, 0.01), xycoords='axes fraction', box_alignment=(0.5,0.0), frameon=False, zorder=3)


#plt.imshow(brick)
fs = 18

plt.rcParams['font.family'] = ['serif']
plt.rcParams['font.serif'] = ['Times New Roman']

#plt.figure()
fig, ax = plt.subplots(figsize=(8,5))
#for i in range(3,9):
plt.plot(dtau[:,-1], -dtau[:,7]/(2e-5), color='r', label='computed (max = {0:.3f})'.format(np.max(-dtau[:,7])/(2e-5)))
plt.plot(z, tau_timo, 'o', label=r'Timoshenko (max = {0:.3f})'.format(tau_max_timo))
plt.plot(0.0, tau_max_roark, 'd', ms=8, color=[0.0, 0.5, 0.0], label='Roark ({0:.3f})'.format(tau_max_roark))
#♦plt.plot(0.0, tau_max_timo, 'x', label='Timoshenko')

ax.set_xlim(-b,b)
ax.set_ylim(0,800)
ax.set_ylabel(r'$\tau_{\xi \zeta} \: \left(\mathrm{N} \cdot \mathrm{m}^{-2}\right) $', fontsize=fs-4)
ax.set_xlabel(r'$\eta \: \left(\mathrm{m}\right)$', fontsize=fs-4)

act_ticks = [-0.16, -0.08, 0.0, 0.08, 0.16] #np.linspace(-b, b, num=5)
ax.set_xticks(ticks=act_ticks)
ax.set_xticklabels(act_ticks, fontsize=fs-4)
ax.tick_params(axis='y', labelsize=fs-4)
ax.ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
ax.yaxis.offsetText.set_fontsize(fs-6)
ax.grid()
ax.legend(fontsize=fs-4, loc=1)


ax.add_artist(ab)

plt.savefig('tau_Mx_upd.png', dpi=300, bbox_inches='tight')

print("Max stress: {0} - Timoshenko: {1} - Roark: {2}".format(np.max(-dtau[:,7])/(2e-5), tau_max_timo, tau_max_roark))


# plt.axis('equal')
