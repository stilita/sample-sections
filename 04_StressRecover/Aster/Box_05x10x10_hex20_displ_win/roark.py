# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 08:55:36 2023

@author: claudio.caccia
"""

w = 0.2
h = 0.25

l = 0.5

E = 1
nu =0.25

G = E/(2*(1+nu))

a = h/2
b = w/2

T = 1e-4

K = a*b**3*(16/3-3.36*b/a*(1-b**4/(12*a**4)))

tau_max = 3*T/(8*a*b**2)*(1+0.6095*b/a+0.8865*(b/a)**2-1.8023*(b/a)**3+0.9100*(b/a)**4)

theta = T*l/(G*K)

print("Tau max: {0}".format(tau_max))
print("theta: {0}".format(theta*180/3.1415)) 


dz = 0.0363922
dy = -0.0454903


Ty = 1e-3
A = w*h

t_max = 1.5*Ty/A

tau_mid = 0.028
tau_side = 0.036

print("tau max theor: {0}".format(t_max))
print("ratio side: {0}".format(tau_side/t_max))
print("ratio mid: {0}".format(tau_mid/t_max))

ratio = w/h

x1 = 1.0
x0 = 0.5

y1_mid = 0.940
y0_mid = 0.856

y1_side = 1.126
y0_side = 1.396


corr_mid  = y0_mid + (ratio - x0)*(y1_mid - y0_mid)/(x1-x0)
corr_side = y0_side + (ratio - x0)*(y1_side - y0_side)/(x1-x0)

print("theor ratio side: {0}".format(corr_side))
print("theor ratio mid: {0}".format(corr_mid))
