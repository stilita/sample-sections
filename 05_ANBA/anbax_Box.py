#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hanba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin
# from dolfin import compile_extension_module
import numpy as np
from petsc4py import PETSc

import anba4
import mshr

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

e_xx = 9.8e9
e_yy = 9.8e9
e_zz = 1.42e11
g_xy = 4.8e9
g_xz = 6.0e9
g_yz = 6.0e9
nu_xy = 0.34
nu_zx = 0.3
nu_zy = 0.3
#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy

# Meshing domain.

w = 0.2
h = 0.05 
Rect = mshr.Rectangle(dolfin.Point(-w/2, -h/2, 0.), dolfin.Point(w/2, h/2, 0.))

mesh = mshr.generate_mesh(Rect, 128)

dolfin.plot(mesh)

#import matplotlib.pyplot as plt
#plt.show()

EA  = e_yy*w*h
EJx = e_yy/12*w*h**3
EJy = e_yy/12*h*w**3


# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

materials.set_all(0)
fiber_orientations.set_all(45.0)
plane_orientations.set_all(0.0)

# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)

anba = anba4.anbax(mesh, 2, matLibrary, materials, plane_orientations, fiber_orientations)
stiff = anba.compute()
stiff.view()

stiff1 = stiff.getDenseArray()

# print(foil_stiff)
# need to copy otherwise I get garbage
f1 = np.copy(stiff1)

print("EA: anba {0:.6e}, Ewh {1:.6e}".format(f1[2,2], EA))
#print("GAx: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[0,0], GA_Timo1))
#print("GAy: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[1,1], GA_Timo2))

#print("GJ: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[5,5], GJ_Timo))
print("EJx: anba {0:.6e}, 1/12Ewh^3 {1:.6e}".format(f1[3,3], EJx))
print("EJy: anba {0:.6e}, 1/12Ehw^3 {1:.6e}".format(f1[4,4], EJy))

#mass = anba.inertia()
#mass.view()

#stress_result_file = dolfin.XDMFFile('Stress.xdmf')
#stress_result_file.parameters['functions_share_mesh'] = True
#stress_result_file.parameters['rewrite_function_mesh'] = False
#stress_result_file.parameters["flush_output"] = True

#anba.stress_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
#anba.strain_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
#stress_result_file.write(anba.STRESS, t = 0.)
#stress_result_file.write(anba.STRAIN, t = 1.)


