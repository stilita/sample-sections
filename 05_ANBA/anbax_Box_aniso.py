#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hanba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin
# from dolfin import compile_extension_module
import numpy as np
from petsc4py import PETSc

import anba4
import mshr

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.


El = 2.00e11   #z
Et = 1.0e10    #x
En = 5.0e9     #y

Gln = 1.2e10   #yz
Glt = 6.0e9    #xz
Gtn = 3.0e9    #xy

nu_lt = 0.3
nu_ln = 0.3
nu_tn = 0.3


e_xx = Et
e_yy = En
e_zz = El
g_xy = Gtn
g_xz = Glt
g_yz = Gln
nu_xy = nu_tn
nu_zx = nu_ln
nu_zy = nu_lt

#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy

# Meshing domain.

w = 0.8

if w == 0.8:
    shape = 'RECT'
    h = 0.2
    timo_shear_coeff_x = 0.1333290635109205
    timo_shear_coeff_y = 0.0933947901258445
elif w == 0.25:
    h = w
    shape = 'SQ'
    timo_shear_coeff_x = 0.051763521409511855
    timo_shear_coeff_y = timo_shear_coeff_x
else:
    print("width not valid for estimate")
     
Rect = mshr.Rectangle(dolfin.Point(-w/2, -h/2, 0.), dolfin.Point(w/2, h/2, 0.))

mesh = mshr.generate_mesh(Rect, 128)

#dolfin.plot(mesh)

#import matplotlib.pyplot as plt
#plt.show()



fo = 45
po = 90

print("fiber orientation: {0:d}".format(fo))

if fo == 0:
    EA  = e_zz*w*h
    EJx = e_zz/12*w*h**3
    EJy = e_zz/12*h*w**3

    GAx = timo_shear_coeff_x * g_xz
    GAy = timo_shear_coeff_y * g_yz
elif fo == 90:
    EA  = e_xx*w*h
    EJx = e_xx/12*w*h**3
    EJy = e_xx/12*h*w**3

    GAx = timo_shear_coeff_x * g_xz
    GAy = timo_shear_coeff_y * g_xy
else:
    print("fiber orientation not valid for estimate")
    EA  = -1
    EJx = -1
    EJy = -1

    GAx = -1
    GAy = -1



# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

materials.set_all(0)
fiber_orientations.set_all(fo)
plane_orientations.set_all(po)

# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)

anba = anba4.anbax(mesh, 2, matLibrary, materials, plane_orientations, fiber_orientations)
stiff = anba.compute()
stiff.view()

stiff1 = stiff.getDenseArray()

# print(foil_stiff)
# need to copy otherwise I get garbage
f1 = np.copy(stiff1)

print("EA: anba {0:.6e}, Ewh {1:.6e}".format(f1[2,2], EA))
print("GAx: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[0,0], GAx))
print("GAy: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[1,1], GAy))

#print("GJ: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[5,5], GJ_Timo))
print("EJx: anba {0:.6e}, 1/12Ewh^3 {1:.6e}".format(f1[3,3], EJx))
print("EJy: anba {0:.6e}, 1/12Ehw^3 {1:.6e}".format(f1[4,4], EJy))

print("Gxy coeff x: {0:.6e}".format(g_xy*timo_shear_coeff_x))
print("Gxy coeff y: {0:.6e}".format(g_xy*timo_shear_coeff_y))
print("Gxz coeff x: {0:.6e}".format(g_xz*timo_shear_coeff_x))
print("Gxz coeff y: {0:.6e}".format(g_xz*timo_shear_coeff_y))
print("Gyz coeff x: {0:.6e}".format(g_yz*timo_shear_coeff_x))
print("Gyz coeff y: {0:.6e}".format(g_yz*timo_shear_coeff_y))



np.savetxt('ANBA_{0}_fo{1:d}_po{2:d}.txt'.format(shape, fo, po), f1)



#mass = anba.inertia()
#mass.view()

#stress_result_file = dolfin.XDMFFile('Stress.xdmf')
#stress_result_file.parameters['functions_share_mesh'] = True
#stress_result_file.parameters['rewrite_function_mesh'] = False
#stress_result_file.parameters["flush_output"] = True

#anba.stress_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
#anba.strain_field([1., 0., 0.,], [0., 0., 0.], "local", "paraview")
#stress_result_file.write(anba.STRESS, t = 0.)
#stress_result_file.write(anba.STRAIN, t = 1.)


