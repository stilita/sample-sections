#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.10.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Documents/GitHub/sample-sections/01_MeshGeneration/T2_Bauchau')

####################################################
##       Begin of NoteBook variables section      ##
####################################################

b = 0.2
h = 0.2
length = 0.5

tp = 0.000625
#tp = 0.005

# mesh
n_thick = 1   # number of segments in thickness
n_width = 12  # number of segmenst in width (has to be even)
n_width_2 = n_width // 2
n_height = 10 # number of segments in height
n_length = 5  # number of blocks

is_quadratic = True
is_biquadratic = False

if is_quadratic:
    mesh_name = "T2_{0:02d}w_{1:02d}h_{2:02d}t_hex20".format(n_width, n_height, n_thick)
else:
    mesh_name = "T2_{0:02d}w_{1:02d}h_{2:02d}t_hex08".format(n_width, n_height, n_thick)


####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

vertices = []
lines = []
faces = []


# vertices first lower 4 plies
for i in range(5):
    vertices.append(geompy.MakeVertex(0, -b/2, -h/2+tp*i))
    if i == 4:
        vertices.append(geompy.MakeVertex(0,  0, -h/2+tp*i))
    vertices.append(geompy.MakeVertex(0,  b/2, -h/2+tp*i))

# vertices first upper 4 plies
for i in range(5):
    vertices.append(geompy.MakeVertex(0, -b/2,  h/2-tp*i))
    if i == 4:
        vertices.append(geompy.MakeVertex(0,  0,  h/2-tp*i))
    vertices.append(geompy.MakeVertex(0,  b/2,  h/2-tp*i))


#vertices second lower 4 plies
for i in range(4):
    vertices.append(geompy.MakeVertex(0, -b/2, -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0, -tp*(i+1), -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  tp*(i+1), -h/2+tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  b/2, -h/2+tp*(5+i)))

# vertices for second upper 4 plies
for i in range(4):
    vertices.append(geompy.MakeVertex(0, -b/2, h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0, -tp*(i+1), h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  tp*(i+1), h/2-tp*(5+i)))
    vertices.append(geompy.MakeVertex(0,  b/2, h/2-tp*(5+i)))


#lines for first lower 4 plies
lines.append(geompy.MakeLineTwoPnt(vertices[0], vertices[1]))

for i in range(0,5,2):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+3]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+2]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+2], vertices[i]))

# last face with 5 lines
lines.append(geompy.MakeLineTwoPnt(vertices[7], vertices[10]))
lines.append(geompy.MakeLineTwoPnt(vertices[10], vertices[9]))
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[8]))
lines.append(geompy.MakeLineTwoPnt(vertices[8], vertices[6]))


# lines for first upper 4 plies
lines.append(geompy.MakeLineTwoPnt(vertices[11], vertices[12]))

for i in range(11,16,2):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+2]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+2], vertices[i+3]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+1]))


# last face with 5 lines
lines.append(geompy.MakeLineTwoPnt(vertices[17], vertices[19]))
lines.append(geompy.MakeLineTwoPnt(vertices[19], vertices[20]))
lines.append(geompy.MakeLineTwoPnt(vertices[20], vertices[21]))
lines.append(geompy.MakeLineTwoPnt(vertices[21], vertices[18]))


#lines for second lower 4 plies

#first ply
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[23]))
lines.append(geompy.MakeLineTwoPnt(vertices[23], vertices[22]))
lines.append(geompy.MakeLineTwoPnt(vertices[22], vertices[8]))

lines.append(geompy.MakeLineTwoPnt(vertices[10], vertices[25]))
lines.append(geompy.MakeLineTwoPnt(vertices[25], vertices[24]))
lines.append(geompy.MakeLineTwoPnt(vertices[24], vertices[9]))

# other 3 plies
for i in range(22,31,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+5]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+5], vertices[i+4]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[i]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+7]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+7], vertices[i+6]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+6], vertices[i+2]))


#lines for second upper 4 plies

#first ply
lines.append(geompy.MakeLineTwoPnt(vertices[20], vertices[39]))
lines.append(geompy.MakeLineTwoPnt(vertices[39], vertices[38]))
lines.append(geompy.MakeLineTwoPnt(vertices[38], vertices[19]))

lines.append(geompy.MakeLineTwoPnt(vertices[21], vertices[41]))
lines.append(geompy.MakeLineTwoPnt(vertices[41], vertices[40]))
lines.append(geompy.MakeLineTwoPnt(vertices[40], vertices[20]))


# other 3 plies
for i in range(38,47,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+5]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+5], vertices[i+4]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+4], vertices[i]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+3], vertices[i+7]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+7], vertices[i+6]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+6], vertices[i+2]))



# vertical plies
for i in range(23,36,4):
    lines.append(geompy.MakeLineTwoPnt(vertices[i], vertices[i+16]))
    lines.append(geompy.MakeLineTwoPnt(vertices[i+1], vertices[i+17]))

#central line
lines.append(geompy.MakeLineTwoPnt(vertices[9], vertices[20]))


# first bottom face
faces.append(geompy.MakeFaceWires([lines[0], lines[1], lines[2], lines[3]], 1))

for i in range(2,7,3):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+2], lines[i+3], lines[i+4]], 1))

# last face 5 lines
faces.append(geompy.MakeFaceWires([lines[8], lines[10], lines[11], lines[12], lines[13]], 1))

#first upper face
faces.append(geompy.MakeFaceWires([lines[14], lines[15], lines[16], lines[17]], 1))

for i in range(16,21,3):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+2], lines[i+3], lines[i+4]], 1))

# last face 5 lines
faces.append(geompy.MakeFaceWires([lines[22], lines[24], lines[25], lines[26], lines[27]], 1))



# first faces second lower ply
faces.append(geompy.MakeFaceWires([lines[12], lines[28], lines[29], lines[30]], 1))
faces.append(geompy.MakeFaceWires([lines[11], lines[31], lines[32], lines[33]], 1))

# other 3 lower plies
for i in range(29,42,6):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+5], lines[i+6], lines[i+7]], 1))
    faces.append(geompy.MakeFaceWires([lines[i+3], lines[i+8], lines[i+9], lines[i+10]], 1))


# first faces second upper ply
faces.append(geompy.MakeFaceWires([lines[25], lines[52], lines[53], lines[54]], 1))
faces.append(geompy.MakeFaceWires([lines[26], lines[55], lines[56], lines[57]], 1))

# other 3 upper plies
for i in range(53,66,6):
    faces.append(geompy.MakeFaceWires([lines[i], lines[i+5], lines[i+6], lines[i+7]], 1))
    faces.append(geompy.MakeFaceWires([lines[i+3], lines[i+8], lines[i+9], lines[i+10]], 1))

faces.append(geompy.MakeFaceWires([lines[82], lines[46], lines[80], lines[70]], 1))
faces.append(geompy.MakeFaceWires([lines[80], lines[40], lines[78], lines[64]], 1))
faces.append(geompy.MakeFaceWires([lines[78], lines[34], lines[76], lines[58]], 1))
faces.append(geompy.MakeFaceWires([lines[76], lines[28], lines[84], lines[52]], 1))

faces.append(geompy.MakeFaceWires([lines[83], lines[51], lines[81], lines[75]], 1))
faces.append(geompy.MakeFaceWires([lines[81], lines[45], lines[79], lines[69]], 1))
faces.append(geompy.MakeFaceWires([lines[79], lines[39], lines[77], lines[63]], 1))
faces.append(geompy.MakeFaceWires([lines[77], lines[33], lines[84], lines[57]], 1))


extrude = []

for face in faces:
    extrude.append(geompy.MakePrismVecH(face, OX, length))

T_comp = geompy.MakeCompound(extrude)


thick_ind = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 26, 28, 30, 32, 38, 39, 41, 42, 44, 45, 50, 52, 53, 54, 56, 57, 68, 69]

half_width_ind = [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 58, 59 ,60, 61, 62, 63, 64, 65, 66, 67]

width_ind = [33, 34, 35, 36, 46, 47, 48, 49]

height_ind = [27, 29, 31, 37, 40, 43, 51, 55]

all_ind = range(102)

length_ind = []

for i in all_ind:
    if i not in thick_ind and i not in half_width_ind and i not in width_ind and i not in height_ind:
        length_ind.append(i)

#print(length_ind)

Compound = geompy.Propagate(T_comp)
thickness = geompy.UnionListOfGroups([Compound[i] for i in thick_ind])
half_width = geompy.UnionListOfGroups([Compound[i] for i in half_width_ind])
width = geompy.UnionListOfGroups([Compound[i] for i in width_ind])
height = geompy.UnionListOfGroups([Compound[i] for i in height_ind])
length = geompy.UnionListOfGroups([Compound[i] for i in length_ind])

deg_0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_0, [247, 213, 560, 594, 104, 70, 288, 322, 934, 1070])
deg_90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_90, [900, 1036, 356, 390])
geompy.DifferenceIDs(deg_90, [900, 1036, 356, 390])
geompy.UnionIDs(deg_90, [900, 1036, 356, 390, 628, 662])
deg_45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_45, [764, 832, 492, 798, 968, 526, 145, 2])
deg_m45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_m45, [696, 866, 424, 730, 1002, 458, 179, 36])


deg_0 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_0, [247, 213, 560, 594, 104, 70, 288, 322, 934, 1070])

deg_90 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_90, [900, 1036, 356, 390])
geompy.DifferenceIDs(deg_90, [900, 1036, 356, 390])
geompy.UnionIDs(deg_90, [900, 1036, 356, 390, 628, 662])

deg_45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_45, [764, 832, 492, 798, 968, 526, 145, 2])

deg_m45 = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_m45, [696, 866, 424, 730, 1002, 458, 179, 36])

geompy.DifferenceIDs(deg_0, [247, 213, 560, 594, 104, 70, 288, 322, 934, 1070])
geompy.UnionIDs(deg_0, [145, 179, 2, 36, 526, 968, 492, 832, 764, 798])

geompy.DifferenceIDs(deg_90, [900, 1036, 356, 390, 628, 662])
geompy.UnionIDs(deg_90, [424, 458, 696, 730])

deg_90_v = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_90_v, [866, 1002])

geompy.DifferenceIDs(deg_45, [764, 832, 492, 798, 968, 526, 145, 2])
geompy.UnionIDs(deg_45, [213, 628, 662, 70, 356, 390])

deg_45_v = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])
geompy.UnionIDs(deg_45_v, [900, 1036])

geompy.DifferenceIDs(deg_m45, [696, 866, 424, 730, 1002, 458, 179, 36])
geompy.UnionIDs(deg_m45, [288, 322, 104, 560, 594, 247])
deg_m45_v = geompy.CreateGroup(T_comp, geompy.ShapeType["SOLID"])



R1 = geompy.CreateGroup(T_comp, geompy.ShapeType["FACE"])
geompy.UnionIDs(R1, [175, 209, 243, 284, 590, 624, 692, 658, 726, 794, 760, 828, 862, 896, 930, 964, 1100, 1066, 1032, 998, 522, 556, 488, 454, 386, 420, 352, 318, 141, 66, 100, 32])
R2 = geompy.CreateGroup(T_comp, geompy.ShapeType["FACE"])
geompy.UnionIDs(R2, [177, 211, 245, 286, 592, 626, 660, 694, 728, 762, 796, 830, 864, 898, 932, 966, 1102, 1068, 1034, 1000, 524, 558, 490, 456, 388, 422, 320, 354, 143, 102, 68, 34])




geompy.UnionIDs(deg_m45_v, [934, 1070])
deg_0.SetColor(SALOMEDS.Color(0.333333,1,0))
deg_90.SetColor(SALOMEDS.Color(0.666667,1,1))
deg_45.SetColor(SALOMEDS.Color(1,0.666667,0))
deg_m45.SetColor(SALOMEDS.Color(0.666667,0,1))
deg_90_v.SetColor(SALOMEDS.Color(0,0.666667,1))
deg_45_v.SetColor(SALOMEDS.Color(1,0.666667,0.498039))
deg_m45_v.SetColor(SALOMEDS.Color(0.666667,0,0.498039))


#Tshell = geompy.MakeShell(faces)
#T_Solid = geompy.MakeThickSolid(Tshell, length, [], True)
#edge = geompy.Propagate(T_Solid)

#thickness = geompy.UnionListOfGroups([edge[0], edge[1], edge[2], edge[3], edge[4], edge[5], edge[6], edge[7], edge[8], edge[9], edge[10], edge[11], edge[17], edge[18], edge[19], edge[20], edge[23], edge[24]])
#half_width = geompy.UnionListOfGroups([edge[12], edge[13], edge[21], edge[22]])
#width = geompy.UnionListOfGroups([edge[15], edge[16]])


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

#for i, vertex in enumerate(vertices):
#    geompy.addToStudy( vertex, f'Vertex_{i}' )


#for i, line in enumerate(lines):
#    geompy.addToStudy( line, f'Line_{i}' )

#for i, solid in enumerate(extrude):
#    geompy.addToStudy( solid, f'solid_{i}' )
    
#geompy.addToStudy( Tshell, 'T_shell' )
#geompy.addToStudy( T_Solid, 'T_Solid' )


#for i, edge in enumerate(Edgepart):
#    geompy.addToStudyInFather( Tsolid, edge, f'edge_{i}' )

#geompy.addToStudyInFather( T_Solid, edge[14], 'height' )
#geompy.addToStudyInFather( T_Solid, edge[25], 'length' )
#geompy.addToStudyInFather( T_Solid, thickness, 'thickness' )
#geompy.addToStudyInFather( T_Solid, half_width, 'half_width' )
#geompy.addToStudyInFather( T_Solid, width, 'width' )

geompy.addToStudy( T_comp, 'T_comp' )
geompy.addToStudyInFather( T_comp, thickness, 'thickness' )
geompy.addToStudyInFather( T_comp, half_width, 'half_width' )
geompy.addToStudyInFather( T_comp, width, 'width' )
geompy.addToStudyInFather( T_comp, height, 'height' )
geompy.addToStudyInFather( T_comp, length, 'length' )
geompy.addToStudyInFather( T_comp, deg_0, 'deg_0' )
geompy.addToStudyInFather( T_comp, deg_90, 'deg_90' )
geompy.addToStudyInFather( T_comp, deg_45, 'deg_45' )
geompy.addToStudyInFather( T_comp, deg_m45, 'deg_m45' )
geompy.addToStudyInFather( T_comp, deg_90_v, 'deg_90_v' )
geompy.addToStudyInFather( T_comp, deg_45_v, 'deg_45_v' )
geompy.addToStudyInFather( T_comp, deg_m45_v, 'deg_m45_v' )
geompy.addToStudyInFather( T_comp, R1, 'R1' )
geompy.addToStudyInFather( T_comp, R2, 'R2' )


###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

T2Mesh = smesh.Mesh(T_comp)

Hexa_3D = T2Mesh.Hexahedron(algo=smeshBuilder.Hexa)

Quadrangle_2D = T2Mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_REDUCED,-1,[],[])

thickness_1 = T2Mesh.GroupOnGeom(thickness,'thickness',SMESH.EDGE)
half_width_1 = T2Mesh.GroupOnGeom(half_width,'half_width',SMESH.EDGE)
width_1 = T2Mesh.GroupOnGeom(width,'width',SMESH.EDGE)
height_1 = T2Mesh.GroupOnGeom(height,'height',SMESH.EDGE)
length_1 = T2Mesh.GroupOnGeom(length,'length',SMESH.EDGE)

deg_0_1 = T2Mesh.GroupOnGeom(deg_0,'deg_0',SMESH.VOLUME)
deg_90_1 = T2Mesh.GroupOnGeom(deg_90,'deg_90',SMESH.VOLUME)
deg_45_1 = T2Mesh.GroupOnGeom(deg_45,'deg_45',SMESH.VOLUME)
deg_m45_1 = T2Mesh.GroupOnGeom(deg_m45,'deg_m45',SMESH.VOLUME)

deg_90_v_1 = T2Mesh.GroupOnGeom(deg_90_v,'deg_90_v',SMESH.VOLUME)
deg_45_v_1 = T2Mesh.GroupOnGeom(deg_45_v,'deg_45_v',SMESH.VOLUME)
deg_m45_v_1 = T2Mesh.GroupOnGeom(deg_m45_v,'deg_m45_v',SMESH.VOLUME)


R1_1 = T2Mesh.GroupOnGeom(R1,'R1',SMESH.FACE)
R2_1 = T2Mesh.GroupOnGeom(R2,'R2',SMESH.FACE)

Regular_1D_1 = T2Mesh.Segment(geom=thickness)
n_thick = Regular_1D_1.NumberOfSegments(n_thick)

Regular_1D_2 = T2Mesh.Segment(geom=width)
nw = Regular_1D_2.NumberOfSegments(n_width)

Regular_1D_3 = T2Mesh.Segment(geom=half_width)
nw2 = Regular_1D_3.NumberOfSegments(n_width_2)

Regular_1D_4 = T2Mesh.Segment(geom=height)
nh = Regular_1D_4.NumberOfSegments(n_height)

Regular_1D_5 = T2Mesh.Segment(geom=length)
nl = Regular_1D_5.NumberOfSegments(n_length)

isDone = T2Mesh.Compute()

coincident_nodes_on_part = T2Mesh.FindCoincidentNodesOnPart( [ T2Mesh ], 1e-05, [], 0 )
#print(coincident_nodes_on_part)
T2Mesh.MergeNodes(coincident_nodes_on_part, [], 0)

if is_quadratic:
    T2Mesh.ConvertToQuadratic(theForce3d=True, theToBiQuad=is_biquadratic)

#[ smeshObj_1, smeshObj_2, smeshObj_3, smeshObj_4, smeshObj_5, smeshObj_6, smeshObj_7, smeshObj_8, smeshObj_9, smeshObj_10, smeshObj_11, smeshObj_12, smeshObj_13, smeshObj_14, smeshObj_15, smeshObj_16, smeshObj_17, smeshObj_18, smeshObj_19, smeshObj_20, smeshObj_21, smeshObj_22, smeshObj_23, smeshObj_24, smeshObj_25, smeshObj_26, smeshObj_27, smeshObj_28, smeshObj_29, smeshObj_30, smeshObj_31, smeshObj_32, smeshObj_33, smeshObj_34, smeshObj_35, smeshObj_36, smeshObj_37, smeshObj_38, smeshObj_39, smeshObj_40, smeshObj_41, smeshObj_42, smeshObj_43, smeshObj_44, smeshObj_45, smeshObj_46, smeshObj_47, smeshObj_48, smeshObj_49, smeshObj_50, smeshObj_51, smeshObj_52, smeshObj_53, smeshObj_54, smeshObj_55, smeshObj_56, smeshObj_57, smeshObj_58, smeshObj_59, smeshObj_60, smeshObj_61, smeshObj_62, smeshObj_63, smeshObj_64, smeshObj_65, smeshObj_66, smeshObj_67, smeshObj_68, smeshObj_69, smeshObj_70, smeshObj_71, smeshObj_72, smeshObj_73, smeshObj_74, smeshObj_75, smeshObj_76, smeshObj_77, smeshObj_78, smeshObj_79, smeshObj_80, smeshObj_81, smeshObj_82, smeshObj_83, smeshObj_84, smeshObj_85, smeshObj_86, smeshObj_87, smeshObj_88, smeshObj_89, smeshObj_90, smeshObj_91, smeshObj_92, smeshObj_93, smeshObj_94, smeshObj_95, smeshObj_96, smeshObj_97, smeshObj_98, smeshObj_99, smeshObj_100, smeshObj_101, smeshObj_102, thickness_1, half_width_1, width_1, height_1, length_1, deg_0_1, deg_90_1, deg_45_1, deg_m45_1, R1_1, R2_1 ] = T2Mesh.GetGroups()
R1_2 = T2Mesh.GroupOnGeom(R1,'R1',SMESH.NODE)
R2_2 = T2Mesh.GroupOnGeom(R2,'R2',SMESH.NODE)

smesh.SetName(T2Mesh.GetMesh(), 'T2Mesh')



try:
    T2Mesh.ExportMED(r'/home/claudio/Projects/Studies/BeamProperties/C_Section/sample-sections/01_MeshGeneration/T2_Bauchau/'+mesh_name+'.med', auto_groups=0, version=40, overwrite=1, meshPart=None, autoDimension=1)
    print('MED file saved')
except:
    print('ExportMED() failed. Invalid file name?')

Sub_mesh_1 = Regular_1D_1.GetSubMesh()
Sub_mesh_2 = Regular_1D_2.GetSubMesh()
Sub_mesh_3 = Regular_1D_3.GetSubMesh()
Sub_mesh_4 = Regular_1D_4.GetSubMesh()
Sub_mesh_5 = Regular_1D_5.GetSubMesh()

## some objects were removed
aStudyBuilder = salome.myStudy.NewBuilder()
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_69))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_68))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_67))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_66))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_65))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_78))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_64))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_79))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_63))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_76))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_62))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_77))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_61))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_74))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_60))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_18))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_75))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_19))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_72))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_73))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_70))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_71))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_10))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_11))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_12))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_13))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_14))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_15))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_16))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_17))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_49))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_48))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_45))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_58))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_44))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_59))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_47))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_46))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_41))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_54))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_40))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_55))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_43))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_56))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_42))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_57))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_50))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_51))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_52))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_81))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_53))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_80))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_83))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_82))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_85))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_90))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_84))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_100))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_91))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_87))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_101))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_92))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_86))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_102))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_93))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_89))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_94))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_88))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_95))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_96))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_97))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_98))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_99))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_9))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_29))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_8))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_28))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_38))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_39))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_23))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_22))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_1))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_21))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_20))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_3))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_27))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_2))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_32))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_26))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_5))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_33))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_25))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_4))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_30))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_24))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_7))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_31))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_6))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_36))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_37))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_34))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)
# SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(smeshObj_35))
# if SO: aStudyBuilder.RemoveObjectWithChildren(SO)

## Set names of Mesh objects
smesh.SetName(Hexa_3D.GetAlgorithm(), 'Hexa_3D')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(n_thick, 'n_thick')
smesh.SetName(nw, 'nw')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(nl, 'nl')
smesh.SetName(nw2, 'nw2')
smesh.SetName(R1_1, 'R1')
smesh.SetName(nh, 'nh')
smesh.SetName(R2_1, 'R2')
smesh.SetName(Sub_mesh_3, 'Sub-mesh_3')
smesh.SetName(Sub_mesh_2, 'Sub-mesh_2')
smesh.SetName(Sub_mesh_1, 'Sub-mesh_1')
smesh.SetName(Sub_mesh_5, 'Sub-mesh_5')
smesh.SetName(Sub_mesh_4, 'Sub-mesh_4')

smesh.SetName(thickness_1, 'thickness')
smesh.SetName(half_width_1, 'half_width')
smesh.SetName(width_1, 'width')
smesh.SetName(height_1, 'height')
smesh.SetName(length_1, 'length')

smesh.SetName(deg_0_1, 'deg_0')
smesh.SetName(deg_45_1, 'deg_45')
smesh.SetName(deg_m45_1, 'deg_m45')
smesh.SetName(deg_90_1, 'deg_90')
smesh.SetName(deg_45_v_1, 'deg_45_v')
smesh.SetName(deg_m45_v_1, 'deg_m45_v')
smesh.SetName(deg_90_v_1, 'deg_90_v')

smesh.SetName(R2_2, 'R2')
smesh.SetName(R1_2, 'R1')



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
