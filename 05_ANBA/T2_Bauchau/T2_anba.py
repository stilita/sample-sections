import dolfin #import *
# from dolfin import compile_extension_module
import time
import math
import numpy as np
from petsc4py import PETSc
import os
import matplotlib.pyplot as plt

import anba4 #import *
import mshr

# from voight_notation import stressVectorToStressTensor, stressTensorToStressVector, strainVectorToStrainTensor, strainTensorToStrainVector
# from material import material
# from anbax import anbax

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.


El = 181.0e9
En = 10.3e9
Glt = 7.17e9
nu_lt = 0.28
nu_tn = 0.33

e_xx = En
e_yy = En
e_zz = El
g_xy = En/(2*(1+nu_tn))
g_xz = Glt
g_yz = Glt
nu_xy = nu_tn
nu_zx = nu_lt
nu_zy = nu_lt

#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy


width = 0.2
height = 0.2

tp = 0.000625
#tp = 0.005


vertices = []

# vertices first lower 4 plies
for i in range(8):
    vertices.append(dolfin.Point( -width/2.*(-1)**(i), height/2. -tp*(i//2), 0.))

for i in range(-1, 2):
    vertices.append(dolfin.Point( width/2*i, height/2. -tp*4, 0.))

for i in range(4):
    vertices.append(dolfin.Point( -width/2, height/2. -tp*(5+i), 0.))
    vertices.append(dolfin.Point( -tp*(i+1), height/2. -tp*(5+i), 0.))
    vertices.append(dolfin.Point( tp*(i+1), height/2. -tp*(5+i), 0.))
    vertices.append(dolfin.Point( width/2, height/2. -tp*(5+i), 0.))


for i in range(4):
    vertices.append(dolfin.Point( -width/2, -height/2. +tp*(8-i), 0.))
    vertices.append(dolfin.Point( -tp*(4-i), -height/2. +tp*(8-i), 0.))
    vertices.append(dolfin.Point( tp*(4-i), -height/2. +tp*(8-i), 0.))
    vertices.append(dolfin.Point( width/2, -height/2. +tp*(8-i), 0.))

for i in range(-1, 2):
    vertices.append(dolfin.Point( width/2*i, -height/2. +tp*4, 0.))

for i in range(7, -1, -1):
    vertices.append(dolfin.Point( width/2.*(-1)**(i), -height/2. +tp*(i//2), 0.))


for i in range(len(vertices)):
    print("point {0:d} x: {1:f} y: {2:f} ".format(i,vertices[i].x(), vertices[i].y()))


Plies = [mshr.Polygon((vertices[2], vertices[3], vertices[1], vertices[0])),
         mshr.Polygon((vertices[4], vertices[5], vertices[3], vertices[2])),
         mshr.Polygon((vertices[6], vertices[7], vertices[5], vertices[4])),
         mshr.Polygon((vertices[8], vertices[9], vertices[10], vertices[7], vertices[6])),
         mshr.Polygon((vertices[11], vertices[12], vertices[9], vertices[8])),
         mshr.Polygon((vertices[13], vertices[14], vertices[10], vertices[9])),
         mshr.Polygon((vertices[15], vertices[16], vertices[12], vertices[11])),
         mshr.Polygon((vertices[17], vertices[18], vertices[14], vertices[13])),
         mshr.Polygon((vertices[19], vertices[20], vertices[16], vertices[15])),
         mshr.Polygon((vertices[21], vertices[22], vertices[18], vertices[17])),
         mshr.Polygon((vertices[23], vertices[24], vertices[20], vertices[19])),
         mshr.Polygon((vertices[25], vertices[26], vertices[22], vertices[21])),
         mshr.Polygon((vertices[28], vertices[32], vertices[20], vertices[24])),
         mshr.Polygon((vertices[32], vertices[36], vertices[16], vertices[20])),
         mshr.Polygon((vertices[36], vertices[40], vertices[12], vertices[16])),
         mshr.Polygon((vertices[40], vertices[44], vertices[9], vertices[12])),
         mshr.Polygon((vertices[44], vertices[41], vertices[13], vertices[9])),
         mshr.Polygon((vertices[41], vertices[37], vertices[17], vertices[13])),
         mshr.Polygon((vertices[37], vertices[33], vertices[21], vertices[17])),
         mshr.Polygon((vertices[33], vertices[29], vertices[25], vertices[21])),
         mshr.Polygon((vertices[31], vertices[32], vertices[28], vertices[27])),
         mshr.Polygon((vertices[33], vertices[34], vertices[30], vertices[29])),
         mshr.Polygon((vertices[35], vertices[36], vertices[32], vertices[31])),
         mshr.Polygon((vertices[37], vertices[38], vertices[34], vertices[33])),
         mshr.Polygon((vertices[39], vertices[40], vertices[36], vertices[35])),
         mshr.Polygon((vertices[41], vertices[42], vertices[38], vertices[37])),
         mshr.Polygon((vertices[43], vertices[44], vertices[40], vertices[39])),
         mshr.Polygon((vertices[44], vertices[45], vertices[42], vertices[41])),
         mshr.Polygon((vertices[46], vertices[47], vertices[45], vertices[44], vertices[43])),
         mshr.Polygon((vertices[48], vertices[49], vertices[47], vertices[46])),
         mshr.Polygon((vertices[50], vertices[51], vertices[49], vertices[48])),
         mshr.Polygon((vertices[52], vertices[53], vertices[51], vertices[50]))] #,

T2 = Plies[0] + Plies[1]

for i in range(2,len(Plies)):
    T2 += Plies[i]

for i, current_ply in enumerate(Plies):
    T2.set_subdomain(i+1, Plies[i])


# C_shape.set_subdomain(1, Box1)
# C_shape.set_subdomain(2, Box2)
# C_shape.set_subdomain(3, Box3)
# C_shape.set_subdomain(4, Box4)
#
mesh = mshr.generate_mesh(T2, 128)
mf = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim(), mesh.domains())

mesh_points=mesh.coordinates()
dolfin.plot(mesh)
dolfin.plot(mf)
plt.show()

# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
tol = 1e-14


materials.set_all(0)

rotation_angle = 0.
plane_orientations.set_all(rotation_angle)

pdeg90 = range(13,17)
pdegm90 = range(17,21)
pdeg180 = list(range(5,13)) + list(range(29,33))


for k in pdeg90:
    [plane_orientations.set_value(i, 90) for i in mf.where_equal(k)]

for k in pdegm90:
    [plane_orientations.set_value(i, -90) for i in mf.where_equal(k)]

for k in pdeg180:
    [plane_orientations.set_value(i, 180) for i in mf.where_equal(k)]


#
pippo=dolfin.plot(plane_orientations)
plt.show()
#
#

fiber_orientations.set_all(rotation_angle)

deg90 = [9, 10, 14, 19, 23, 24]
degm45 = [3, 7, 8, 15, 18, 25, 26, 30]
deg45 = [4, 5, 6, 16, 17, 27, 28, 29]

for k in deg90:
    [fiber_orientations.set_value(i, 90) for i in mf.where_equal(k)]

for k in deg45:
    [fiber_orientations.set_value(i, 45) for i in mf.where_equal(k)]

for k in degm45:
    [fiber_orientations.set_value(i, -45) for i in mf.where_equal(k)]

pippo=dolfin.plot(fiber_orientations)
plt.show()


# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)


anba = anba4.anbax_singular(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
stiff = anba.compute()

stiff1 = stiff.getDenseArray()

f1 = np.copy(stiff1)

perm = [2, 0, 1, 5, 3, 4]

print("Permuted:")
f1 = f1[np.ix_(perm, perm)]
print(f1)

f1[np.abs(f1) < 0.01] = 0.0
print(f1)

#anba2 = anbax2(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
#stiff2 = anba2.compute()
stiff.view()
#stiff2.view()

np.savetxt('ANBA_T2.txt', f1)

