#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hanba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin
# from dolfin import compile_extension_module
import numpy as np
from petsc4py import PETSc

import anba4
import mshr

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

E = 2.e11
nu = 0.3

G = E/(2*(1+nu))

#Assmble into material mechanical property Matrix.
matMechanicProp = [E, nu]
# Meshing domain.

w = 0.25


if w == 0.8:
    h = 0.2
    timo_shear_coeff_x = 0.1333290635109205
    timo_shear_coeff_y = 0.0933947901258445
    timo_tors_coeff = 0.001797202933169174
    shape = 'RECT'
elif w == 0.25:
    h = w
    timo_shear_coeff_x = 0.051763521409511855
    timo_shear_coeff_y = timo_shear_coeff_x
    timo_tors_coeff = 0.0005491289646685773
    shape = 'SQ'
else:
    print("width not valid for estimate")


Rect = mshr.Rectangle(dolfin.Point(-w/2, -h/2, 0.), dolfin.Point(w/2, h/2, 0.))

mesh = mshr.generate_mesh(Rect, 128)
    
EA  = E*w*h
EJx = E/12*w*h**3
EJy = E/12*h*w**3
GAx = G*timo_shear_coeff_x
GAy = G*timo_shear_coeff_y
GJ = G*timo_tors_coeff

#dolfin.plot(mesh)

#import matplotlib.pyplot as plt
#plt.show()


# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())

materials.set_all(0)
fiber_orientations.set_all(0.0)
plane_orientations.set_all(0.0)

# Build material property library.
mat1 = anba4.material.IsotropicMaterial(matMechanicProp, 1.)

matLibrary = []
matLibrary.append(mat1)

anba = anba4.anbax(mesh, 2, matLibrary, materials, plane_orientations, fiber_orientations)
stiff = anba.compute()
stiff.view()

stiff1 = stiff.getDenseArray()

# print(foil_stiff)
# need to copy otherwise I get garbage
f1 = np.copy(stiff1)

print("EA: anba {0:.6e}, Ewh {1:.6e}".format(f1[2,2], EA))
print("GAx: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[0,0], GAx))
print("GAy: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[1,1], GAy))

print("GJ: anba {0:.6e}, Timoshenko {1:.6e}".format(f1[5,5], GJ))
print("EJx: anba {0:.6e}, 1/12Ewh^3 {1:.6e}".format(f1[3,3], EJx))
print("EJy: anba {0:.6e}, 1/12Ehw^3 {1:.6e}".format(f1[4,4], EJy))



np.savetxt('ANBA_{0}_ISO.txt'.format(shape), f1)

#mass = anba.inertia()
#mass.view()

#stress_result_file = dolfin.XDMFFile('Stress.xdmf')
#stress_result_file.parameters['functions_share_mesh'] = True
#stress_result_file.parameters['rewrite_function_mesh'] = False
#stress_result_file.parameters["flush_output"] = True

#anba.stress_field([0., 1., 0.,], [0., 0., 0.], "local", "paraview")
#anba.strain_field([0., 1., 0.,], [0., 0., 0.], "local", "paraview")
#stress_result_file.write(anba.STRESS, t = 0.)
#stress_result_file.write(anba.STRAIN, t = 1.)


