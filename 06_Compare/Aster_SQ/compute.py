# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np

El = 2.00e11
Et = 1.0e10
En = 5.0e9

Gln = 1.2e10
Glt = 6.0e9
Gtn = 3.0e9

w = 0.25
h = 0.25

A = w*h
Jy = 1/12.*w*h**3
Jz = 1/12.*h*w**3


timo_shear_coeff_y = 0.051763521409511855
timo_shear_coeff_z = timo_shear_coeff_y
timo_tors_coeff = 0.0005491289646685773


EA_L = El*A
EJy_L = El*Jy
EJz_L = El*Jz

EA_T = Et*A
EJy_T = Et*Jy
EJz_T = Et*Jz

EA_N = En*A
EJy_N = En*Jy
EJz_N = En*Jz

nu_iso = 0.3

G_iso = El/(2*(1+nu_iso))

GAy_LT = timo_shear_coeff_y*Glt
GAz_LN = timo_shear_coeff_z*Gln

GAz_TN = timo_shear_coeff_z*Gtn
GAy_LT = timo_shear_coeff_y*Glt

GAy_TN = timo_shear_coeff_y*Gtn

GAy_iso = timo_shear_coeff_y*G_iso
GAz_iso = timo_shear_coeff_z*G_iso
GJ_iso = timo_tors_coeff*G_iso


Aster_th0_phi0 = np.genfromtxt('stiff_X_th0_phi0_SQ_hex20.txt')
Aster_th90_phi0 = np.genfromtxt('stiff_X_th90_phi0_SQ_hex20.txt')
Aster_th0_phi90 = np.genfromtxt('stiff_X_th0_phi90_SQ_hex20.txt')

Aster_iso = np.genfromtxt('./ISO/stiff_X_ISO_SQ_hex20.txt')

anba_fo0 = np.genfromtxt('./anba/ANBA_SQ_fo0.txt')
anba_fo90 = np.genfromtxt('./anba/ANBA_SQ_fo90.txt')
anba_iso = np.genfromtxt('./anba/ANBA_SQ_ISO.txt')

print("compare 0 0")
print("$EA$ & $E_Lwh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_L, Aster_th0_phi0[0, 0], anba_fo0[2, 2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_L, Aster_th0_phi0[4, 4], anba_fo0[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_L, Aster_th0_phi0[5, 5], anba_fo0[4, 4]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAy_LT, Aster_th0_phi0[1, 1], anba_fo0[0, 0]))
print("$GA_3$ & Timoshenko $(G_{{LN}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAz_LN, Aster_th0_phi0[2, 2], anba_fo0[1, 1]))
print("GJ & & & {0:f} & {1:f}\\\\".format(Aster_th0_phi0[3, 3], anba_fo0[5, 5]))

print("compare 90 0")
print("$EA$ & $E_Twh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_T, Aster_th90_phi0[0, 0], anba_fo90[2, 2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_T, Aster_th90_phi0[4, 4], anba_fo90[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_T, Aster_th90_phi0[5, 5], anba_fo90[4, 4]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAy_LT, Aster_th90_phi0[1, 1], anba_fo90[0, 0]))
print("$GA_3$ & Timoshenko $(G_{{TN}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAz_TN, Aster_th90_phi0[2, 2], anba_fo90[1, 1]))
print("GJ & & & {0:f} & {1:f}\\\\".format(Aster_th90_phi0[3, 3], anba_fo90[5, 5]))


print("compare 0 90")
print("$EA$ & $E_Twh$ & {0:f} & {1:f} \\\\".format(EA_N, Aster_th0_phi90[0, 0]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} \\\\".format(EJy_N, Aster_th0_phi90[4, 4]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} \\\\".format(EJz_N, Aster_th0_phi90[5, 5]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} \\\\".format(GAy_TN, Aster_th0_phi90[1, 1]))
print("$GA_3$ & Timoshenko $(G_{{TN}})$ & {0:f} & {1:f} \\\\".format(GAz_LN, Aster_th0_phi90[2, 2]))
print("GJ & & & {0:f} \\\\".format(Aster_th0_phi90[3, 3]))


print("compare isotropic")
print("$EA$ & $E_Lwh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_L, Aster_iso[0, 0], anba_iso[2,2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_L, Aster_iso[4, 4], anba_iso[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_L, Aster_iso[5, 5], anba_iso[4, 4]))
print("$GA_2$ & Timoshenko  & {0:f} & {1:f} & {2:f}\\\\".format(GAy_iso, Aster_iso[1, 1], anba_iso[0,0]))
print("$GA_3$ & Timoshenko  & {0:f} & {1:f} & {2:f}\\\\".format(GAz_iso, Aster_iso[2, 2], anba_iso[1,1]))
print("GJ & & & {0:f} & {1:f} & {2:f}\\\\".format(GJ_iso, Aster_iso[3, 3], anba_iso[5,5]))


