import numpy as np
import matplotlib.pyplot as plt


def rotmat(axis, angle_d):
	angle = np.deg2rad(angle_d)

	if axis == 'X':
		mat = np.array([[1.0, 0.0, 0.0], [0.0, np.cos(angle), -np.sin(angle)],[0.0, np.sin(angle), np.cos(angle)]])
	elif axis == 'Y':
		mat = np.array([[np.cos(angle), 0.0, np.sin(angle)], [0.0, 1.0, 0.0], [-np.sin(angle), 0.0, np.cos(angle)]])
	elif axis == 'Z':
		mat = np.array([[np.cos(angle), -np.sin(angle), 0.0], [np.sin(angle), np.cos(angle), 0.0], [0.0, 0.0, 1.0]])
	else:
		raise ValueError('Axis type unknown')
	return mat

def plot_reference_frames( frames, args, vectors = [], plots = [], planes = [] ):
	_args = {
		'figsize'       : ( 12, 12 ),
		'base_frame'    : True,
		'base_color'    : 'w',
		'base_label'    : 'Inertial',
		'frame_labels'  : [ '' ] * len( frames ),
		'frame_colors'  : [ 'm', 'c', 'b' ],
		'frame_zorders' : [ 10 ] * len( frames ),
		'vector_colors' : [ 'm', 'c', 'b' ],
		'vector_labels' : [ '' ] * len( vectors ),
		'vector_texts'  : True,
		'plots_labels'  : [ '' ] * len( plots ),
		'plots_colors'  : [ 'm' ],
		'plots_styles'  : [ '-' ] * len( plots ),
		'eq_plane'      : False,
		'eq_plane_color': 'c',
		'plane_labels'  : [ '' ] * len( planes ),
		'plane_colors'  : [ 'w' ],
		'plane_alphas'  : [ 0.3 ] * len( planes ),
		'no_axes'       : True,
		'axes_no_fill'  : False,
		'legend'        : True,
		'xlabel'        : 'X',
		'ylabel'        : 'Y',
		'zlabel'        : 'Z',
		'xlim'          : 1,
		'ylim'          : 1,
		'zlim'          : 1,
		'title'         : '',
		'azimuth'       : None,
		'elevation'     : None,
		'show'          : False,
		'filename'      : False,
		'dpi'           : 300,
		'frame_text_scale' : 1.1,
		'base_text_scale'  : 1.25,
		'vector_text_scale': 1.3
	}
	for key in args.keys():
		_args[ key ] = args[ key ]

	fig      = plt.figure( figsize = _args[ 'figsize' ] )
	ax       = fig.add_subplot( 111, projection = '3d'  )
	zeros    = [ 0.0, 0.0, 0.0 ]
	n        = 0
	identity = [ [ 1, 0, 0 ], [ 0, 1, 0 ], [ 0, 0, 1 ] ]

	for frame in frames:
		'''
		The frame is passed into the quiver method by rows, but they
		are being plotted by columns. So the 3 basis vectors of the frame
		are the columns of the 3x3 matrix
		'''
		ax.quiver( zeros, zeros, zeros,
			frame[ 0, : ], frame[ 1, : ], frame[ 2, : ],
			color  = _args[ 'frame_colors'  ][ n ],
			label  = _args[ 'frame_labels'  ][ n ],
			zorder = _args[ 'frame_zorders' ][ n ] )

		if _args[ 'vector_texts' ]:
			frame *= _args[ 'frame_text_scale' ]
			if _args['anba']:
				ax.text( frame[ 0, 0 ], frame[ 1, 0 ], frame[ 2, 0 ], 'L',
						color = _args[ 'frame_colors' ][ n ] )
				ax.text( frame[ 0, 1 ], frame[ 1, 1 ], frame[ 2, 1 ], 'T',
						color = _args[ 'frame_colors' ][ n ] )
				ax.text( frame[ 0, 2 ], frame[ 1, 2 ], frame[ 2, 2 ], 'N',
						color = _args[ 'frame_colors' ][ n ] )
			else:
				ax.text( frame[ 0, 0 ], frame[ 1, 0 ], frame[ 2, 0 ], 'L',
						color = _args[ 'frame_colors' ][ n ] )
				ax.text( frame[ 0, 1 ], frame[ 1, 1 ], frame[ 2, 1 ], 'T',
						color = _args[ 'frame_colors' ][ n ] )
				ax.text( frame[ 0, 2 ], frame[ 1, 2 ], frame[ 2, 2 ], 'N',
						color = _args[ 'frame_colors' ][ n ] )

		n += 1

	if _args[ 'base_frame' ]:
		ax.quiver( zeros, zeros, zeros,
			identity[ 0 ], identity[ 1 ], identity[ 2 ],
			color  = _args[ 'base_color' ],
			label  = _args[ 'base_label' ],
			zorder = 0 )

		if _args[ 'vector_texts' ]:
			if _args['anba']:
				ax.text( _args[ 'base_text_scale' ], 0, 0, 'x',
						color = _args[ 'base_color' ] )
				ax.text( 0, _args[ 'base_text_scale' ], 0, 'y',
						color = _args[ 'base_color' ] )
				ax.text( 0, 0, _args[ 'base_text_scale' ], 'z',
						color = _args[ 'base_color' ] )
			else:
				ax.text( _args[ 'base_text_scale' ], 0, 0, '1',
						color = _args[ 'base_color' ] )
				ax.text( 0, _args[ 'base_text_scale' ], 0, '2',
						color = _args[ 'base_color' ] )
				ax.text( 0, 0, _args[ 'base_text_scale' ], '3',
						color = _args[ 'base_color' ] )

	n = 0
	for plot in plots:
		ax.plot( plot[ :, 0 ], plot[ :, 1 ], plot[ :, 2 ],
			_args[ 'plots_colors' ][ n ] + _args[ 'plots_styles' ][ n ],
			label = _args[ 'plots_labels' ][ n ] )
		n += 1

	n = 0
	for vector in vectors:
		ax.quiver( 0, 0, 0,
			vector[ 0 ], vector[ 1 ], vector[ 2 ],
			color = _args[ 'vector_colors' ][ n ],
			label = _args[ 'vector_labels' ][ n ] )

		if _args[ 'vector_texts' ]:
			vector *= _args[ 'vector_text_scale' ]
			ax.text( vector[ 0 ], vector[ 1 ], vector[ 2 ],
				_args[ 'vector_labels' ][ n ],
				color = _args[ 'vector_colors' ][ n ] )
		n += 1


	for n, plane in enumerate(planes):
		ax.plot_surface( plane[ 0 ], plane[ 1 ], plane[ 2 ],
			color  = _args[ 'plane_colors' ][ n ],
			alpha  = _args[ 'plane_alphas' ][ n ],
			label  = _args[ 'plane_labels'][ n ],
			zorder = 0 )

	ax.set_xlabel( _args[ 'xlabel' ] )
	ax.set_ylabel( _args[ 'ylabel' ] )
	ax.set_zlabel( _args[ 'zlabel' ] )
	ax.set_xlim( [ -_args[ 'xlim' ], _args[ 'xlim' ] ] )
	ax.set_ylim( [ -_args[ 'ylim' ], _args[ 'ylim' ] ] )
	ax.set_zlim( [ -_args[ 'zlim' ], _args[ 'zlim' ] ] )
	ax.set_box_aspect( [ 1, 1, 1 ] )
	ax.set_title( _args[ 'title' ] )

	if _args[ 'legend' ]:
		ax.legend()

	if _args[ 'no_axes' ]:
		ax.set_axis_off()

	if _args[ 'axes_no_fill' ]:
		ax.w_xaxis.pane.fill = False
		ax.w_yaxis.pane.fill = False
		ax.w_zaxis.pane.fill = False

	if _args[ 'azimuth' ] is not None:
		ax.view_init( elev = _args[ 'elevation' ],
					  azim = _args[ 'azimuth'   ] )

	if _args[ 'show' ]:
		plt.show()

	if _args[ 'filename' ]:
		plt.savefig( _args[ 'filename' ], dpi = _args[ 'dpi' ] )
		print( 'Saved', _args[ 'filename' ] )

	plt.close()


if __name__ == '__main__':

	anba = False

	if anba:
		fo = 23.0
		po = 90.0
		bl = 'beam ANBA'
		frame1 = np.matmul(rotmat('Y', -90.0), np.matmul(rotmat('X', -90.0), np.matmul(rotmat('X', po), rotmat('Z', fo))))
		#frame1 = rotmat('Y', fo) * rotmat('Z', po)
	else:
		alpha = 0.0 # around z, first
		beta = -23.0  # around current y, second
		gamma = 90.0 # around current x ,third
		bl = 'beam 3D'
		#frame1 = np.dot(rotmat('Y',45), rotmat('X', 90.0))
		frame1 = np.matmul(np.matmul(rotmat('Z', alpha), rotmat('Y', beta)), rotmat('X', gamma) )

	print(frame1)

	frames = [frame1]

	config = {
		'base_color'   : 'k',
		'base_label'   : bl,
		'frame_colors' : ['r', 'g'],
		'frame_labels' : ['orth orientation'],
		'plane_labels' : ['section', r'$G_{NT}$', r'$G_{LT}$'],
		'plane_colors' : ['b','y','m'],
		'elevation'    : 12,
		'azimuth'      : -19,
		'filename'     : 'frame_3D.png',
		'anba'		   : anba,
		'show'         : True
	}

	y1 = np.arange(-2, 2, 0.25)
	z1 = np.arange(-1.2, 1.2, 0.25)
	Y, Z = np.meshgrid(y1, z1)
	X = np.zeros_like(Y)

	x2 = np.arange(0, 0.5, 0.25)
	y2 = np.arange(0, 0.5, 0.25)

	X2, Y2 = np.meshgrid(x2, y2)

	Z2 = np.zeros_like(X2)

	if anba:
		sect_plane = [Y, Z, X]
		g1_plane = [X2, Z2, Y2]
		g2_plane = [X2, Y2, Z2]
	else:
		sect_plane = [X, Y, Z]
		g1_plane = [X2, Z2, Y2]
		g2_plane = [X2, Y2, Z2]

	curr_planes = [sect_plane] #, g1_plane, g2_plane]

	plot_reference_frames(frames, config, planes=curr_planes )
