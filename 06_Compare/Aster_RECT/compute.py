# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np

El = 2.00e11
Et = 1.0e10
En = 5.0e9

Gln = 1.2e10
Glt = 6.0e9
Gtn = 3.0e9

w = 0.8
h = 0.2

A = w*h
Jy = 1/12.*w*h**3
Jz = 1/12.*h*w**3


timo_shear_coeff_y = 0.1333290635109205
timo_shear_coeff_z = 0.0933947901258445


timo_tors_coeff = 0.001797202933169174

EA_L = El*A
EJy_L = El*Jy
EJz_L = El*Jz

EA_T = Et*A
EJy_T = Et*Jy
EJz_T = Et*Jz

EA_N = En*A
EJy_N = En*Jy
EJz_N = En*Jz

nu_iso = 0.3

G_iso = El/(2*(1+nu_iso))

GAy_LN = timo_shear_coeff_y*Gln
GAz_LN = timo_shear_coeff_z*Gln

GAy_LT = timo_shear_coeff_y*Glt
GAz_TN = timo_shear_coeff_z*Gtn

GAy_TN = timo_shear_coeff_y*Gtn

GAy_iso = timo_shear_coeff_y*G_iso
GAz_iso = timo_shear_coeff_z*G_iso
GJ_iso = timo_tors_coeff*G_iso


Aster_th0_phi0 = np.genfromtxt('stiff_X_th0_phi0_RECT_hex20.txt')
Aster_th90_phi0 = np.genfromtxt('stiff_X_th90_phi0_RECT_hex20.txt')
Aster_th0_phi90 = np.genfromtxt('stiff_X_th0_phi90_RECT_hex20.txt')

Aster_th45_phi0 = np.genfromtxt('stiff_X_th45_phi0_RECT_hex20.txt')

Aster_th0_phi45_psi90 = np.genfromtxt('stiff_X_th0_phi-45_psi90_RECT_hex20.txt')


Aster_iso = np.genfromtxt('./ISO/stiff_X_ISO_l05_RECT_hex20.txt')

anba_fo0 = np.genfromtxt('./anba/ANBA_RECT_fo0.txt')
anba_fo90 = np.genfromtxt('./anba/ANBA_RECT_fo90.txt')

anba_fo45 = np.genfromtxt('./anba/ANBA_RECT_fo45.txt')
anba_fo45_po90 = np.genfromtxt('./anba/ANBA_RECT_fo45_po90.txt')

anba_iso = np.genfromtxt('./anba/ANBA_RECT_ISO.txt')

print("compare 0 0")
print("$EA$ & $E_Lwh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_L, Aster_th0_phi0[0, 0], anba_fo0[2, 2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_L, Aster_th0_phi0[4, 4], anba_fo0[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_L, Aster_th0_phi0[5, 5], anba_fo0[4, 4]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAy_LT, Aster_th0_phi0[1, 1], anba_fo0[0, 0]))
print("$GA_3$ & Timoshenko $(G_{{LN}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAz_LN, Aster_th0_phi0[2, 2], anba_fo0[1, 1]))
print("GJ & & & {0:f} & {1:f}\\\\".format(Aster_th0_phi0[3, 3], anba_fo0[5, 5]))

print("compare 90 0")
print("$EA$ & $E_Twh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_T, Aster_th90_phi0[0, 0], anba_fo90[2, 2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_T, Aster_th90_phi0[4, 4], anba_fo90[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_T, Aster_th90_phi0[5, 5], anba_fo90[4, 4]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAy_LT, Aster_th90_phi0[1, 1], anba_fo90[0, 0]))
print("$GA_3$ & Timoshenko $(G_{{TN}})$ & {0:f} & {1:f} & {2:f}\\\\".format(GAz_TN, Aster_th90_phi0[2, 2], anba_fo90[1, 1]))
print("GJ & & & {0:f} & {1:f}\\\\".format(Aster_th90_phi0[3, 3], anba_fo90[5, 5]))


print("compare 0 90")
print("$EA$ & $E_Twh$ & {0:f} & {1:f} \\\\".format(EA_N, Aster_th0_phi90[0, 0]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} \\\\".format(EJy_N, Aster_th0_phi90[4, 4]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Twh^3$ & {0:f} & {1:f} \\\\".format(EJz_N, Aster_th0_phi90[5, 5]))
print("$GA_2$ & Timoshenko $(G_{{LT}})$ & {0:f} & {1:f} \\\\".format(GAy_TN, Aster_th0_phi90[1, 1]))
print("$GA_3$ & Timoshenko $(G_{{TN}})$ & {0:f} & {1:f} \\\\".format(GAz_LN, Aster_th0_phi90[2, 2]))
print("GJ & & & {0:f} \\\\".format(Aster_th0_phi90[3, 3]))


print("compare isotropic")
print("$EA$ & $E_Lwh$ & {0:f} & {1:f} & {2:f}\\\\".format(EA_L, Aster_iso[0, 0], anba_iso[2,2]))
print("$EJ_2$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJy_L, Aster_iso[4, 4], anba_iso[3, 3]))
print("$EJ_3$ & $\\frac{{1}}{{12}}E_Lwh^3$ & {0:f} & {1:f} & {2:f}\\\\".format(EJz_L, Aster_iso[5, 5], anba_iso[4, 4]))
print("$GA_2$ & Timoshenko  & {0:f} & {1:f} & {2:f}\\\\".format(GAy_iso, Aster_iso[1, 1], anba_iso[0,0]))
print("$GA_3$ & Timoshenko  & {0:f} & {1:f} & {2:f}\\\\".format(GAz_iso, Aster_iso[2, 2], anba_iso[1,1]))
print("GJ & & & {0:f} & {1:f} & {2:f}\\\\".format(GJ_iso, Aster_iso[3, 3], anba_iso[5,5]))

print(GAz_LN)
print(GAy_LN)



print("compare 45")

anba_fo45[np.abs(anba_fo45) < 0.01] = 0.0
Aster_th45_phi0[np.abs(Aster_th45_phi0) < 0.01] = 0.0
print("ANBA")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*anba_fo45[i,:]))

print("3D")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*Aster_th45_phi0[i,:]))


print("compare 45 90")

anba_fo45_po90[np.abs(anba_fo45_po90) < 0.01] = 0.0
Aster_th0_phi45_psi90[np.abs(Aster_th0_phi45_psi90) < 0.01] = 0.0
print("ANBA")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*anba_fo45_po90[i,:]))

print("3D")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*Aster_th0_phi45_psi90[i,:]))

