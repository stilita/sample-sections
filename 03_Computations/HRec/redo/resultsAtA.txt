Compare norm of results:
2-Norm of original solution: 0.845693
2-Norm of overdetermined system solution: 0.845693
Relative error (norm of difference)/(norm of solution): 1.77295e-08

**********
1-Norm of original solution: 94.7516
1-Norm of overdetermined system solution: 94.7516
Relative error (norm of difference)/(norm of solution): 1.47209e-08

**********
Inf-Norm of original solution: 0.0263452
Inf-Norm of overdetermined system solution: 0.0263452
Relative error (norm of difference)/(norm of solution): 3.18367e-08
