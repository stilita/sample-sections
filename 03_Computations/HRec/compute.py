# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np

Aster_low = np.genfromtxt('./low/stiff_HRec_24w_12h_10t_hex20.txt')

# Aster20 = np.genfromtxt('stiff_HRec_24w_12h_10t_hex20.txt')

anba = np.genfromtxt('ANBA_FeilRect.txt')


perm = [1, 2, 0, 4, 5, 3]

print("Permuted:")
Aster_p = Aster_low[np.ix_(perm, perm)]

print(Aster_p)

anba[np.abs(anba) < 0.01] = 0.0
Aster_low[np.abs(Aster_low) < 0.01] = 0.0
Aster_p[np.abs(Aster_p) < 0.01] = 0.0
#Aster20[np.abs(Aster20) < 0.01] = 0.0


print("ANBA")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*anba[i,:]))

print("3D quad")
for i in range(6):
    print("{0:.2f} & {1:.2f} & {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f}\\\\".format(*Aster_p[i,:]))

