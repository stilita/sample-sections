# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 09:40:12 2024

@author: claudio.caccia
"""

import numpy as np


constlbfin2 = 0.0028731106352
constlbfin = 0.11298482933333


def format_scientific(num):
    if np.isnan(num):
        curr_str = '-'
    elif num == 0.0:
        curr_str = '0'
    else:
        curr_str = "{:.4e}".format(num).replace('e', '\\times 10^{') + '}'
    return curr_str


stiffYu = np.zeros((6, 6))

stiffYu[0, 0] = 1.437e6
stiffYu[1, 1] = 9.027e4
stiffYu[2, 2] = 3.943e4
stiffYu[3, 3] = 1.679e4
stiffYu[4, 4] = 6.621e4
stiffYu[5, 5] = 1.725e5

stiffYu[0, 3] = 1.074e5
stiffYu[3, 0] = stiffYu[0, 3]

stiffYu[1, 4] = -5.201e4
stiffYu[4, 1] = stiffYu[1, 4]

stiffYu[2, 5] = -5.635e4
stiffYu[5, 2] = stiffYu[2, 5]

compYu = np.linalg.inv(stiffYu)

resS44 = np.zeros((6, 5))
resS55 = np.zeros((6, 5))
resS14 = np.zeros((6, 5))
resS45 = np.zeros((6, 5))


# S44 Borri
resS44[0, 0] = 11.25e-5
resS44[1, 0] = 6.445e-5
resS44[2, 0] = 6.836e-5
resS44[3, 0] = 8.332e-5
resS44[4, 0] = 6.120e-5
resS44[5, 0] = 5.903e-5

# S44 Bauchau
resS44[0, 1] = 11.21e-5
resS44[1, 1] = 6.297e-5
resS44[2, 1] = 6.674e-5
resS44[3, 1] = 8.336e-5
resS44[4, 1] = 6.130e-5
resS44[5, 1] = 5.915e-5

# S44 Yu
resS44[0, 2] = compYu[3, 3]
resS44[1, 2] = np.nan
resS44[2, 2] = np.nan
resS44[3, 2] = np.nan
resS44[4, 2] = np.nan
resS44[5, 2] = np.nan

# S55 Borri
resS55[0, 0] = 2.665e-5
resS55[1, 0] = 1.884e-5
resS55[2, 0] = 2.018e-5
resS55[3, 0] = 2.380e-5
resS55[4, 0] = 5.620e-5
resS55[5, 0] = 10.60e-5

# S55 Bauchau
resS55[0, 1] = 2.666e-5
resS55[1, 1] = 1.899e-5
resS55[2, 1] = 2.098e-5
resS55[3, 1] = 2.376e-5
resS55[4, 1] = 5.622e-5
resS55[5, 1] = 10.35e-5

# S55 Yu
resS55[0, 2] = compYu[4, 4]
resS55[1, 2] = np.nan
resS55[2, 2] = np.nan
resS55[3, 2] = np.nan
resS55[4, 2] = np.nan
resS55[5, 2] = np.nan

# S14 Borri
resS14[0, 0] = 8.340e-6
resS14[1, 0] = 2.700e-6
resS14[2, 0] = 1.410e-6
resS14[3, 0] = 0.0
resS14[4, 0] = 0.0
resS14[5, 0] = 0.0

# S14 Bauchau
resS14[0, 1] = 8.338e-6
resS14[1, 1] = 2.736e-6
resS14[2, 1] = 1.436e-6
resS14[3, 1] = 0.0
resS14[4, 1] = 0.0
resS14[5, 1] = 0.0

# S14 Yu
resS14[0, 2] = compYu[0, 3]
resS14[1, 2] = np.nan
resS14[2, 2] = np.nan
resS14[3, 2] = np.nan
resS14[4, 2] = np.nan
resS14[5, 2] = np.nan

# S45 Borri
resS45[0, 0] = 0.0
resS45[1, 0] = 0.0
resS45[2, 0] = 0.0
resS45[3, 0] = 2.470e-5
resS45[4, 0] = 3.600e-5
resS45[5, 0] = 3.610e-5

# S45 Bauchau
resS45[0, 1] = 0.0
resS45[1, 1] = 0.0
resS45[2, 1] = 0.0
resS45[3, 1] = 2.469e-5
resS45[4, 1] = 3.606e-5
resS45[5, 1] = 3.616e-5

# S14 Yu
resS45[0, 2] = compYu[3, 4]
resS45[1, 2] = np.nan
resS45[2, 2] = np.nan
resS45[3, 2] = np.nan
resS45[4, 2] = np.nan
resS45[5, 2] = np.nan


for i in range(1, 7):
    st = np.loadtxt(f'./Case{i}/stiff_Thin6_20w_10h_02t_hex20_case{i}.txt')
    comp = np.linalg.inv(st)
    resS44[i-1, 4] = comp[3, 3]
    resS55[i-1, 4] = comp[4, 4]
    resS14[i-1, 4] = comp[0, 3]
    resS45[i-1, 4] = comp[3, 4]


for i in range(1, 7):
    comp = np.loadtxt(f'../Anba/ANBA_6Plies_compl_case{i}.txt')
    resS44[i-1, 3] = comp[3, 3]
    resS55[i-1, 3] = comp[4, 4]
    resS14[i-1, 3] = comp[0, 3]
    resS45[i-1, 3] = comp[3, 4]


resS44 *= constlbfin2
resS55 *= constlbfin2
resS14 *= constlbfin
resS45 *= constlbfin2


with open('resultsS44.txt', 'w') as f:
    for i in range(6):
        f.write(f'{i+1} ')
        for j in range(5):
            # print(resS44[i, j])
            f.write('& $' + format_scientific(resS44[i, j]) + '$ ')
        f.write('\\\\\n')

with open('resultsS55.txt', 'w') as f:
    for i in range(6):
        f.write(f'{i+1} ')
        for j in range(5):
            # print(resS44[i, j])
            f.write('& $' + format_scientific(resS55[i, j]) + '$ ')
        f.write('\\\\\n')


with open('resultsS14.txt', 'w') as f:
    for i in range(6):
        f.write(f'{i+1} ')
        for j in range(5):
            # print(resS44[i, j])
            f.write('& $' + format_scientific(resS14[i, j]) + '$ ')
        f.write('\\\\\n')


with open('resultsS45.txt', 'w') as f:
    for i in range(6):
        f.write(f'{i+1} ')
        for j in range(5):
            # print(resS44[i, j])
            f.write('& $' + format_scientific(resS45[i, j]) + '$ ')
        f.write('\\\\\n')

