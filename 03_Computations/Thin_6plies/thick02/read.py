# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 09:40:12 2024

@author: claudio.caccia
"""

import numpy as np


res = np.zeros((6, 8))

# S44 Bauchau
res[0, 0] = 11.21e-5
res[1, 0] = 6.297e-5
res[2, 0] = 6.674e-5
res[3, 0] = 8.336e-5
res[4, 0] = 6.130e-5
res[5, 0] = 5.915e-5

# S55 Bauchau
res[0, 2] = 2.666e-5
res[1, 2] = 1.899e-5
res[2, 2] = 2.098e-5
res[3, 2] = 2.376e-5
res[4, 2] = 5.622e-5
res[5, 2] = 10.35e-5

# S14 Bauchau
res[0, 4] = 8.338e-6
res[1, 4] = 2.736e-6
res[2, 4] = 1.436e-6
res[3, 4] = 0.0
res[4, 4] = 0.0
res[5, 4] = 0.0

# S45 Bauchau
res[0, 6] = 0.0
res[1, 6] = 0.0
res[2, 6] = 0.0
res[3, 6] = 2.469e-5
res[4, 6] = 3.606e-5
res[5, 6] = 3.616e-5


for i in range(1, 7):
    st = np.loadtxt(f'./Case{i}/stiff_Thin6_20w_10h_02t_hex20_case{i}.txt')
    comp = np.linalg.inv(st)
    res[i-1, 1] = comp[3, 3]
    res[i-1, 3] = comp[4, 4]
    res[i-1, 5] = comp[0, 3]
    res[i-1, 7] = comp[3, 4]

print(res)

with open('results.txt', 'w') as f:
    f.write(' Lay-up & $S^{Bau}_{44}$ & $S^{pres}_{44}$ & $S^{Bau}_{55}$ & $S^{pres}_{55}$ & $S^{Bau}_{14}$ & $S^{pres}_{14}$ & $S^{Bau}_{45}$ & $S^{pres}_{45}$ \\\\\n')
    for i in range(6):
        f.write(f'{i+1} ')
        for j in range(8):
            print(res[i,j])
            f.write('& {:.4e}'.format(res[i, j]))
        f.write('\\\\\n')
