File config_Thin_6plies.json will be used for configuration.
Using MED mesh file
Name of Meshes...
Thin_6plies 
Looking for R1 group...
Looking for R2 group...
Nodes and elements:
Mesh dimension: 3
Mesh number of nodes: 17580
Read R2 coordinates for REORDERING
not Y
not Z
Beam oriented along X
Reorder R2
Using NPZ matrix type
Load stiffness matrix
Load mass matrix
Loading shape... 
Loading format... 
Loading indptr... 
Loading indices... 
Loading data... 
Load stiffness matrix
Loading shape... 
Loading format... 
Loading indptr... 
Loading indices... 
Loading data... 
Setup section centers coordinates...
P1: 0
0
0
P2: 0.5
  0
  0
DoFs: 52740
R coord: 13680
L coord: 39060
Permute matrices to split into R and L dofs
finished Permutation at Thu Feb  6 16:44:41 2025
elapsed time: 0.000806554s
Extract M_RR
Extract M_RL
Extract M_LL
Permute matrices to split into R and L dofs
finished Permutation at Thu Feb  6 16:44:42 2025
elapsed time: 0.374575s
Extract K_RR
Extract K_LLExtract K_LR

finished Extracting Matrices at Thu Feb  6 16:44:42 2025
elapsed time: 0.376228s
finished Building Solver at Thu Feb  6 16:44:42 2025
elapsed time: 0.000126636s
finished Analyzing pattern at Thu Feb  6 16:44:42 2025
elapsed time: 0.266368s
finished Factorizing at Thu Feb  6 16:44:47 2025
elapsed time: 4.37298s
finished Computing at Thu Feb  6 16:44:51 2025
elapsed time: 4.20408s
