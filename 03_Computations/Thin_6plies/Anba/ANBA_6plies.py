#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Anba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin #import *
# from dolfin import compile_extension_module
import time
import math
import numpy as np
from petsc4py import PETSc
import os
# import matplotlib.pyplot as plt

import anba4 #import *
import mshr


from datetime import datetime

startTime = datetime.now()

# from voight_notation import stressVectorToStressTensor, stressTensorToStressVector, strainVectorToStrainTensor, strainTensorToStrainVector
# from material import material
# from anbax import anbax

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

e_xx = 1.42e6
e_yy = 1.42e6
e_zz = 20.59e6
nu_xy = 0.42
nu_zx = 0.42
nu_zy = 0.42
g_xy = e_yy/(2*(1+nu_xy))
g_xz = 0.87e6
g_yz = 0.87e6


#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy

thickness = 0.03
width = 0.953 
height = 0.537

P_11 = dolfin.Point(-width/2., -height/2., 0.)
P_12 = dolfin.Point( width/2., -height/2., 0.)
P_22 = dolfin.Point( width/2.,  height/2., 0.)
P_21 = dolfin.Point(-width/2.,  height/2., 0.)
p_11 = dolfin.Point(-width/2.+thickness, -height/2.+thickness, 0.)
p_12 = dolfin.Point( width/2.-thickness, -height/2.+thickness, 0.)
p_22 = dolfin.Point( width/2.-thickness,  height/2.-thickness, 0.)
p_21 = dolfin.Point(-width/2.+thickness,  height/2.-thickness, 0.)
Box1 = mshr.Polygon((P_11, P_12, p_12, p_11))
Box2 = mshr.Polygon((P_12, P_22, p_22, p_12))
Box3 = mshr.Polygon((P_22, P_21, p_21, p_22))
Box4 = mshr.Polygon((P_21, P_11, p_11, p_21))

C_shape = Box1 + Box2 + Box3 + Box4
C_shape.set_subdomain(1, Box1)
C_shape.set_subdomain(2, Box2)
C_shape.set_subdomain(3, Box3)
C_shape.set_subdomain(4, Box4)

mesh = mshr.generate_mesh(C_shape, 237)
mf = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim(), mesh.domains())


mesh_points=mesh.coordinates()

print(mesh_points.shape)
print(mesh.num_cells())
print(mesh.num_vertices())


pippo

#dolfin.plot(mesh)
#dolfin.plot(mf)
#plt.show()

print("Execution time pre:")
print(datetime.now() - startTime)


# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
tol = 1e-14


# Rotate mesh.
theta = 15.#0.0
rotation_angle = 0.
materials.set_all(0)
fiber_orientations.set_all(theta)
plane_orientations.set_all(rotation_angle)

[plane_orientations.set_value(i, 180) for i in mf.where_equal(1)]
[plane_orientations.set_value(i, -90) for i in mf.where_equal(2)]
[plane_orientations.set_value(i, 0) for i in mf.where_equal(3)]
[plane_orientations.set_value(i, 90) for i in mf.where_equal(4)]
#pippo=dolfin.plot(plane_orientations)
#plt.show()


# rotate mesh.
rotate = dolfin.Expression(("x[0] * (cos(rotation_angle)-1.0) - x[1] * sin(rotation_angle)",
    "x[0] * sin(rotation_angle) + x[1] * (cos(rotation_angle)-1.0)"), rotation_angle = rotation_angle * np.pi / 180.0,
    degree = 1)

dolfin.ALE.move(mesh, rotate)
#dolfin.plot(mesh)
#dolfin.plot(mf)
#plt.show()


# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)


anba = anba4.anbax_singular(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
stiff = anba.compute()

stiff1 = stiff.getDenseArray()

f1 = np.copy(stiff1)

perm = [2, 0, 1, 5, 3, 4]

print("Permuted:")
f1 = f1[np.ix_(perm, perm)]
print(f1)

f1[np.abs(f1) < 0.5] = 0.0
print(f1)

#anba2 = anbax2(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
#stiff2 = anba2.compute()
stiff.view()
#stiff2.view()


np.savetxt('ANBA_6Plies_case1.txt', f1)

print("Execution time total:")
print(datetime.now() - startTime)

