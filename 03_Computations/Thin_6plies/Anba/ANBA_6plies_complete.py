#
# Copyright (C) 2018 Marco Morandini
#
#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Anba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#

import dolfin #import *
# from dolfin import compile_extension_module
import time
import math
import numpy as np
from petsc4py import PETSc
import os
# import matplotlib.pyplot as plt

import anba4 #import *
import mshr

from datetime import datetime

startTime = datetime.now()


# from voight_notation import stressVectorToStressTensor, stressTensorToStressVector, strainVectorToStrainTensor, strainTensorToStrainVector
# from material import material
# from anbax import anbax

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["form_compiler"]["quadrature_degree"] = 2

# Basic material parameters. 9 is needed for orthotropic materials.

e_xx = 1.42e6
e_yy = 1.42e6
e_zz = 20.59e6
nu_xy = 0.42
nu_zx = 0.42
nu_zy = 0.42
g_xy = e_yy/(2*(1+nu_xy))
g_xz = 0.87e6
g_yz = 0.87e6


#Assmble into material mechanical property Matrix.
matMechanicProp = np.zeros((3,3))
matMechanicProp[0,0] = e_xx
matMechanicProp[0,1] = e_yy
matMechanicProp[0,2] = e_zz
matMechanicProp[1,0] = g_yz
matMechanicProp[1,1] = g_xz
matMechanicProp[1,2] = g_xy
matMechanicProp[2,0] = nu_zy
matMechanicProp[2,1] = nu_zx
matMechanicProp[2,2] = nu_xy

thickness = 0.03
width = 0.953 
height = 0.537

P_11 = [None] * 7
P_12 = [None] * 7
P_22 = [None] * 7
P_21 = [None] * 7

for i in range(7):
    P_11[i] = dolfin.Point(-width/2. + i*thickness/6, -height/2. + i*thickness/6, 0.)
    P_12[i] = dolfin.Point( width/2. - i*thickness/6, -height/2. + i*thickness/6, 0.)
    P_22[i] = dolfin.Point( width/2. - i*thickness/6,  height/2. - i*thickness/6, 0.)
    P_21[i] = dolfin.Point(-width/2. + i*thickness/6,  height/2. - i*thickness/6, 0.)

#p_11 = dolfin.Point(-width/2.+thickness, -height/2.+thickness, 0.)
#p_12 = dolfin.Point( width/2.-thickness, -height/2.+thickness, 0.)
#p_22 = dolfin.Point( width/2.-thickness,  height/2.-thickness, 0.)
#p_21 = dolfin.Point(-width/2.+thickness,  height/2.-thickness, 0.)

LowerWall = [None] * 6
LeftWall = [None] * 6
UpperWall = [None] * 6
RightWall = [None] * 6

for i in range(5, -1, -1):
    LowerWall[i] = mshr.Polygon((P_11[i], P_12[i], P_12[i+1], P_11[i+1]))  # Box1
    RightWall[i] = mshr.Polygon((P_12[i], P_22[i], P_22[i+1], P_12[i+1]))  # Box2
    UpperWall[i] = mshr.Polygon((P_22[i], P_21[i], P_21[i+1], P_22[i+1]))  # Box3
    LeftWall[i] = mshr.Polygon((P_21[i], P_11[i], P_11[i+1], P_21[i+1]))   # Box4 


Box6Plies = LowerWall[0] + LowerWall[1] + LowerWall[2] + LowerWall[3] + LowerWall[4] + LowerWall[5] + \
RightWall[0] + RightWall[1] + RightWall[2] + RightWall[3] + RightWall[4] + RightWall[5] + \
UpperWall[0] + UpperWall[1] + UpperWall[2] + UpperWall[3] + UpperWall[4] + UpperWall[5] + \
LeftWall[0] + LeftWall[1] + LeftWall[2] + LeftWall[3] + LeftWall[4] + LeftWall[5]

# Box6Plies = sum(LowerWall) + sum(RightWall) + sum(UpperWall) + sum(LeftWall)


for i in range(1,7):
    Box6Plies.set_subdomain(i, LowerWall[i-1])
    Box6Plies.set_subdomain(6+i, RightWall[i-1])
    Box6Plies.set_subdomain(12+i, UpperWall[i-1])
    Box6Plies.set_subdomain(18+i, LeftWall[i-1])

mesh = mshr.generate_mesh(Box6Plies, 256)
mf = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim(), mesh.domains())

print("num cells :" + str(mesh.num_cells()))
print("num vertices :" + str(mesh.num_vertices()))

mesh_points=mesh.coordinates()
#dolfin.plot(mesh)
#dolfin.plot(mf)
#plt.show()

print("Execution time pre:")
print(datetime.now() - startTime)

# CompiledSubDomain
materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
tol = 1e-14


case_study = 6

if case_study == 1:
	fiber_up = [15.0]*6
	fiber_low = [15.0]*6
	fiber_left  = [15.0]*6
	fiber_right = [15.0]*6
elif case_study == 2:
	fiber_up = [0.0, 30.0]*3
	fiber_low = [0.0, 30.0]*3
	fiber_left  = [0.0, 30.0]*3
	fiber_right = [0.0, 30.0]*3
elif case_study == 3:
	fiber_up = [0.0, 45.0]*3
	fiber_low = [0.0, 45.0]*3
	fiber_left  = [0.0, 45.0]*3
	fiber_right = [0.0, 45.0]*3
elif case_study == 4:
	fiber_up = [15.0]*6
	fiber_low = [-15.0]*6
	fiber_left  = [15.0, -15.0]*3
	fiber_right = [-15.0, 15.0]*3
elif case_study == 5:
	fiber_up = [30.0]*6
	fiber_low = [-30.0]*6
	fiber_left  = [30.0, -30.0]*3
	fiber_right = [-30.0, 30.0]*3
elif case_study == 6:
	fiber_up = [45.0]*6
	fiber_low = [-45.0]*6
	fiber_left  = [45.0, -45.0]*3
	fiber_right = [-45.0, 45.0]*3
else:
	print("Case study not in correct range. Exiting...")
	exit()



materials.set_all(0)

for i_dom in range(1,7):
    [plane_orientations.set_value(i, 180) for i in mf.where_equal(i_dom)]
    [plane_orientations.set_value(i, -90) for i in mf.where_equal(6+i_dom)]
    [plane_orientations.set_value(i, 0) for i in mf.where_equal(12+i_dom)]
    [plane_orientations.set_value(i, 90) for i in mf.where_equal(18+i_dom)]

for i_dom in range(1,7):
    [fiber_orientations.set_value(i, fiber_low[i_dom-1]) for i in mf.where_equal(i_dom)]
    [fiber_orientations.set_value(i, fiber_right[i_dom-1]) for i in mf.where_equal(6+i_dom)]
    [fiber_orientations.set_value(i, fiber_up[i_dom-1]) for i in mf.where_equal(12+i_dom)]
    [fiber_orientations.set_value(i, fiber_left[i_dom-1]) for i in mf.where_equal(18+i_dom)]

#pippo=dolfin.plot(plane_orientations)
#plt.show()


# Build material property library.
mat1 = anba4.material.OrthotropicMaterial(matMechanicProp)

matLibrary = []
matLibrary.append(mat1)


anba = anba4.anbax_singular(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
stiff = anba.compute()

stiff1 = stiff.getDenseArray()

f1 = np.copy(stiff1)

perm = [2, 0, 1, 5, 3, 4]

print("Permuted:")
f1 = f1[np.ix_(perm, perm)]
print(f1)

f1[np.abs(f1) < 0.01] = 0.0
print(f1)

#anba2 = anbax2(mesh, 1, matLibrary, materials, plane_orientations, fiber_orientations, 1.)
#stiff2 = anba2.compute()
stiff.view()
#stiff2.view()


np.savetxt(f'ANBA_6Plies_case{case_study}.txt', f1)

print("Execution time total:")
print(datetime.now() - startTime)
