#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import os


# In[2]:


sims = ['hex08', 'hex20'] #['ABQ', 'CCX', 'MED']

#sims = ['CCX']
numblocks = [1, 2, 3, 4, 5, 8 , 12, 20] #, 16, 20]
dirblocks = ['01', '02', '03', '04', '05', '08', '12', '20']


# In[3]:


E = 1.0
ν = 0.3

G = E/(2*(1+ν))

ρ = 1.0

lx = 0.2
ly = 0.25
lz = 0.5

A = lx*ly
Jx = 1./12*lx*ly**3
Jy = 1./12*ly*lx**3

# Roark formula for polar inertia table 10.7 pag. 401
if ly > lx:
    a = ly/2.
    b = lx/2.
else:
    a = lx/2.
    b = ly/2.

Jz = a*b**3*(16./3 - 3.36*b/a*(1-b**4/(12*a**4)))


m_th = ρ*lx*ly*lz
cg_th = np.array([0., 0., 0.25])
I_th = 1./12*m_th*np.array([(ly**2+lz**2),(lx**2+lz**2),(lx**2+ly**2)])


# In[4]:


EA_th = E*A
GA_th = 5./6*G*A
EJx_th = E*Jx
EJy_th = E*Jy
GJz_th = G*Jz


# In[5]:


hex08 = {}
hex20 = {}

nsim = len(numblocks)

hex08['mass'] = np.zeros((nsim-1,1))
hex20['mass'] = np.zeros((nsim,1))

hex08['cg'] = np.zeros((nsim-1,3))
hex20['cg'] = np.zeros((nsim,3))

hex08['inertia'] = np.zeros((nsim-1,3))
hex20['inertia'] = np.zeros((nsim,3))

hex08['elastic'] = np.zeros((nsim-1,6))
hex20['elastic'] = np.zeros((nsim,6))



# In[6]:


for sim in sims:
    for cdb, db in enumerate(dirblocks):
        if sim == 'hex08' and cdb == 0:
            continue
        
        curr_dir = './'+db+'x10x10_'+sim
        
        print(curr_dir)
        content = os.listdir(curr_dir)
        for curr_file in content:
            if curr_file.find('mass') >= 0:
                print(curr_file)
                data = np.genfromtxt(curr_dir+'/'+curr_file)
                
                if sim == 'hex20':
                    hex20['mass'][cdb] = data[0,0]
                    hex20['cg'][cdb,:] = data[1,:]
                    hex20['inertia'][cdb,:] = np.diag(data[2:,:])
                elif sim == 'hex08':
                    hex08['mass'][cdb-1] = data[0,0]
                    hex08['cg'][cdb-1,:] = data[1,:]
                    hex08['inertia'][cdb-1,:] = np.diag(data[2:,:])
            elif curr_file.find('stiff') >= 0:
                print(curr_file)
                data = np.genfromtxt(curr_dir+'/'+curr_file)
                
                if sim == 'hex20':
                    hex20['elastic'][cdb,:] = np.diag(data)
                elif sim == 'hex08':
                    hex08['elastic'][cdb-1,:] = np.diag(data)





# In[44]:


fs = 24

plt.rcParams['font.family'] = ['serif']
plt.rcParams['font.serif'] = ['Times New Roman']

fig, axes = plt.subplots(3,2,figsize=(30,28))

# EA

axes[0,0].plot(numblocks, hex20['elastic'][:,0], 'o', ms=12, label = 'computed: quadratic')
axes[0,0].plot(numblocks[1:], hex08['elastic'][:,0], 'd', ms=10, label = 'computed: linear')
axes[0,0].plot([0,40], [EA_th, EA_th], '-.', lw=2, label=r'analytical: $E\ell_{\eta}\ell_{\zeta}$')
#plt.plot(numblocks[0:3], abq['elastic'][:,2], 'd', label = 'Abaqus')
#plt.xlabel('# elems. along axis', fontsize=fs)
axes[0,0].set_xlim(numblocks[0],numblocks[-1])
axes[0,0].set_ylabel(r'$EA$', fontsize=fs)
axes[0,0].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[0,0].set_xticks(ticks=numblocks)
axes[0,0].set_xticklabels(numblocks, fontsize=fs-4)
axes[0,0].tick_params(axis='y', labelsize=fs-4)
axes[0,0].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[0,0].yaxis.offsetText.set_fontsize(fs-6)
axes[0,0].grid()
axes[0,0].legend(fontsize=fs-4);

# GJ

axes[0,1].plot(numblocks, hex20['elastic'][:,3], 'o', ms=10, label = 'computed: quadratic')
axes[0,1].plot(numblocks[1:], hex08['elastic'][:,3], 'd', ms=10, label = 'computed: linear')
axes[0,1].plot([0,40], [1.32101955e-4, 1.32101955e-4], '-.', lw=2, label='analytical: Timoshenko')
axes[0,1].plot([0,40], [GJz_th, GJz_th], ':', lw=2, label='analytical: Roark')
#plt.plot(numblocks[0:3], abq['elastic'][:,5], 'd', label = 'Abaqus')
#plt.xlabel('# elems. along axis', fontsize=fs)
axes[0,1].set_ylabel(r'$GJ$', fontsize=fs)
axes[0,1].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[0,1].set_ylim(1.3*1e-4, 1.35*1e-4) #(numblocks[0],numblocks[-1])
axes[0,1].set_xticks(ticks=numblocks)
axes[0,1].set_xticklabels(numblocks, fontsize=fs-4)
axes[0,1].tick_params(axis='y', labelsize=fs-6)
axes[0,1].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-3,1))
axes[0,1].yaxis.offsetText.set_fontsize(fs-6)
axes[0,1].grid()
axes[0,1].legend(fontsize=fs-4);

# GA eta

axes[1,0].plot(numblocks, hex20['elastic'][:,1], 'o', ms=10, label = 'computed: quadratic')
axes[1,0].plot(numblocks[1:], hex08['elastic'][:,1], 'd', ms=10, label = 'computed: linear')
axes[1,0].plot([0,40], [GA_th, GA_th], '-.', lw=2, label=r'analytical: $\frac{5}{6}G\ell_{\eta}\ell_{\zeta}$')
axes[1,0].plot([0,40], [0.01671445649, 0.01671445649], ':', lw=2, label=r'analytical: Hutchinson')
#plt.plot(numblocks[0:3], abq['elastic'][:,0], 'd', label = 'Abaqus')
axes[1,0].set_ylabel(r'$GA_{\eta}$', fontsize=fs)
axes[1,0].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[1,0].set_ylim(1.5*1e-2, 2.5*1e-2) #(numblocks[0],numblocks[-1])
axes[1,0].set_xticks(ticks=numblocks)
axes[1,0].set_xticklabels(numblocks, fontsize=fs-4)
axes[1,0].tick_params(axis='y', labelsize=fs-6)
axes[1,0].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[1,0].yaxis.offsetText.set_fontsize(fs-6)
axes[1,0].grid()
axes[1,0].legend(fontsize=fs-4);

# GA zeta

axes[1,1].plot(numblocks, hex20['elastic'][:,2], 'o', ms=10, label = 'computed: quadratic')
axes[1,1].plot(numblocks[1:], hex08['elastic'][:,2], 'd', ms= 10, label = 'computed: linear')
axes[1,1].plot([0,40], [GA_th, GA_th], '-.', lw=2, label=r'analytical: $\frac{5}{6}G\ell_{\eta}\ell_{\zeta}$')
axes[1,1].plot([0,40], [0.016904227, 0.016904227], ':', lw=2, label=r'analytical: Hutchinson')

#plt.plot(numblocks[0:3], abq['elastic'][:,1], 'd', label = 'Abaqus')
#plt.xlabel('# elems. along axis', fontsize=fs)
axes[1,1].set_ylabel(r'$GA_{\zeta}$', fontsize=fs)
axes[1,1].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[1,1].set_ylim(1.5*1e-2, 2.5*1e-2) #(numblocks[0],numblocks[-1])
axes[1,1].set_xticks(ticks=numblocks)
axes[1,1].set_xticklabels(numblocks, fontsize=fs-4)
axes[1,1].tick_params(axis='y', labelsize=fs-6)
axes[1,1].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[1,1].yaxis.offsetText.set_fontsize(fs-6)
axes[1,1].grid()
axes[1,1].legend(fontsize=fs-4);


# EJ eta

axes[2,0].plot(numblocks, hex20['elastic'][:,4], 'o', ms=10, label = 'computed: quadratic')
axes[2,0].plot(numblocks[1:], hex08['elastic'][:,4], 'd', ms=10, label = 'computed: linear')
axes[2,0].plot([0,40], [EJx_th, EJx_th], '-.', lw=2, label=r'analytical: $\frac{E}{12}\ell_{\eta}\ell_{\zeta}^3$')
#plt.plot(numblocks[0:3], abq['elastic'][:,3], 'd', label = 'Abaqus')
#plt.xlabel('# elems. along axis', fontsize=fs)
axes[2,0].set_ylabel(r'$EJ_{\eta}$', fontsize=fs)
axes[2,0].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[2,0].set_xticks(ticks=numblocks)
axes[2,0].set_xticklabels(numblocks, fontsize=fs-4)
axes[2,0].tick_params(axis='y', labelsize=fs-6)
axes[2,0].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[2,0].yaxis.offsetText.set_fontsize(fs-6)
axes[2,0].grid()
axes[2,0].set_xlabel(r'# elems. along $\xi$', fontsize=fs)
axes[2,0].legend(fontsize=fs-4);
#plt.ylim(1e3*EJx_th -1e-2,1e3*EJx_th +1e-2)

#EJ zeta

axes[2,1].plot(numblocks, hex20['elastic'][:,5], 'o', ms=10, label = 'computed: quadratic')
axes[2,1].plot(numblocks[1:], hex08['elastic'][:,5], 'd', ms=10, label = 'computed: linear')
axes[2,1].plot([0,40], [EJy_th, EJy_th], '-.', lw=2, label=r'analytical: $\frac{E}{12}\ell_{\eta}^3\ell_{\zeta}$')
#plt.plot(numblocks[0:3], abq['elastic'][:,4], 'd', label = 'Abaqus')
axes[2,1].set_xlabel(r'# elems. along $\xi$', fontsize=fs)
axes[2,1].set_ylabel(r'$EJ_{\zeta}$', fontsize=fs)
axes[2,1].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[2,1].set_xticks(ticks=numblocks)
axes[2,1].set_xticklabels(numblocks, fontsize=fs-4)
axes[2,1].tick_params(axis='y', labelsize=fs-6)
axes[2,1].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[2,1].yaxis.offsetText.set_fontsize(fs-6)
axes[2,1].grid()
axes[2,1].set_xlabel(r'# elems. along $\xi$', fontsize=fs)
#plt.ylim(1e3*EJy_th -1e-2,1e3*EJy_th +1e-2)
axes[2,1].legend(fontsize=fs-4);

#axes[1,0].set_xlabel(r'# elems. along $\xi$', fontsize=fs)

plt.savefig('elastic2.png', dpi=300)

plt.show()


