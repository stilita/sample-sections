# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 12:08:48 2023

@author: claudio.caccia
"""

import numpy as np


tall = False

if tall:
    a = 0.16/2
    b = 0.32/2
else:
    a = 0.32/2
    b = 0.16/2
    

nu = 0.3
E = 1
A = 4*a*b

G = E/(2*(1+nu))

C4 = 4/45*a**3*b*(-12*a**2-15*nu*a**2+5*nu*b**2)

n_arr = np.arange(start=1, stop=1000)

acc = 0.0

for n in n_arr:
    tmpval = 16*nu**2*b**5*(n*np.pi*a-b*np.tanh(n*np.pi*a/b))/((n*np.pi)**5*(1+nu))
    print("n: {0} - current value: {1}".format(n,tmpval))
    
    acc += tmpval
    
    if np.abs(tmpval) < 1e-16:
        break

C4 += acc

k = -2*(1+nu)/(9/(4*a**5*b)*C4+nu*(1-b**2/a**2))

print("k = {0} - kGA = {1} - 5/6GA = {2} - Delta perc: {3}".format(k, k*G*A, 5/6.*G*A, (k*G*A-5/6.*G*A)/(5/6.*G*A)*100.))

