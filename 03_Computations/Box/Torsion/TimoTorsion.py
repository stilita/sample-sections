# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 14:56:49 2023

@author: claudio.caccia
"""
import numpy as np

a = 0.25
b = 0.25
E = 1.0
nu = 0.30
G = 1 #E/(2*(1+nu))


a = a / 2
b = b / 2
#Timoshenko Eq. 154 pp. 300
k2 = 16/3*b*a**3

for i in range(10000):
    k2 = k2 + 16/3*b*a**3 * (- 192/np.pi**5*a/b/(2*i+1)**5*np.tanh(((2*i+1)*np.pi*b)/(2*a)));

k2 = k2*G;

print(k2)
