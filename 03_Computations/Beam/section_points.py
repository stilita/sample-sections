
import numpy as np
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import matplotlib
print(matplotlib.get_backend())

matplotlib.use('Qt5Agg')

print(os.getcwd())

root_folder = '//wsl.localhost/Ubuntu-24.04D/home/claudio/Projects/sample-sections/03_Computations/Beam/'

test_folder = 'hex20/'

pR1 = 'Warp_R1.txt'
pR2 = 'Warp_R2.txt'

dR1 = 'Warp_Dwarp1.txt'
dR2 = 'Warp_Dwarp2.txt'

coords_R1 = np.genfromtxt(root_folder + test_folder + pR1)
coords_R2 = np.genfromtxt(root_folder + test_folder + pR2)

warpR1 = np.genfromtxt(root_folder + test_folder + dR1)
warpR2 = np.genfromtxt(root_folder + test_folder + dR2)

print(np.shape(warpR1))

Dtot = np.load(root_folder + test_folder + 'FD.npz')

Dnode = Dtot['Dnode']

Dnode = Dnode.reshape(-1,6)

DnodeR1 = Dnode[:np.shape(warpR1)[0], :]
DnodeR2 = Dnode[np.shape(warpR1)[0]:, :]

print(np.shape(Dnode))

F = np.zeros((6,1))

F[0] = 1e8


displacementsR1 = (np.dot(warpR1, F)).reshape(-1, 3)
displacementsR2 = (np.dot(warpR2, F)).reshape(-1, 3)

disptotR1 = (np.dot(DnodeR1, F)).reshape(-1, 3)
disptotR2 = (np.dot(DnodeR2, F)).reshape(-1, 3)

useR1 = True
plotDispTot = False
# print(coords_R1)
# print(np.shape(coords_R1))
# print(np.shape(warpR1))

# Extract components
if useR1:
    x, y, z = coords_R1[:, 0], coords_R1[:, 1], coords_R1[:, 2]
    u, v, w = displacementsR1[:, 0], displacementsR1[:, 1], displacementsR1[:, 2]
    ut, vt, wt = disptotR1[:, 0], disptotR1[:, 1], disptotR1[:, 2]
else:
    x, y, z = coords_R2[:, 0], coords_R2[:, 1], coords_R2[:, 2]
    u, v, w = displacementsR2[:, 0], displacementsR2[:, 1], displacementsR2[:, 2]
    ut, vt, wt = disptotR2[:, 0], disptotR2[:, 1], disptotR2[:, 2]


fig = plt.figure(figsize=(20,10))

ax1 = fig.add_subplot(221)

ax1.plot(y, z, 'ro', ms=6)
ax1.plot(y+v, z+w, 'bx', ms=6)
for i in range(len(x)):
    ax1.plot([y[i], y[i]+v[i]], [z[i], z[i]+w[i]], color=[0.5, 0.5, 0.5])

if plotDispTot:
    ax1.plot(y + vt, z + wt, 'k^', ms=6)
    for i in range(len(x)):
        ax1.plot([y[i], y[i] + vt[i]], [z[i], z[i] + wt[i]], color='g')

ax2 = fig.add_subplot(222)

ax2.plot(x, z, 'ro', ms=6)
ax2.plot(x+u, z+w, 'bx', ms=6)
for i in range(len(x)):
    ax2.plot([x[i], x[i]+u[i]], [z[i], z[i]+w[i]], color=[0.5, 0.5, 0.5])

if plotDispTot:
    ax2.plot(x + ut, z + wt, 'k^', ms=6)
    for i in range(len(x)):
        ax2.plot([x[i], x[i] + ut[i]], [z[i], z[i] + wt[i]], color='g')

ax3 = fig.add_subplot(223)

ax3.plot(y, x, 'ro', ms=6)
ax3.plot(y+v, x+u, 'bx', ms=6)
for i in range(len(x)):
    ax3.plot([y[i], y[i]+v[i]], [x[i], x[i]+u[i]], color=[0.5, 0.5, 0.5])
if plotDispTot:
    ax3.plot(y + vt, x + ut, 'k^', ms=6)
    for i in range(len(x)):
        ax3.plot([y[i], y[i] + vt[i]], [x[i], x[i] + ut[i]], color='g')

ax4 = fig.add_subplot(224, projection='3d')
ax4.quiver(x, y, z, u, v, w, color='r')
if plotDispTot:
    ax4.quiver(x, y, z, ut, vt, wt, color='g')

ax4.set_title("3D Vector Plot")
ax4.set_xlabel("X")
ax4.set_ylabel("Y")
ax4.set_zlabel("Z")

plt.show()


# # Create 3D plot
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')

# # Plot vectors using quiver
# ax.quiver(x, y, z, u, v, w, length=1.0, normalize=True, color='r')

# # Add labels for clarity
# ax.set_xlabel('X')
# ax.set_ylabel('Y')
# ax.set_zlabel('Z')

# # Show the plot
# plt.show()