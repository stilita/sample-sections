# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 11:12:13 2024

@author: claudio.caccia
"""

import numpy as np




Stiff_0_0 = np.array([1.25e+10, 312488063.4, 624931451.7, 4411426.638, 65104166.67, 65104166.67]) 

# beam axis z->L T->x N->y
Anba_0_0 = np.array([3.124868e+08, 6.249219e+08, 1.250000e+10, 6.510417e+07,6.510417e+07, 4.4114199071993716e+06])

Stiff_0_90 = np.array([312500000, 156448895.5, 613246330.6, 2679870.521, 1627604.167, 1627604.167])

# beam axis z->T L->x N->z 
Anba_0_90 = np.array([2.976266e+08, 1.562483e+08, 3.125000e+08, 1.627604e+06, 1.627604e+06, 2.2057099538579322e+06])

Stiff_90_0 = np.array([625000000, 309033662.2, 156299572.7, 2205713.319, 3255208.333, 3255208.333])


Anba_90_0 = np.zeros_like(Anba_0_90)

w = 0.25
h = 0.25

El = 2.00e11
Et = 1.0e10
En = 5.0e9

Gln = 1.2e10
Glt = 6.0e9
Gtn = 3.0e9

nu_lt = 0.3
nu_ln = 0.3
nu_tn = 0.3

A = w*h
J = 1./12*w*h**3

k_timo = 0.0005491289646685773
xiA = 0.0517636508164

Jp = 2*J

print('Axial 0  0: {0:6.4e}\texpected El*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_0[0], El*A, Anba_0_0[2]))
print('Axial 0 90: {0:6.4e}\texpected En*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_90[0], En*A, Anba_0_90[2]))
print('Axial 90 0: {0:6.4e}\texpected Et*A: {1:6.4e}'.format(Stiff_90_0[0], Et*A))

print(' ')

print('Bending 0  0: {0:6.4e}\texpected El*J: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_0[4], El*J, Anba_0_0[3]))
print('Bending 0 90: {0:6.4e}\texpected En*J: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_90[4], En*J, Anba_0_90[3]))
print('Bending 90 0: {0:6.4e}\texpected Et*J: {1:6.4e}'.format(Stiff_90_0[4], Et*J))

print(' ')

print('Torsion 0  0: {0:6.4e}\ttentative k_timo*(Glt+Gln)/2: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_0[3], (Gln+Glt)/2*k_timo, Anba_0_0[5]))
print('Torsion 0 90: {0:6.4e}\ttentative k_timo*(Gnt+Gln)/2: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_90[3], (Gtn+Gln)/2*k_timo, Anba_0_90[5]))
print('Torsion 90 0: {0:6.4e}\ttentative k_timo*(Glt+Gtn)/2: {1:6.4e}'.format(Stiff_90_0[3], (Glt+Gtn)/2*k_timo))

print(' ')

print('Shear 0  0: {0:6.4e}\texpected xGlt*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_0[1], Glt*xiA, Anba_0_0[0]))
print('Shear 0 90: {0:6.4e}\texpected xGtn*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_90[1], Gtn*xiA, Anba_0_90[0]))
print('Shear 90 0: {0:6.4e}\texpected xGlt*A: {1:6.4e}'.format(Stiff_90_0[1], Glt*xiA))

print(' ')

print('Shear 0  0: {0:6.4e}\texpected xGln*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_0[2], Gln*xiA, Anba_0_0[1]))
print('Shear 0 90: {0:6.4e}\texpected xGln*A: {1:6.4e}\t ANBA: {2:6.4e}'.format(Stiff_0_90[2], Gln*xiA, Anba_0_90[1]))
print('Shear 90 0: {0:6.4e}\texpected xGtn*A: {1:6.4e}'.format(Stiff_90_0[2], Gtn*xiA))
