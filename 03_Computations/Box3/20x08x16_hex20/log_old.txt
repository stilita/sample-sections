File config_box3.json will be used for configuration.
Using MED mesh file
Name of Meshes...
section 
Looking for R1 group...
Looking for R2 group...
Nodes and elements:
Mesh dimension: 3
Mesh number of nodes: 12153
Read R2 coordinates for REORDERING
not Y
not Z
Beam oriented along X
Reorder R2
Using NPZ matrix type
Load stiffness matrix
Load mass matrix
Loading shape... 
Loading format... 
Loading indptr... 
Loading indices... 
Loading data... 
Load stiffness matrix
Loading shape... 
Loading format... 
Loading indptr... 
Loading indices... 
Loading data... 
Setup section centers coordinates...
P1: 0
0
0
P2: 0.5
  0
  0
DoFs: 36459
R coord: 2598
L coord: 33861
Permute matrices to split into R and L dofs
finished Permutation at Thu Feb 13 09:50:33 2025
elapsed time: 0.000471281s
Extract M_RR
Extract M_LL
Extract M_RL
Permute matrices to split into R and L dofs
finished Permutation at Thu Feb 13 09:50:33 2025
elapsed time: 0.281476s
Extract K_RR
Extract K_LLExtract K_LR

finished Extracting Matrices at Thu Feb 13 09:50:34 2025
elapsed time: 0.283142s
finished Building Solver at Thu Feb 13 09:50:34 2025
elapsed time: 7.3642e-05s
finished Analyzing pattern at Thu Feb 13 09:50:34 2025
elapsed time: 0.240942s
finished Factorizing at Thu Feb 13 09:50:40 2025
elapsed time: 5.59644s
finished Computing at Thu Feb 13 09:50:43 2025
elapsed time: 3.87035s
finished Solving at Thu Feb 13 09:52:58 2025
elapsed time: 134.134s
finished inverting PhiR at Thu Feb 13 09:52:58 2025
elapsed time: 0.0624621s
finished computing MRL_PhiR at Thu Feb 13 09:52:58 2025
elapsed time: 0.443356s
finished computing Mat1 at Thu Feb 13 09:53:13 2025
elapsed time: 15.244s
finished computing M_BB 1 at Thu Feb 13 09:53:13 2025
elapsed time: 0.0818871s
finished computing M_BB 2 at Thu Feb 13 09:53:13 2025
elapsed time: 0.00714405s
finished computing M_BB 3 at Thu Feb 13 09:53:13 2025
elapsed time: 0.0079791s
finished computing M_BB at Thu Feb 13 09:53:13 2025
elapsed time: 0.00593085s
finished computing K_BB at Thu Feb 13 09:53:14 2025
elapsed time: 0.127399s
empty partitioned matrices..
finished generating Phi_R at Thu Feb 13 09:53:14 2025
elapsed time: 0.00595645s
Compute QR decomposition
QR Decomposition succeded
Get Matrix Q
Get Q block
Compute QR decomposition
QR Decomposition succeded
Get Matrix Q
Get Q block
Generate V
finished generating V at Thu Feb 13 09:53:14 2025
elapsed time: 0.129763s
Generate W
finished generating W at Thu Feb 13 09:53:14 2025
elapsed time: 0.090293s
Generate B
finished generating B at Thu Feb 13 09:53:14 2025
elapsed time: 0.00103027s
Generate Krd
finished generating HC (split) at Thu Feb 13 09:53:14 2025
delta time: 0.0308933s
finished generating Krd at Thu Feb 13 09:53:15 2025
elapsed time: 1.29082s
finished generating V W B Krd Matrices at Thu Feb 13 09:53:15 2025
elapsed time: 0.000108392s
Assemble matrices
build VTB
finished generating VTB at Thu Feb 13 09:53:15 2025
elapsed time: 0.105747s
build WTB
finished generating WTB at Thu Feb 13 09:53:15 2025
elapsed time: 0.0556724s
build VTKrdV
finished generating VTKrdV at Thu Feb 13 09:53:19 2025
elapsed time: 4.15946s
build WTKrdV
finished generating WTKrdV at Thu Feb 13 09:53:24 2025
elapsed time: 4.4755s
build VTW
finished generating VTW at Thu Feb 13 09:53:26 2025
delta time: 1.91689s
build invWTW
finished generating inv invWTW at Thu Feb 13 09:53:32 2025
elapsed time: 7.86692s
finished generating A B matrices at Thu Feb 13 09:53:32 2025
elapsed time: 0.000761776s
build A11 - A12*invA22*A21
finished building matrix A at Thu Feb 13 09:53:33 2025
elapsed time: 1.13418s
solve
qtot size: 2604 rows, 6 columns
finished solving for qtot at Thu Feb 13 09:53:39 2025
elapsed time: 6.01442s
ftot size: 2598 rows, 6 columns
finished solving for ftot at Thu Feb 13 09:53:39 2025
elapsed time: 0.0379845s
Mass: 0.0256
center of mass: 
        0.25
 -1.1591e-15
-1.08116e-15
Jcm: 
 0.000273067  9.63368e-19 -3.09898e-18
 9.58391e-19  0.000751787  9.47533e-18
-3.05694e-18  9.46475e-18  0.000587947
Generate H
Generate C
Elastic matrix:
      0.0512  1.97947e-16 -8.43511e-16 -1.24688e-17 -9.04524e-17  2.42412e-17
-5.88889e-16     0.015448  9.69287e-16 -3.53479e-17  5.79591e-16 -8.97141e-15
-7.79661e-16  9.85498e-16    0.0164026 -5.55799e-18  3.31756e-15 -2.52881e-16
-1.24688e-17 -3.53479e-17 -5.55799e-18  0.000115287  6.17553e-19  5.62437e-19
-9.04524e-17  5.79591e-16  3.31756e-15 -1.45221e-18  0.000436907  -3.0409e-17
 2.42412e-17 -8.97141e-15 -2.52881e-16  3.75167e-18 -3.27083e-17  0.000109227
finished computing properties at Thu Feb 13 09:53:39 2025
elapsed time: 0.101147s
Writing inertial properties...
Writing elastic properties...
