# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 13:00:34 2023

@author: claudio.caccia
"""

import os
import glob
import subprocess

def find_and_replace(template_path, subfolder_path, file_name):
    
    # Extract the file name without extension
    file_name_without_extension = os.path.splitext(file_name)[0]
       
    curr_type = file_name_without_extension.split('_',1)[1]
    
    token_replacements = {
        '{mesh_file}': file_name,
        '{type}': curr_type,
        }
    
    # Read the template file
    with open(template_path, 'r') as template_file:
        template_content = template_file.read()

    # Replace tokens with predefined strings
    for token, replacement in token_replacements.items():
        template_content = template_content.replace(token, replacement)

    # Save the modified template into the subfolder
    modified_template_path = os.path.join(subfolder_path, 'config_box3.json')
    with open(modified_template_path, 'w') as modified_template_file:
        modified_template_file.write(template_content)

    return os.path.basename(modified_template_path)

def process_subfolders(current_folder, target_files, template_path):
    # Get the list of subfolders in the current folder
    subfolders = [f.path for f in os.scandir(current_folder) if f.is_dir()]

    for subfolder in subfolders:
        print(f"Processing subfolder: {subfolder}")

        # Check if all target files are present in the subfolder
        matrices_present = all(os.path.isfile(os.path.join(subfolder, file)) for file in target_files)

        if matrices_present:
            # Get the name of one of the target files (assuming the first one)
            med_files = [os.path.basename(x) for x in glob.glob(os.path.join(subfolder, '*.med'))]
            
            if len(med_files) < 1:
                print(f"not enough MED mesh files in {subfolder}")
            elif len(med_files) > 1:
                print(f"too many MED mesh files in {subfolder}")
            else:
                # Call the function to replace tokens in the template
                modified_template_path = find_and_replace(template_path, subfolder, med_files[0])

                # Run the external program in the current subfolder with the modified template as input
                subprocess.run(['Section', '-f', modified_template_path], cwd=subfolder)
                #print(f"mock run Section -f {modified_template_path} in {subfolder}")
        else:
            print(f'Mass and/or Stiff not found in {subfolder}')

if __name__ == "__main__":
    # Replace these values with your actual file names, template path, and external program
    target_files = ['Mass.npz', 'Mass.npz']
    template_path = 'config_box_template.json'  # Replace with the actual path to your template file

    # Get the current folder
    current_folder = os.getcwd()

    # Call the function to process subfolders
    process_subfolders(current_folder, target_files, template_path)

    print("Program completed.")
