#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import os


# In[2]:


sims = ['hex08', 'hex20'] #['ABQ', 'CCX', 'MED']

#sims = ['CCX']
numblocks = [1, 2, 3, 4, 5, 8 , 12, 20] #, 16, 20]
dirblocks = ['01', '02', '03', '04', '05', '08', '12', '20']


# In[3]:


E = 1.0
ν = 0.3

G = E/(2*(1+ν))

ρ = 1.0

lx = 0.16
ly = 0.32
lz = 0.5

A = lx*ly
Jx = 1./12*lx*ly**3
Jy = 1./12*ly*lx**3

# Roark formula for polar inertia table 10.7 pag. 401
if ly > lx:
    a = ly/2.
    b = lx/2.
else:
    a = lx/2.
    b = ly/2.

Jz = a*b**3*(16./3 - 3.36*b/a*(1-b**4/(12*a**4)))


m_th = ρ*lx*ly*lz
cg_th = np.array([0., 0., 0.25])
I_th = 1./12*m_th*np.array([(lx**2+ly**2),(ly**2+lz**2),(lx**2+lz**2)])


# In[4]:


EA_th = E*A
GA_th = 5./6*G*A
EJx_th = E*Jx
EJy_th = E*Jy
GJz_th = G*Jz


# In[5]:


hex08 = {}
hex20 = {}

nsim = len(numblocks)

hex08['mass'] = np.zeros((nsim-1,1))
hex20['mass'] = np.zeros((nsim,1))

hex08['cg'] = np.zeros((nsim-1,3))
hex20['cg'] = np.zeros((nsim,3))

hex08['inertia'] = np.zeros((nsim-1,3))
hex20['inertia'] = np.zeros((nsim,3))

hex08['elastic'] = np.zeros((nsim-1,6))
hex20['elastic'] = np.zeros((nsim,6))



# In[6]:


for sim in sims:
    for cdb, db in enumerate(dirblocks):
        if sim == 'hex08' and cdb == 0:
            continue
        
        curr_dir = './'+db+'x08x16_'+sim
        
        print(curr_dir)
        content = os.listdir(curr_dir)
        for curr_file in content:
            if curr_file.find('mass') >= 0:
                print(curr_file)
                data = np.genfromtxt(curr_dir+'/'+curr_file)
                
                if sim == 'hex20':
                    hex20['mass'][cdb] = data[0,0]
                    hex20['cg'][cdb,:] = data[1,:]
                    hex20['inertia'][cdb,:] = np.diag(data[2:,:])
                elif sim == 'hex08':
                    hex08['mass'][cdb-1] = data[0,0]
                    hex08['cg'][cdb-1,:] = data[1,:]
                    hex08['inertia'][cdb-1,:] = np.diag(data[2:,:])
            elif curr_file.find('stiff') >= 0:
                print(curr_file)
                data = np.genfromtxt(curr_dir+'/'+curr_file)
                
                if sim == 'hex20':
                    hex20['elastic'][cdb,:] = np.diag(data)
                elif sim == 'hex08':
                    hex08['elastic'][cdb-1,:] = np.diag(data)





# In[44]:

print("mass (kg) & ${0}$ & $ {1} $ & \\\\".format(hex20['mass'][-1][0], m_th))
print("CoG $\\xi$ (m) & ${0}$ & $ {1} $ & \\\\".format(hex20['cg'][-1][0], cg_th[2]))
print("CoG $\\eta$ (m) & ${0}$ & $ {1} $ & \\\\".format(hex20['cg'][-1][1], cg_th[1]))
print("CoG $\\zeta$ (m) & ${0}$ & $ {1} $ & \\\\".format(hex20['cg'][-1][2], cg_th[0]))

print("$I_\\xi\\xi$ (kg m^2) & ${0:.6e}$ & ${1:.6e}$ & \\\\".format(hex20['inertia'][-1][0], I_th[0]))
print("$I_\\eta\\eta$ (kg m^2) & ${0:.6e}$ & ${1:.6e}$ & \\\\".format(hex20['inertia'][-1][1], I_th[1]))
print("$I_\\zeta\\zeta$ (kg m^2) & ${0:.6e}$ & ${1:.6e}$ & \\\\".format(hex20['inertia'][-1][2], I_th[2]))

print(np.max(hex20['mass'])-np.min(hex20['mass']))
print(np.max(hex20['cg'][:,0])-np.min(hex20['cg'][:,0]))
print(np.max(hex20['cg'][:,1])-np.min(hex20['cg'][:,1]))
print(np.max(hex20['cg'][:,2])-np.min(hex20['cg'][:,2]))

print(np.max(hex20['inertia'][:,0])-np.min(hex20['inertia'][:,0]))
print(np.max(hex20['inertia'][:,1])-np.min(hex20['inertia'][:,1]))
print(np.max(hex20['inertia'][:,2])-np.min(hex20['inertia'][:,2]))


fs = 24

plt.rcParams['font.family'] = ['serif']
plt.rcParams['font.serif'] = ['Times New Roman']

fig, axes = plt.subplots(3,3,figsize=(30,20))

# mass

axes[0,1].plot([0,40], [m_th, m_th], '-.', lw=2, label='theoretical')
axes[0,1].plot(numblocks, hex20['mass'], 'o', ms=12, label = 'quadratic')
axes[0,1].plot(numblocks[1:], hex08['mass'], 'd', ms=10, label = 'linear')
#axes[0,0].plot(numblocks[0:3], abq['mass'], 'd', label = 'Abaqus')
#axes[0,0].xlabel('blocks')
axes[0,1].set_xlim(numblocks[0],numblocks[-1])
axes[0,1].set_ylabel('mass', fontsize=fs)
axes[0,1].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
axes[0,1].set_xticks(ticks=numblocks)
axes[0,1].set_xticklabels(numblocks, fontsize=fs-4)
axes[0,1].tick_params(axis='y', labelsize=fs-4)
axes[0,1].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
axes[0,1].yaxis.offsetText.set_fontsize(fs-6)
axes[0,1].grid()
axes[0,1].legend(fontsize=fs-2);
axes[0,1].legend();

# CoG


for ind in range(3):
    axes[1,ind].plot([0, 40], [cg_th[ind], cg_th[ind]], '-.', lw=2, label='theoretical')
    #axes[1,0].plot(numblocks[0:3], abq['cg'][:,0], 'd', label = 'Abaqus')
    if ind == 0:
        axes[1,ind].set_ylabel(r'CoG [$\xi$ comp.]', fontsize=fs)
        axes[1,ind].plot(numblocks, hex20['cg'][:,2], 'o', ms=12, label = 'quadratic')
        axes[1,ind].plot(numblocks[1:], hex08['cg'][:,2], 'd', ms=10, label = 'linear')
    elif ind ==1:
        axes[1,ind].set_ylabel(r'CoG [$\eta$ comp.]', fontsize=fs)
        axes[1,ind].plot(numblocks, hex20['cg'][:,ind], 'o', ms=12, label = 'quadratic')
        axes[1,ind].plot(numblocks[1:], hex08['cg'][:,ind], 'd', ms=10, label = 'linear')
    elif ind ==2:
        axes[1,ind].set_ylabel(r'CoG [$\zeta$ comp.]', fontsize=fs)
        axes[1,ind].plot(numblocks, hex20['cg'][:,0], 'o', ms=12, label = 'quadratic')
        axes[1,ind].plot(numblocks[1:], hex08['cg'][:,0], 'd', ms=10, label = 'linear')
        
    axes[1,ind].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
    axes[1,ind].set_xticks(ticks=numblocks)
    axes[1,ind].set_xticklabels(numblocks, fontsize=fs-4)
    axes[1,ind].tick_params(axis='y', labelsize=fs-4)
    axes[1,ind].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
    axes[1,ind].yaxis.offsetText.set_fontsize(fs-6)
    axes[1,ind].grid()
    axes[1,ind].legend(fontsize=fs-2);
    axes[1,ind].legend();

for ind in range(3):
    axes[2,ind].plot([0, 40], [I_th[ind], I_th[ind]], '-.', lw=2, label='theoretical')
    axes[2,ind].plot(numblocks, hex20['inertia'][:,ind], 'o', ms=12, label = 'quadratic')
    axes[2,ind].plot(numblocks[1:], hex08['inertia'][:,ind], 'd', ms=10, label = 'linear')
    #axes[1,0].plot(numblocks[0:3], abq['cg'][:,0], 'd', label = 'Abaqus')
    if ind == 0:
        axes[2,ind].set_ylabel(r'$I_{\xi\xi}$', fontsize=fs)
        #axes[2,ind].set_ylim(2.0*1e-4, 2.2*1e-4)
    elif ind ==1:
        axes[2,ind].set_ylabel(r'$I_{\eta\eta}$', fontsize=fs)
        #axes[2,ind].set_ylim(6.4*1e-4, 6.6*1e-4)
    elif ind ==2:
        axes[2,ind].set_ylabel(r'$I_{\zeta\zeta}$', fontsize=fs)
        #axes[2,ind].set_ylim(6.0*1e-4, 6.2*1e-4)
        
    axes[2,ind].set_xlim(0.5, 20.5) #(numblocks[0],numblocks[-1])
    axes[2,ind].set_xticks(ticks=numblocks)
    axes[2,ind].set_xticklabels(numblocks, fontsize=fs-4)
    axes[2,ind].tick_params(axis='y', labelsize=fs-4)
    axes[2,ind].ticklabel_format(axis='y', style='scientific', useOffset=False, useMathText=True, scilimits=(-1,1))
    axes[2,ind].yaxis.offsetText.set_fontsize(fs-6)
    axes[2,ind].grid()
    axes[2,ind].legend(fontsize=fs-2);
    axes[2,ind].set_xlabel(r'# elems. along $\xi$', fontsize=fs)
    axes[2,ind].legend();

axes[0,0].set_axis_off()
axes[0,2].set_axis_off()

plt.savefig('box3_mass.png', dpi=300, bbox_inches='tight')

plt.show()


