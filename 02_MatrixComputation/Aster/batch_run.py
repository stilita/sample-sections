# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 11:26:55 2023

@author: claudio.caccia
"""

import os
import shutil
import subprocess

def copy_and_process_files(source_dir, template_file, output_dir, file_extension):
    # Create the output directory if it doesn't exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # List all files with the specified extension in the source directory
    files_to_process = [file for file in os.listdir(source_dir) if file.endswith(file_extension)]

    for file_to_process in files_to_process:
        # Build the full path of the file to process
        file_path = os.path.join(source_dir, file_to_process)

        # Copy the file to the target directory
        shutil.copy(file_path, './')
        
        
        # Read the content of the template file
        with open(template_file, 'r') as template:
            template_content = template.read()

        # Replace a placeholder in the template with the file name
        new_content = template_content.replace('{mesh_file}', file_to_process)

        # Create a new file with the replaced content
        new_file_path = os.path.join('./', "assemble.export")
        with open(new_file_path, 'w') as new_file:
            new_file.write(new_content)

        # Execute the program with the new file as input
        subprocess.run(['as_run', new_file_path])
        
        
        # Extract the file name without extension
        file_name_without_extension = os.path.splitext(file_to_process)[0]
        
        curr_output_dir_name = file_name_without_extension.split('_',1)[1]
        print(curr_output_dir_name)
        

        # Create the output directory based on the file name without extension
        curr_output_directory = os.path.join(output_dir, curr_output_dir_name)

        # Create the output directory if it doesn't exist
        if not os.path.exists(curr_output_directory):
            os.makedirs(curr_output_directory)

        # Move the output file to the output directory
        files_to_move = ['Mass.npz', 'Stiff.npz', file_to_process]
        
        for file_to_move in files_to_move:
            curr_file_path = os.path.join(curr_output_directory, file_to_move)
            if os.path.isfile(file_to_move):
                shutil.move(file_to_move, curr_file_path)
            else:
                print("File {0} doesn't exist! not moved".format(curr_file_path))


if __name__ == "__main__":
    # Set your source directory, target directory, template file, program executable, output directory, and file extension
    source_directory = "../../01_MeshGeneration/Box"
    template_file_path = "assemble_template.export"
    output_directory = "../../03_Computations/Box3/face/"
    file_extension = ".med"

    copy_and_process_files(source_directory, template_file_path, output_directory, file_extension)


